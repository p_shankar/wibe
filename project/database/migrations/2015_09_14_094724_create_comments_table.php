<?php

use Jenssegers\Mongodb\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('comments', function(Blueprint $collection)
		{
			$collection->increments('id');
			$collection->string('type',125);
			$collection->integer('type_id')->unsigned();
			$collection->string('name',32);
			$collection->string('email',125);
			$collection->string('web_site',125);
			$collection->text('comment');
			$collection->boolean('status');
			$collection->integer('created_by')->nullable();
			$collection->integer('modified_by')->nullable();
			$collection->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('comments');
	}

}
