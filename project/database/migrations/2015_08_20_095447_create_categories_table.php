<?php

use Jenssegers\Mongodb\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('categories', function(Blueprint $collection)
		{
			$collection->increments('id');
			$collection->string('type',100);
			$collection->string('name',100);
			$collection->text('description')->nullable();
			$collection->string('image',125)->nullable();
			$collection->boolean('status');
			$collection->integer('created_by')->nullable();
			$collection->integer('modified_by')->nullable();
			$collection->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('categories');
	}

}
