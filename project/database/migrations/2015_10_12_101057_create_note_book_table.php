<?php

use Jenssegers\Mongodb\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoteBookTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('note_book', function(Blueprint $collection)
		{
			$collection->increments('id');
			$collection->string('type',125);
			$collection->integer('type_id')->unsigned();
			$collection->string('description',255);
			$collection->integer('created_by')->nullable();
			$collection->integer('modified_by')->nullable();
			$collection->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('note_book');
	}

}
