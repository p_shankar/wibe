<?php

use Jenssegers\Mongodb\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesHTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('articles_h', function(Blueprint $collection)
		{
			$collection->string('title',100);
			$collection->integer('category_id');
			$collection->text('description')->nullable();
			$collection->string('image',125)->nullable();
			$collection->boolean('status')->nullable();
			$collection->integer('created_by')->nullable();
			$collection->integer('modified_by')->nullable();
			$collection->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('articles_h');
	}

}
