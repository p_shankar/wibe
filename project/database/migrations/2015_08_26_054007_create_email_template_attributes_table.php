<?php

use Jenssegers\Mongodb\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailTemplateAttributesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('email_template_attributes', function(Blueprint $collection)
		{
			$collection->increments('id');
			$collection->integer('email_template_id');
			$collection->string('variable',255);
			$collection->integer('created_by')->nullable();
			$collection->integer('modified_by')->nullable();
			$collection->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('email_template_attributes');
	}

}
