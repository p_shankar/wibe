<?php

use Jenssegers\Mongodb\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pages', function(Blueprint $collection)
		{
			$collection->increments('id');
			$collection->string('title',150);
			$collection->integer('type_id');
			$collection->text('description')->nullable();
			$collection->string('image',125)->nullable();
			$collection->string('img_alt_txt',125)->nullable();
			$collection->string('link',125)->nullable();
			$collection->string('meta_title',125)->nullable();
			$collection->text('meta_description')->nullable();
			$collection->string('meta_tags',255)->nullable();
			$collection->boolean('status');
			$collection->integer('created_by')->nullable();
			$collection->integer('modified_by')->nullable();
			$collection->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pages');
	}

}
