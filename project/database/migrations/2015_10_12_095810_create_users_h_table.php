<?php

use Jenssegers\Mongodb\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersHTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_h', function(Blueprint $collection)
		{
			$collection->string('name',50)->nullable();
			$collection->string('email',125)->unique();
			$collection->string('password', 150);
			$collection->boolean('gender')->nullable();
			$collection->date('dob')->nullable();
			$collection->string('phone',32)->nullable();
			$collection->string('profile_photo',255)->nullable();
			$collection->boolean('status')->nullable();
			$collection->integer('created_by')->nullable();
			$collection->integer('modified_by')->nullable();
			$collection->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users_h');
	}

}
