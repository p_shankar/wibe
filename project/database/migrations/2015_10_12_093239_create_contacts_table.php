<?php

use Jenssegers\Mongodb\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contacts', function(Blueprint $collection)
		{
			$collection->increments('id');
			$collection->string('first_name',50);
			$collection->string('last_name',50);
			$collection->string('email',125);
			$collection->string('organisation',150)->nullable();
			$collection->string('location',125);
			$collection->text('message')->nullable();
			$collection->string('address',125);
			$collection->string('phone',150);
			$collection->integer('created_by')->nullable();
			$collection->integer('modified_by')->nullable();
			$collection->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contacts');
	}

}
