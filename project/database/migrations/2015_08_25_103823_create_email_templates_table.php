<?php

use Jenssegers\Mongodb\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailTemplatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('email_templates', function(Blueprint $collection)
		{
			$collection->increments('id');
			$collection->string('name',255)->nullable();
			$collection->string('subject',255);
			$collection->text('content');
			$collection->integer('created_by')->nullable();
			$collection->integer('modified_by')->nullable();
			$collection->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('email_templates');
	}

}
