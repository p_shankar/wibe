<?php

use Jenssegers\Mongodb\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestimonialsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('testimonials', function(Blueprint $collection)
		{
			$collection->increments('id');
			$collection->string('organisation',150);
			$collection->string('employee_name',100);
			$collection->string('employee_designation',100);
			$collection->text('description');
			$collection->string('image',125)->nullable();
			$collection->string('img_alt_txt',125)->nullable();
			$collection->boolean('status');
			$collection->integer('created_by')->nullable();
			$collection->integer('modified_by')->nullable();
			$collection->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('testimonials');
	}

}
