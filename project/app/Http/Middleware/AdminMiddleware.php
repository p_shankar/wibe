<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class AdminMiddleware {

	protected $auth;

	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
		  {
			if ($this->auth->guest())
			{
				   if ($request->ajax())
				   {
					return response('Unauthorized.', 401);
				   }
				   else
				   {
					return redirect()->guest('admin/auth/login');
				   }
			}
			/* elseif($this->auth->check())
				{
					if($request->user()->role == 1 || $request->user()->role == 2 ) {
				
					} else {
						return redirect('admin/auth/login');
					}
				} */
				
			return $next($request);
		  }
}
