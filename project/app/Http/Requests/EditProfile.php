<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class EditProfile extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name'	 => trim('required|alpha|min:3|max:32'),
			'dob' 		 => 'required|date',
			'country_id' => 'required',
			'state_id' 	 => 'required',
			'zip' 		 => trim('required|numeric|digits_between:4,8'),
			'address'	 => trim('required|max:250'),
			'phone' 	 => trim('required|numeric|digits_between:10,12'),
			'type'		 => 'required'
		];
	}
	
	public function messages()
	{
		return [
			'country_id.required' => 'The Country field is required.',
			'state_id.required' => 'The Sate field is required.',
			'dob.date' 		  	  => 'The Date of birth field must be in valid format.',
			'zip.required' 		  => 'The Zip code field is required.',
			'zip.numeric' 		  => 'The Zip code must be a number.',
			'zip.digits_between'  => 'The Xip code must be between 4 and 8 digits.',
			'type.required'  	  => 'The Type field is required.'
		];
	}

}
