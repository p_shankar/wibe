<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class ImageFormat extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'pic' => 'required|image'
		];
	}
	
	public function messages()
	{
		return [
			'pic.required' => 'The photo field is required.',
			'pic.image'    => 'The photo field must be an image.'
		];
	}

}
