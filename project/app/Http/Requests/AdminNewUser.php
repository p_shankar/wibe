<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class AdminNewUser extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	
	public function rules()
	{
		return [
			'name'	 => trim('required|alpha|min:3|max:32'),
			'email'		 	=> trim('required|email|unique:users|max:255'),
			'dob' 		 => 'required|date',
			'country_id' => 'required',
			'state_id' 	 => 'required',
			'zip' 		 => trim('required|numeric|digits_between:4,8'),
			'address'	 => trim('required|max:250'),
			'phone' 	 => trim('required|numeric|digits_between:10,12'),
			'type'		 => 'required'
		];
		
		
		
		
		
		
		/*return [
			'name' 		=> trim('required|min:3|max:32|alpha'),
			'email'		 	=> trim('required|email|unique:users|max:255'),
			'password'	 	=> 'required|confirmed|min:6|max:30',
			'type' 			=> 'required'		
		];*/
	}
}
