<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class CategoryRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type_name' => 'required|alpha|max:124|min:3',
            'type_description' => 'required',
			'status' => 'required',
			'category-image'=>'image'
        ];
    }
	
	public function messages()
    {
        return [
            'type_name.required' => 'Category name is required.',
            'type_name.alpha' => 'Category name may only contain letters.',
            'type_name.max' => 'Category name may not be greater than 124 characters.',
            'type_description.required' => 'Category description is required.',
        ];
    }
}
