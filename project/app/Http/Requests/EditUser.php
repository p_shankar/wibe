<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class EditUser extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'username'	 => trim('required|alpha|min:3|max:32'),
			'first_name' => trim('required|alpha_spaces|min:3|max:32'),
			'last_name'	 => trim('required|alpha_spaces|min:3|max:32'),
			'dob' 		 => 'required|date',
			'country_id' => 'required',
			'state' 	 => 'required',
			'zip' 		 => trim('required|numeric|digits_between:4,8'),
			'address'	 => trim('required|max:250'),
			'phone' 	 => trim('required|numeric|digits_between:10,12'),
			'photo'		 => 'image'
		];
	}
	
	public function messages()
	{
		return [
			'country_id.required' => 'The country field is required.',
			'dob.date' 		  	  => 'The date of birth field must be in valid format.',
			'zip.required' 		  => 'The zip code field is required.',
			'zip.numeric' 		  => 'The zip code must be a number.',
			'zip.digits_between'  => 'The zip code must be between 4 and 8 digits.'
		];
	}

}
