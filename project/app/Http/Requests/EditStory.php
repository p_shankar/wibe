<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class EditStory extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'title' 				=> trim('required|min:3|max:124'),
			'category_id' 			=> 'required',
			'subcategory_id'		=> 'required',
			'story' 				=> 'required',
			'vc_meta_description' 	=> trim('required|alpha_spaces'),
			'vc_meta_title' 		=> trim('required|alpha_spaces|max:124'),
			'vc_meta_keywords' 		=> trim('required|alpha_spaces|max:255'),
			'alias' 				=> 'required|unique:stories|max:124',
			'status' 				=> 'required'
		];
	}
	
	public function messages()
	{
		return [
			'category_id.required' 			=> 'Category field is required',
			'subcategory_id.required'		=> 'Sub-Category field is required',
			'vc_meta_description.required' 	=> 'Description field is required',
			'vc_meta_title.required' 		=> 'Meta Title field is required',
			'vc_meta_keywords.required' 	=> 'Meta keywords field is required',
		];
	}

}
