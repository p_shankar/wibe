<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class PagesRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:125',
            'name' => 'required|max:125',
            'content' => 'required',
            'meta_title' => 'required',
            'meta_description' => 'required',
            'meta_tags' => 'required',
            'status' => 'required'
        ];
    }
}
