<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class AddNewUser extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'email'		 => trim('required|email|unique:users|max:255'),
			'password'	 => 'required|confirmed|min:6|max:30',
			'username'	 => trim('required|alpha|min:3|max:32'),
			'dob' 		 => 'required|date',
			'captcha' 	 => 'required|captcha',
			'agree'      => 'required'
			
		];
	}
	
	public function messages()
	{
		return [
			'dob.date'     => 'The date of birth must be in valid format',
			'dob.required'     => 'The date of birth field is required'
		];
	}
}
