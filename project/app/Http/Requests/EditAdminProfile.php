<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class EditAdminProfile extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name'	 	 => trim('required|alpha|min:3|max:32'),
			'gender'	 => 'required',
			'dob'    	 => 'date',
			'phone'  	 => trim('required|numeric|digits_between:10,12')
		];
	}

	public function messages()
	{
		return [
			'dob.date' 		  	  => 'The date of birth field must be in valid format.'
		];
	}

}
