<?php
Route::get('/home', 'HomeController@index');
Route::get('sociallogin/{provider}', 'Auth\AuthController@sociallogin');
//Route::get('/', 'FrontEndController@index');
Route::get('/', 'FrontEndController@index');
Route::controller('Solar', 'SolarController');
Route::controller('front', 'FrontEndController');
Route::controller('user', 'UserController');
Route::group(['prefix' => 'admin'], function() 
{
	Route::get('/', 'Admin\AdminController@Index');
	Route::controller('auth', 'Admin\AdminauthController');
	Route::controller('dashboard', 'Admin\AdminController');
	Route::controller('password', 'Admin\AdminPasswordController');
	Route::controller('roles', 'Admin\RolesController');
	Route::controller('articles', 'Admin\AdminArticlesController');
	
	Route::controller('permissions', 'Admin\AdminPermissionsController');
	Route::resource('pages', 'Admin\PagesController');
	Route::resource('category', 'Admin\CategoryController');
	Route::resource('subcategory', 'Admin\SubCategoryController');
	Route::resource('template', 'Admin\TemplateController');
});
Route::get('register/verify/{confirmationCode}', [
    'uses' => 'RegistrationController@confirm'
]);
Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
