<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Story;
use App\Models\Like;
use App\Models\Vote;
use App\Models\Page;
use App\Models\Rating;
use App\Models\Comment;
use App\Models\Announcement;
use App\Http\Requests\CommentsRequest;
use App\Models\Follow;
use Input;
use DB;
use Illuminate\Http\Request;
use App\Models\Article;
use App\Models\User;
use repositories\CommonRepositoryInterface;
use App\Models\Category;

class FrontEndController extends Controller {
	
	/**
	 * Added On : 19 oct 2015
	 * Added by : debut infotech
	 * desc : to use common repository using $common variable.
	 */
	public function __construct(CommonRepositoryInterface $common)
	{
		$this->common = $common;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('frontend.home');//return redirect('front');
	}
	
	public function getIndex()
	{
		return view('frontend.home');
	}
	
	/**
	  * Show the announcements.
      * @param int $id            
      * @return Response
	  * Created on: 1/9/2015
	  * Updated on: 8/9/2015
	**/
	public function getAnnouncements()
	{
		try
		{
			$announcements=Announcement::where('status','=','1')->latest()->take(5)->get();
			return view('frontend.announcement',compact('announcements'));
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
	}
	
	/**
	  * Show CMS pages.
      * @param int $id            
      * @return Response
	  * Created on: 1/9/2015
	  * Updated on: 8/9/2015
	**/
	public function getPages()
	{
		try
		{
			$pages=Page::where('status','=','1')->paginate(10);
			return view('frontend.pages',compact('pages'));
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
	}
	
	/**
	  * Get all published stories.
      * @param int $id            
      * @return Response
	  * Created on: 8/9/2015
	  * Updated on: 8/9/2015
	**/
	
	public function getStories()
	{
		try
		{ 
			$stories = Story::where(['status' => '3'])->orderBy('_id','DESC')->get();
			return view('frontend/stories', compact('stories'));
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        } 
	}
	
	/*
	 * Added on : 14 Oct 2015
	 * Added by : debut infotech
	 * Desc : to return all articles in json
	 * */
	public function getArticles()
	{
		try
		{ 
			$articles = Article::orderBy('id','DESC')->get()->toArray();
			//array('report'=>$articles->toArray(),'campaign'=>$articles->category->toArray())
			if(!empty($articles)){
				
				foreach($articles as $article){
					$articleData = Article::findOrfail($article['id']);		
					
					if(!empty($articleData)){
						$category_name = '';
						$articleData->category->toArray();
						$articleData->createdBy->toArray(); 
						$articleData->modifiedBy->toArray(); 
						$data[]['article'] = $articleData->toArray();
					}
				}
				return response()->json($data);
			}else{
				return response()->json(['error'=>'No Records Found.']);
			}
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        } 
	}
	
	/*
	 * Added on : 14 Oct 2015
	 * Added by : debut infotech
	 * Desc : to return single article in json
	 * */
	public function getArticle($id)
	{
		try
		{ 
			$article = Article::findOrfail($id);
			
			if(!empty($article)){
				$article->category->toArray();
				$article->createdBy->toArray(); 
				$article->modifiedBy->toArray(); 
				$data['article'] = $article->toArray();
				return response()->json($data);
			}else{
				return response()->json(['error'=>'No Records Found.']);
			}
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        } 
	}
	
	/*
	 * Added on : 14 Oct 2015
	 * Added by : debut infotech
	 * Desc : to return all users in json
	 * */
	public function getUsers()
	{
		try
		{ 
			$users = User::where('role','!=',1)->orderBy('id','DESC')->get()->toArray();
			if(!empty($users)){
				return response()->json($users);
			}else{
				return response()->json(['error'=>'No Records Found.']);
			}
			
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        } 
	}

	/*
	 * Added on : 14 Oct 2015
	 * Added by : debut infotech
	 * Desc : to return single user in json
	 * */
	public function getUser($id)
	{
		try
		{ 
			$user = User::where(['id' => $id])->where('role','!=',1)->get()->toArray();
			if(!empty($user)){
				return response()->json($user);
			}else{
				return response()->json(['error'=>'No Records Found.']);
			}
			
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        } 
	}
	
	/*
	 * Added on : 19 Oct 2015
	 * Added by : debut infotech
	 * Desc : to return all categories in json
	 * */
	public function getArticleCategory($id)
	{
		try
		{ 
			$article = Article::where('category_id',$id)->where('status','1')->get()->toArray(); 
			if(!empty($article)){
				foreach($article as $catgryArticl){
					$articleData = Article::findOrfail($catgryArticl['id']);		
					if(!empty($articleData)){
						$category_name = '';
						$articleData->category->toArray();
						$articleData->createdBy->toArray(); 
						$articleData->modifiedBy->toArray(); 
						$data[]['article'] = $articleData->toArray();
					}
				}
				return response()->json($data);
			}else{
				return response()->json(['error'=>'No Records Found.']);
			}
			
		}
		catch (\Exception $e) 
		{ 
            echo $e->getMessage();//abort(404);
        } 
	}
	
	/*
	 * Added on : 23 oct 2015
	 * Added by : debut infotech
	 * Desc : to return category list in json.
	 * */
	function getCategories(){
		try
		{ 
			$category = Category::where('status','1')->select(array('type_name','id'))->get(); 
			if(!empty($category)){
				//subcategories
				
				foreach($category as $cat){
					$fetchCat = Category::findOrfail($cat['id']);
					$fetchCat->subcategories;
					$data[]['category'] = $fetchCat;
				}
				
				return response()->json($data);
			}else{
				return response()->json(['error'=>'No Records Found.']);
			}
			
		}
		catch (\Exception $e) 
		{ 
            echo $e->getMessage();//abort(404);
        } 
	}
	
	/*
	 * Added on : 27 oct 2015
	 * Added by : debut infotech
	 * Desc : dummy function to check data sent by ajax request using angular js
	 * */
	 
	 public function getDummy(){
		 return "Please send  a post request to check data.";
	 }
	 public function postDummy(Request $request){
		 return $request->all();
	 }
	
	
}
