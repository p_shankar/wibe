<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Config;

/* Solar service class to call solar services */
use App\Services\Solar;



class SolarController extends Controller {


      protected $solar;


function __construct() {

$this->solar =  new Solar();

}

   /**
	 * function to check connection .
	 * Param : Array of fields those you want to enter in the core
	 */

	public function getIndex()
	{
        $resp = $this->solar->check_connection();
		return $resp;

	}

	/**
	 * function to add document to the Core.
	 * Param : Array of fields those you want to enter in the core
	 */
	public function getAdd()
	{
		$data['id'] = time(); 
		$data['title_s'] = time()."title_s8888";
		$data['desc_txt'] = time()."description9999";
		$resp = $this->solar->add_document($data);
		return  $resp;
	}

		/**
		* function to add document to the Core.
		* Param : Array of fields those you want to enter in the core
		*/

	public function getUpdate($id)
	{
		$data['id'] = $id; 
		$data['title_s'] = time()."title_s8888";
		$data['desc_txt'] = time()."description9999";
		$resp = $this->solar->add_document($data);
		return  $resp;
	}






	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function getDelete($id)
	{
			$resp = $this->solar->delete_document($id);
		return  $resp;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
