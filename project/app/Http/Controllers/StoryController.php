<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use repositories\CommonRepositoryInterface;
use App\Http\Requests\CommentsRequest;
use LaravelCaptcha\Lib\Captcha;
use App\Models\User;
use App\Models\Like;
use App\Models\Vote;
use App\Models\Follow;
use App\Models\Comment;
use App\Models\Story;
use App\Models\Rating;
use Input;
use Image;
use DB;
use Auth;
use App\Models\SubCategory;

class StoryController extends Controller {
	
	/*
	|--------------------------------------------------------------------------
	| Story Controller
	|--------------------------------------------------------------------------
	|
	| This controller manages all operations regarding stories.
	|
	*/
	
	protected $common;
	
	public function __construct(CommonRepositoryInterface $common)
	{
		$this->middleware('auth',['except' => ['getDetail','getComments']]);
		$this->common = $common;
	}
	
	/**
	  * Get all stories written by authenticated user.
      * @param int $id            
      * @return Response
	  * Created on: 8/9/2015
	  * Updated on: 8/9/2015
	**/
	
	public function getStories()
	{
		try
		{
			$id=Auth::user()->_id;
			$stories = Story::where('author_id','=',$id)->paginate(10);
			$stories->setPath('stories');
			return view('user/stories', compact('stories'));
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
	}
	
	/**
	  * Render edit form to update particular story.
      * @param int $id            
      * @return Response
	  * Created on: 8/9/2015
	  * Updated on: 8/9/2015
	**/
	
	public function getEdit($id)
	{
		try
		{
			$story = Story::findorFail($id); 
			// CommonRepository file is used here to fetch categories and subcategories.
			$categories=$this->common->getCategories();
			$subcategories=$this->common->getSubcategories();
			return view('user/edit-story', compact('categories','subcategories','story'));
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
	}
	
	/**
	  * To update the particular story.
      * @param int $id            
      * @return Response
	  * Created on: 8/9/2015
	  * Updated on: 8/9/2015
	**/
	
	public function postEdit($id, Requests\EditStory $request)
	{
		try
		{
			$story = Story::find($id);
			$story->update($request->all());
			if($story->save())
			{
				flash()->success('Story Updated Successfully');
			}
			else
			{
				flash()->error('Sorry, Story can not be updated');
			}
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
		return redirect('story/stories');
	}
	
	/**
	  * Render view to add new story.
      * @param int $id            
      * @return Response
	  * Created on: 8/9/2015
	  * Updated on: 8/9/2015
	**/
	
	public function getNewStory()
	{
		try
		{
			// CommonRepository file is used here to fetch categories and subcategories.
			$categories=$this->common->getCategories();
			$subcategories=$this->common->getSubcategories();
			return view('user/new-story', compact('categories','subcategories'));
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
	}
	
	/**
	  * Fetches the subcategories for a specific category.
      * @param int $id            
      * @return Response
	  * Created on: 8/9/2015
	  * Updated on: 8/9/2015
	**/
	
	public function postSubcategory(Request $request)
	{ 
		try
		{
			$category=$request->cid;
			$subcategories = SubCategory::where('category_id',$category)->where('status','1')->lists('name', '_id');
			return view('user.subcategory', compact('subcategories'));
		}
		catch (\Exception $e) 
		{ 
            return response()->json(['success'=>false]);
        }
	}
	
	/**
	  * Save new story in database.
      * @param int $id            
      * @return Response
	  * Created on: 8/9/2015
	  * Updated on: 8/9/2015
	**/
	
	public function postNewStory(Requests\SubmitStory $request)
	{
		try
		{
			$id = Auth::user()->id;
			$image = Input::file('image')->getClientOriginalName();
			$path = 'uploads/' . $image;
			//Resizing the image
			Image::make(Input::file('image')->getRealPath())->resize(200, 200)->save($path);
			$story = new Story($request->all());
			$story->author_id = $id;
			$story->image = $image;
			if($story->save())
			{
				flash()->success('Story Added Successfully');	
			}
			else
			{
				flash()->error('Sorry, Story can not be added');
			}
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
		return redirect('story/stories');
	}
	
	/**
	  * Check if alias exists or not in database.
      * @param int $id            
      * @return Response
	  * Created on: 8/9/2015
	  * Updated on: 8/9/2015
	**/
	
	public function postUrl(Request $request)
	{
		try
		{
			$alias=$request->url2;
			if(empty($alias) || Story::where('alias','=',$alias)->exists())
			{
				return response()->json(['success'=>true]);
			}
			else
			{
				return response()->json(['success'=>false]);
			}
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
	}
	
	/**
	  * Deletes a specific story.
      * @param int $id            
      * @return Response
	  * Created on: 8/9/2015
	  * Updated on: 8/9/2015
	**/
	
	public function postDelete($id)
	{
		try
		{
			$story = Story::findorFail($id);
			$deleted=$story->delete();
			if($deleted)
			{
				flash()->success('Story deleted Successfully');
			}
			else
			{
				flash()->error('Sorry, Story can not be deleted');
			}
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
		return redirect('story/stories');
	}
	
	/**
	  * View Details of Specified story.
      * @param int $id            
      * @return Response
	  * Created on: 8/9/2015
	  * Updated on: 8/9/2015
	**/
	
	public function getDetail($alias)
	{
		try
		{
			$story_id=Story::where('alias',$alias)->get(['_id']);
			$id=$story_id[0]->id;
			$story = Story::findorFail($id);
			/*************  Like Dislike Code *********/
			$is_follower = false;
			$is_like = false;
			$is_vote = false;
			$is_dislike = false;
			$story_like = Like::where(array('type'=>'story','type_id'=>$id))->get();
			$story_votes = Vote::where(array('type'=>'story','type_id'=>$id))->get();
			if(count($story_votes)==0)
			{
				$is_vote = false;
			}
			if(count($story_like)==0)
			{
			$is_dislike=true;
			}
			else
			{
			if($story_like[0]->like_dislike==1)
			{
			$is_like=true;
			}
			else{
			$is_dislike=true;
			}	
			}
			$count_likes = Like::where(array('type'=>'story','type_id'=>$id,'like_dislike'=>1))->get(array('like_dislike'))->count();
			$count_dislikes = Like::where(array('type'=>'story','type_id'=>$id,'like_dislike'=>0))->get(array('like_dislike'))->count();
			$count_votes = Vote::where(array('type'=>'story','type_id'=>$id,'vote'=>1))->get(array('vote'))->count();
			/*************  Like Dislike Code *********/

			if (auth()->user()) {
			$story_follow = Follow::where(array('type'=>'story','type_id'=>$id))->get();
					if(count($story_follow)==0)
					{
						$is_follower=false;
					}
					else
					{	
						$is_follower=true;
					}					
			}
			return view('user/story_detail', compact('story','is_follower','is_like', 'is_dislike','is_vote','count_likes','count_dislikes','count_votes'),['captcha' => (new Captcha)->html()]);
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
	}
	
	/**
	  * Functionality of Follow/Unfollow the story.            
      * @return Response
	  * Created on: 14/9/2015
	  * Updated on: 14/9/2015
	**/
	
	public function getFollow()
    {
        try {
            $request = Input::all();
            if ($request['follower']) {
                $followers = Follow::where([
					'type'    => $request['type'],
                    'type_id' => $request['id'],
                    'user_id' => auth()->user()->id
                ])->delete();
                $followed = false;
            } else {
                $followers = new Follow();
				$followers->type	= $request['type'];
                $followers->type_id = $request['id'];
                $followers->user_id = auth()->user()->id;
                $followers->save();
                $followed = true;
            }

            $count = Follow::where(array('type'=>'story','type_id'=>$request['id']))->get(array('user_id'))->count();
            $result = [
                'success' => true,
				'type' => $request['type'],
                'followed' => $followed,
                'count' => [
					
					'followers' => Follow::where(array('type'=>$request['type'],'type_id'=>$request['id']))->get()->count()
                ]
            ];
        } catch (\Exception $e) {
            $result = [
                'exception_message' => $e->getMessage()
            ];
        }
        
        return $result;
    }
	
	/**
	  * Functionality of Like/Dislike the story.             
      * @return Response
	  * Created on: 14/9/2015
	  * Updated on: 14/9/2015
	**/
	
	public function getLikedislike()
    {
            $request = Input::all();
            if ($request['type'] == 'like') {
                if (! Like::where([
					'type'	  => $request['category'],
                    'type_id' => $request['id'],
                    'user_id' => auth()->user()->id
                ])->count()) {
                    $likes = new Like();
					$likes->type = $request['category'];
                    $likes->type_id = $request['id'];
                    $likes->user_id = auth()->user()->id;
					$likes->like_dislike = 1;
                    $likes->save();
                } else {
						Like::where(array('user_id'=>auth()->user()->id,'type'=>$request['category'],'type_id'=>$request['id']))->update(array('like_dislike'=>1));
                }
			}
            else {
                
						Like::where(array('user_id'=>auth()->user()->id,'type'=>$request['category'],'type_id'=>$request['id']))->update(array('like_dislike'=>0));
                } 
			
            $result = [
                'success' => true,
                'type' => $request['type'],
                'count' => [
				
				'likes' => Like::where(array('type'=>$request['category'],'type_id'=>$request['id'],'like_dislike'=>1))->get(array('like_dislike'))->count(),
				'dislikes' => Like::where(array('type'=>$request['category'],'type_id'=>$request['id'],'like_dislike'=>0))->get(array('like_dislike'))->count()
                    
                ]
            ];
      
        
        return $result;
    }
	
	public function getVote()
    {
            $request = Input::all();
            if ($request['type'] == 'vote') {
                if (! Vote::where([
					'type'	  => $request['category'],
                    'type_id' => $request['id'],
                    'user_id' => auth()->user()->id
                ])->count()) {
                    $votes = new Vote();
					$votes->type = $request['category'];
                    $votes->type_id = $request['id'];
                    $votes->user_id = auth()->user()->id;
					$votes->vote = 1;
                    $votes->save();
                } else {
						Vote::where(array('user_id'=>auth()->user()->id,'type'=>$request['category'],'type_id'=>$request['id']))->update(array('vote'=>1));
                }
			} 
			
            $result = [
                'success' => true,
                'type' => $request['type'],
                'count' => [
				
				'votes' => Vote::where(array('type'=>$request['category'],'type_id'=>$request['id'],'vote'=>1))->get(array('vote'))->count()
                    
                ]
            ];
      
        
        return $result;
    }
	
	/**
	  * Save comment in storage.             
      * @return Response
	  * Created on: 14/9/2015
	  * Updated on: 14/9/2015
	**/
	
	public function postSaveComment(CommentsRequest $request)
    {
        try {
            $comment = new Comment();
			$comment->type = $request->type;
            $comment->type_id = $request->id;
            $comment->user_id = auth()->user()->id;
            $comment->name = auth()->user()->first_name . ' ' . auth()->user()->last_name;
            $comment->email = auth()->user()->email;
            $comment->comment = $request->comment;
            $comment->save();
            
            return [
                'success' => true
            ];
        } catch (\Exception $e) {
            return [
                'exception_message' => $e->getMessage()
            ];
        }
    }
	
	/**
	  * Fetch number of comments on story.             
      * @return Response
	  * Created on: 14/9/2015
	  * Updated on: 14/9/2015
	**/

    public function getComments()
    {
		try
		{
			//get count of comments
			$count = Comment::where(array('type'=>Input::get('type'),'type_id'=>Input::get('type_id')))->get()->count();
			//apply pagination on comment section
			$comments = Comment::where(array('type'=>Input::get('type'),'type_id'=>Input::get('type_id')))->orderBy('_id','DESC')->paginate(10);
			$user_data =DB::table('comments')
				->join('users', 'users.id', '=', 'comments.user_id')
				->where(array('type'=>Input::get('type'),'type_id'=>Input::get('type_id')))
				->select('users.photo','users.id')
				->get();
			return view('user.comments', compact('comments','user_data','count'));
		}
		catch (\Exception $e) {
            return [
                'exception_message' => $e->getMessage()
            ];
        }
    }
	
	public function postRating(Request $request)
	{
		//return $avg_rating = DB::table('ratings')->select(DB::raw('AVG(score) as avg'))->where(['type' => $request['type'],'type_id' => $request['type_id']])->get();
		
		try
		{
			if (! Rating::where([
					'type'	  => $request['type'],
                    'type_id' => $request['type_id'],
                    'user_id' => auth()->user()->id
			])->count()){
				$rate = new Rating();
				$rate->type	= $request['type'];
				$rate->type_id = $request['type_id'];
				$rate->user_id = auth()->user()->id;
				$rate->score = $request['score'];
				$rate->save();
				
				$count = Rating::where(['type' => $request['type'],'type_id' => $request['type_id']])->count();
				$rating = Rating::where(['type' => $request['type'],'type_id' => $request['type_id']])->get(['score']);
				$sum = 0;
				foreach($rating as $rate)
				{
					 $sum = $sum + $rate['score'];
				}
				$avg_rating = $sum/$count;
				$result = [
					'success' => true,
					'avg_rating' => $avg_rating
				];
			}
			else{
				$count = Rating::where(['type' => $request['type'],'type_id' => $request['type_id']])->count();
				$rating = Rating::where(['type' => $request['type'],'type_id' => $request['type_id']])->get(['score']);
				$sum = 0;
				foreach($rating as $rate)
				{
					 $sum = $sum + $rate['score'];
				}
				$avg_rating = $sum/$count;
				
				$result = [
					'success' => false,
					'avg_rating' => $avg_rating
				];
			}
			
		}
		catch(\Exception $e)
		{
			$result = [
                'exception_message' => $e->getMessage()
            ];
		}
		return $result;
	}
}
