<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\Http\Requests;
use Illuminate\Http\Request;
use LaravelCaptcha\Lib\Captcha;
use repositories\CommonRepositoryInterface;
use App\Models\User;
use App\Models\EmailTemplate;
use App\AuthenticateUser;
use Mail;
use Input;
use Hash;
use Auth;
use Cookie;
use Flash;

class AuthController extends Controller {
	
	protected $common;

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	use AuthenticatesAndRegistersUsers;
	
	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @return void
	 */
	public function __construct(Guard $auth, Registrar $registrar,CommonRepositoryInterface $common)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;
		$this->common = $common;
		$this->middleware('guest', ['except' => 'getLogout','getSocialredirect']);
		
	}
	
	/**
	  * Render the view to register new users.
      * @param int $id            
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 21/8/2015
	**/

	public function getRegister()
	{
		try
		{
			// CommonRepository file is used here to fetch states and countries.
			$states = $this->common->getStates();
			$countries = $this->common->getCountries();
			return view('auth.register',compact('states','countries'), ['captcha' => (new Captcha)->html()]);
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
		
	}
	
	/**
	  * Save registration form data into database.
      * @param int $id            
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 21/8/2015
	**/
	
	public function postRegister(Requests\AddNewUser $request)
	{
		try
		{
			//Calculating age from user's date of birth
			$dob= $request->dob;
			$date= explode('-', $dob);
			$current_date = date('Y');
			$age= $current_date-$date[0];
			// Check Age Limit
			if($age>18)
			{
				$confirmation_code= str_random(30);
				$user=new User($request->all());
				$user->password= Hash::make($user->password);
				$user->confirmation_code= $confirmation_code;
				$user->role= '3';
				
				//Define dynamic email template for use in confirmation mail.
				$template=EmailTemplate::find('55dd47101d7859cc24000029');
				$link="<a href='http:localhost:81/siteconcept/register/verify/$user->confirmation_code'>Click here</a>";
				$find=array('@username@','@email@','@password@','@Click here@');
				$values=array($user->username,$user->email,$request->password,$link);
				$body=str_replace($find,$values,$template->content);
				if($user->save())
				{
					//Sending Mail...
					Mail::send('emails.verify', array('content'=>$body), function($message) use($template)
					{
						$message->to(Input::get('email'))
								->subject($template->subject);
								
					});
					Flash::success('Thanks for signing up. Your account activation link has been sent to your email.');
				}
				else
				{
				Flash::error('Sorry, something went wrong');
				}
			}
			else
			{
				Flash::error('Sorry, your age is below 18 years');
			}
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
		return redirect('/auth/register');
	}
	
	/**
	  * Render view of login page.
      * @param int $id            
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 21/8/2015
	**/
	
	public function getLogin()
	{
		try
		{
			return view('auth.login');
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
	}
	
	/**
	  * Verify that user is authenticated or not.
      * @param int $id            
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 21/8/2015
	**/
	
	public function postLogin(Requests\AuthenticateUser $request)
	{
		try
		{
			$credentials = $request->only('email', 'password');
			$credentials['status'] = 1;
			$credentials['role'] = '3';
			$remember = (Input::has('remember')) ? true : false;
			if($remember == "on")
			{
				
				if ($this->auth->attempt($credentials))
				{
					//Generate cookies.
					return redirect('user/dashboard')->withCookie(cookie('email', $request->email,60))->withCookie(cookie('password', $request->password,60));
				}
			}
			else
			{
				if ($this->auth->attempt($credentials))
				{
					//Forget cookies.
					$cookie_email = Cookie::forget('email');
					$cookie_password = Cookie::forget('password');
					
					return redirect('user/dashboard')->withCookie($cookie_email)->withCookie($cookie_password);
					
				}
			}
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
		//Redirect after successful login.
		return redirect($this->loginPath())
					->withInput($request->only('email', 'remember'))
					->withErrors([
						'email' => $this->getFailedLoginMessage(),
					]);
	}
	
	/**
	  * Log out the user from dashboard.
      * @param int $id            
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 21/8/2015
	**/
	
	public function getLogout()
	{
		try
		{
			$this->auth->logout();
			$val1 = Cookie::get('email');
			$val2= Cookie::get('password');
			response()->view('/auth/login')->withCookie(cookie('email', $val1))->withCookie(cookie('password', $val2));		
			return redirect('/auth/login');
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
	}
	
	/**
	  * This method calls when user trying to login with social media.
      * @param int $id            
      * @return Response
	  * Created on: 07/9/2015
	  * Updated on: 07/9/2015
	**/
	
	public function sociallogin(AuthenticateUser $authenticateUser, Request $request, $provider) 
	{
		try
		{
			return $authenticateUser->execute($request, $this, $provider);
		}	
		catch (\Exception $e) 
		{ 
            abort(404);
        }
	}
	
	/**
	  * Redirect logged in users from social media popup.
      * @param int $id            
      * @return Response
	  * Created on: 07/9/2015
	  * Updated on: 07/9/2015
	**/

	public function userHasLoggedIn($type) 
	{
		try
		{
			return redirect('/home/socialredirect/'.$type);
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
	}

}
