<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;
use App\Http\Requests;
use Illuminate\Http\Request;
use Mail;
use Hash;
use Input;
use App\Models\User;
use App\Models\EmailTemplate;
use Illuminate\Auth\Passwords\TokenRepositoryInterface;

class PasswordController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Password Reset Controller
	|--------------------------------------------------------------------------
	|
	| This controller is responsible for handling password reset requests
	| and uses a simple trait to include this behavior. You're free to
	| explore this trait and override any methods you wish to tweak.
	|
	*/

	use ResetsPasswords;

	/**
	 * Create a new password controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\PasswordBroker  $passwords
	 * @param  \Illuminate\Auth\Passwords\TokenRepositoryInterface  $tokens
	 * @return void
	 */
	public function __construct(TokenRepositoryInterface $tokens,Guard $auth, PasswordBroker $passwords)
	{
		$this->auth = $auth;
		$this->passwords = $passwords;
		$this->tokens = $tokens;
		$this->middleware('guest');
	}
	
	public function postEmail(Request $request)
	{
		try
		{
			switch ($response = $this->sendResetLink($request->only('email')))
			{
				case PasswordBroker::INVALID_USER:
					return redirect()->back()->withErrors(['email' =>trans($response)]);
		
				case PasswordBroker::RESET_LINK_SENT:
					flash()->success('Reset Link has been sent to your Email Address');
					return redirect()->back()->with('status', trans($response));
			}
			return Redirect('auth/reset');
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
	}
	
	/**
	  * Send reset link to registered user.
      * @param int $id            
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 21/8/2015
	**/
	
	public function sendResetLink(array $credentials)
	{
		try
		{
			$user = $this->passwords->getUser($credentials);
			if (is_null($user))
			{
				return PasswordBroker::INVALID_USER;
			}
			$token = $this->tokens->create($user);
			$this->emailResetLink($user, $token);
			return PasswordBroker::RESET_LINK_SENT;
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
	}
	
	/**
	  * Send dynamic email template to user for reset the password
      * @param int $id            
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 21/8/2015
	**/
	
	public function emailResetLink($user, $token)
	{
		try
		{
			$template=EmailTemplate::find('55dea2771d78596c0200002d');
			$link="<a href='http:localhost:81/siteconcept/password/reset/$token'>Click here</a>";
			$find=array('@Click here@');
			$values=array($link);
			$body=str_replace($find,$values,$template->content);

			return Mail::send('emails.password', array('content'=>$body), function($m) use($template)
			{
				$m->to(Input::get('email'))
					->subject($template->subject);
			});
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
	}
	
	/**
	  * Reset the password.
      * @param int $id            
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 21/8/2015
	**/
	
	public function postReset(Request $request)
	{
		$this->validate($request, [
				'token' => 'required',
				'email' => 'required|email',
				'password' => 'required|confirmed',
			]);
		try
		{
			$credentials = $request->only(
				'email', 'password', 'password_confirmation', 'token'
			);
			$response = $this->passwords->reset($credentials, function($user, $password)
			{
				$user->password = Hash::make($password);
				if($user->save())
				{
					flash()->success('Password Updated Successfully');
				}
				else
				{
					flash()->error('Sorry, password can not be updated. Please try again.');
				}
				return Redirect('/auth/login');
			});

			switch ($response)
			{
				case PasswordBroker::PASSWORD_RESET:
					return redirect($this->redirectPath());

				default:
					return redirect()->back()
								->withInput($request->only('email'))
								->withErrors(['email' => trans($response)]);
			}
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
	}
}
