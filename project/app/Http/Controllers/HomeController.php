<?php namespace App\Http\Controllers;

use App\Models\Announcement;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	public function __construct()
	{
		$this->middleware('auth', ['except' =>'getSocialredirect']);
	}

	/**
	  * Show the application dashboard to the user.
      * @param int $id            
      * @return Response
	  * Created on: 1/9/2015
	  * Updated on: 8/9/2015
	**/
	public function index()
	{
		try
		{
			$announcements=Announcement::where('status','=','1')->latest()->take(5)->get();
			return view('home',compact('announcements'));
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
	}
	
	/**
	  * Redirect user to application dashboard from social media.
      * @param int $id            
      * @return Response
	  * Created on: 2/9/2015
	  * Updated on: 3/9/2015
	**/
	
	public function getSocialredirect($type)
	{
		try
		{
			flash()->success('You are successfully Logged in'); 
			$url = url('/home');
			return view('auth.socialredirect',compact('url','type'));
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
	}

	
}
