<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use repositories\CommonRepositoryInterface;
use Illuminate\Http\Request;
use Auth;
use Input;
use App\Models\User;
use App\Models\Story;
use Flash;
use Hash;
use Image;
use App\Models\State;

class UserController extends Controller {
	
	/*
	|--------------------------------------------------------------------------
	| User Controller
	|--------------------------------------------------------------------------
	|
	| This controller manages user's profile.
	|
	*/
	
	protected $common;
	
	public function __construct(CommonRepositoryInterface $common)
	{
		$this->middleware('auth',['except' =>'postState']);
		$this->common = $common;
	}
	
	/**
	  * By default controller calls this method.
      * @param int $id            
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 21/8/2015
	**/
	
	public function index()
	{
        abort(404);
	}
	
	/**
	  * Render view of application dashboard.
      * @param int $id            
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 21/8/2015
	**/

	public function getDashboard()
	{
		try
		{
			return view('user/dashboard');
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
		
	}
	
	/**
	  * Fetches the states for a specific country.
      * @param int $id            
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 21/8/2015
	**/
	
	public function postState(Request $request)
	{ 
		try
		{
			$country=(int)$request->cid;
			$states = State::where('country_id',$country)->lists('name', '_id');
			return view('user.getStates', compact('states'));
		}
		catch (\Exception $e) 
		{ 
			return response()->json(['success'=>false]);
		}	
	}
	
	/**
	  * Render the view to edit the user profile.
      * @param int $id            
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 21/8/2015
	**/
	
	public function getEditProfile()
	{
		try
		{
			$id = Auth::user()->id;
			$user = User::findOrfail($id);
			// CommonRepository file is used here to fetch states and countries.
			$states = $this->common->getStates();
			$countries = $this->common->getCountries();
			return view('user/edit-profile', compact('states','countries','user'));
		}
		catch (\Exception $e) 
		{ 
			abort(404);
		}
		
	}
	
	/**
	  * Save Updated data in database.
      * @param int $id            
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 21/8/2015
	**/
	
	public function postEditProfile(Requests\EditUser $request, $id)
	{
		try
		{
			$dob= $request->dob;
			$date= explode('-', $dob);
			$current_date = date('Y');
			$age= $current_date-$date[0];
			// Check Age Limit
			if($age>18)
			{
				$img = Input::file('photo');
				$image = $img->getClientOriginalName();
				$path = 'uploads/' . $image;
				//Resizing image
				$editImage=Image::make($img->getRealPath())->resize(200, 200)->save($path);
				$user=User::find($id);
				$user->update($request->all());
				$user->photo = $image;
				$user->save();
				if($user->save())
				{
					Flash::success('Profile Updated Successfully');
				}
				else
				{
					Flash::error('Sorry, Profile can not be updated');
				}
			}
			else
			{
				Flash::error('Sorry, profile not updated as your age is below 18 years');
			}
		}
		catch (\Exception $e) {
            $result = [
                'exception_message' => $e->getMessage()
            ];
        }
		return response()->json(['redirect_url' => 'user/dashboard']);
	}
	
	/**
	  * Render the view to change password.
      * @param int $id            
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 21/8/2015
	**/
	
	public function getChangePassword()
	{
		try
		{
			return view('user/change-password');
		}
		catch (\Exception $e) 
		{ 
			abort(404);
		}
	}
	
	/**
	  * Save updated password in database.
      * @param int $id            
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 21/8/2015
	**/
	
	public function postChangePassword(Requests\ResetPassword $request)
	{
		try
		{
			// Check that Old Password entered is correct or not.
			if(Hash::check($request->old_password, Auth::user()->password))
			{
				$id = Auth::user()->id;
				$user = User::find($id);
				$user->password=Hash::make($request->password);
				$user->save();
				if($user->save())
				{
					Flash::success('Password Updated successfully');
					return redirect('user/dashboard');
				}
				else
				{
					Flash::error('Sorry, Password can not be changed');
					return redirect('user/change-password');
				}
			}
			else
			{
				Flash::error('Sorry, your current password is not correct');
				return redirect('user/change-password');
			}
		}
		catch (\Exception $e) 
		{ 
			abort(404);
		}
	}	
}
