<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class RegistrationController extends Controller {
	
	/*
	|--------------------------------------------------------------------------
	| Registration Controller
	|--------------------------------------------------------------------------
	|
	| This controller activates the user account and completes the Registration process.
	| Created on: 11/8/2015
	| Updated on: 31/8/2015
	*/
	
	public function confirm($confirmation_code)
    {
		try
		{
			// Check whether confirmation code exists or not.
			if( ! $confirmation_code)
			{
				return 'Whoops, this is an invalid confirmation code....please try again';
			}
			
			$user = User::whereConfirmationCode($confirmation_code)->first();
			// Check whether user exists or not.
			if ( ! $user)
			{
				 return 'Whoops, looks like this is an invalid user....please try again';
			}

			$user->status = 1;
			$user->confirmation_code = null;
			$user->save();
			if($user->role == '2')
			{
				flash()->success('Your email has been confirmed');
				return redirect('admin/auth/login');
			}
			elseif($user->role == '3')
			{
				flash()->success('Your email has been confirmed');
				return redirect('auth/login'); 
			}
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
    }

}
