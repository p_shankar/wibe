<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\SubCategory;
use App\Http\Requests\CategoryRequest;

class CategoryController extends Controller
{

    /**
     * Display a listing of all categories.
     *
     * @return Response
	 * Created on: 15/9/2015
	 * Updated on: 15/9/2015
     */
    public function index()
    {
		try
		{
			$active = 'category';
			$category = Category::orderBy('id', 'DESC')->paginate(10);
			$category->setPath('category');
			return view('admin.category.index', compact('category','active'));
		}
        catch (\Exception $e) 
		{ 
            abort(404);
        }
    }

    /**
     * Show the form for creating a new category.
     *
     * @return Response
	 * Created on: 15/9/2015
	 * Updated on: 15/9/2015
     */
    public function create()
    {
		try
		{
			$active = 'category';
			$subcategories=SubCategory::where('status','1')->lists('name','id');
			return view('admin.category.create',compact('active','subcategories'));
		}
        catch (\Exception $e) 
		{ 
           echo $e->getMessage(); //abort(404);
        }
    }

    /**
     * Store a newly created category in database.
     *
     * @return Response
	 * Created on: 15/9/2015
	 * Updated on: 15/9/2015
     */
    public function store(CategoryRequest $request)
    {
		try
		{
			$category = Category::create($request->all());
			$attributes=$request->variable;
			$count = 0;
			foreach($attributes as $attribute)
			{
				if($attribute){
					$object = new SubCategory(['name' => $attribute]);
					$category->subcategories()->save($object);
				}
				
				$count = 1;
			}
			if($count==1)
			{
				flash()->success('Category has been created!!');
			}
			else
			{
				flash()->error('Sorry! Something went wrong.');
			}
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
		return redirect('admin/category');
        
    }

    /**
     * Show the form for editing the specified category.
     *
     * @param int $id            
     * @return Response
	 * Created on: 15/9/2015
	 * Updated on: 15/9/2015
     */
    public function edit($id)
    {
		try
		{
			$active = 'category';
			$category = Category::findOrfail($id);
			$subcategory=$category->subcategories;
			return view('admin.category.edit', compact('category','subcategory','active'));
		}
        catch (\Exception $e) 
		{ 
            abort(404);
        }
    }

    /**
     * Update the specified category in database.
     *
     * @param int $id            
     * @return Response
	 * Created on: 15/9/2015
	 * Updated on: 15/9/2015
     */
    public function update($id, CategoryRequest $request)
    {
		try
		{
			$category = Category::findOrfail($id);
			if($category->update($request->all()))
			{
				flash()->success('Category has been updated!!');
			}
			else
			{
				flash()->error('Sorry! Something went wrong.');
			}
			
			
		}
        catch (\Exception $e) 
		{ 
            abort(404);
        }
		return redirect('admin/category');
    }

    /**
     * Remove the specified category from database.
     *
     * @param int $id            
     * @return Response
	 * Created on: 15/9/2015
	 * Updated on: 15/9/2015
     */
    public function destroy($id)
    {
		try
		{
			if(Category::destroy($id))
			{
				flash()->success('Category has been deleted!!');
			}
			else
			{
				flash()->error('Sorry! Something went wrong.');
			}
			
		}
        catch (\Exception $e) 
		{ 
            abort(404);
        }
        return redirect('admin/category');
    }
}
