<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use repositories\CommonRepositoryInterface;
use App\Models\Story;
use App\Models\Like;
use Auth;
use DB;
use Input;
use Image;
use App\Http\Requests\SubmitStory;
use App\Http\Requests\EditStory;
use Request as AjaxRequest;

class StoryController extends Controller
{
	protected $common;
	
	public function __construct(CommonRepositoryInterface $common)
	{
		$this->common = $common;
	}
	
    /**
	  * Show the listing of all stories.           
      * @return Response
	  * Created on: 15/9/2015
	  * Updated on: 21/9/2015
	**/
    public function index(Request $request)
    { 
		try
		{	
			$active = 'story';
			$stories = Story::orderBy('_id', 'DESC')->whereNotIn('status',['5'])->paginate(10);
			// CommonRepository file is used here to fetch categories and subcategories.
			$categories=$this->common->getCategories();
			$subcategories=$this->common->getSubcategories();
			
			if(AjaxRequest::ajax()) 
			  {
				//Filter by category and sub-category

				//Fetch records filtered by category only
				if(isset($request['category']) && empty($request['subcategory']))
				{ 
					$category = $request['category'];
					$subcategory = $request['subcategory'];
					$stories = Story::where('category_id','=',$request['category'])->paginate(10)->setPath('story');
				}
				//Fetch records filtered by both category and sub-category
				elseif(isset($request['category'], $request['subcategory']))
				{	
					$category = $request['category'];
					$subcategory = $request['subcategory'];
					$stories = Story::where(array('category_id' => $request['category'],'subcategory_id' => $request['subcategory']))->paginate(10)->setPath('story');
				}
				//Fetch records when filter is not applied
				else
				{
					$category = '';
					$subcategory = '';
					$stories = Story::orderBy('_id', 'DESC')->paginate(10);
					return view('admin.story.ajax_stories', compact('stories','category','subcategory'));
				}
				return view('admin.story.ajax_stories', compact('stories','category','subcategory'));
			  }
			  $stories->setPath('story');
			return view('admin.story.index', compact('stories','categories','subcategories','active'));
		}
        catch (\Exception $e) 
		{ 
			$e->getMessage();
            abort(404);
        }
    }

    /**
	  * Render the view to create a new story.            
      * @return Response
	  * Created on: 15/9/2015
	  * Updated on: 215/9/2015
	**/
    public function create()
    {
		try
		{
			$active = 'story';
			// CommonRepository file is used here to fetch categories and subcategories.
			$categories=$this->common->getCategories();
			$subcategories=$this->common->getSubcategories();
			return view('admin.story.create',compact('categories','subcategories','active'));
		}
        catch (\Exception $e) 
		{ 
            abort(404);
        }
    }

    /**
	  * Save story in database.            
      * @return Response
	  * Created on: 15/9/2015
	  * Updated on: 15/9/2015
	**/
    public function store(SubmitStory $request)
    {
		try
		{
			$id = Auth::user()->id;
			$image = Input::file('image')->getClientOriginalName();
			$path = 'uploads/' . $image;
			//Resizing the image
			Image::make(Input::file('image')->getRealPath())->resize(200, 200)->save($path);
			$story = new Story($request->all());
			$story->author_id = $id;
			$story->image = $image;
			if($story->save())
			{
				flash()->success('Story has been created!!');
			}
			else
			{
				flash()->error('Sorry! Something went wrong.');
			}
		}
        catch (\Exception $e) 
		{ 
            abort(404);
        }
        return redirect('admin/story');
    }
	
	/**
	  * Show the story detail page.  
	  * @param int $alias	  
      * @return Response
	  * Created on: 15/9/2015
	  * Updated on: 22/9/2015
	**/
	
	public function show(Request $request,$alias)
	{
		try
		{
			$active = 'story';
			$story=Story::where(['alias' => $alias])->get();
			if(AjaxRequest::ajax()) 
			{
				//find story by id
				$story=Story::find($request['id']);
				
				//get number of likes of a specified story
				$total_likes=$story->likes()->paginate(10);
				
				//get number of votes of a specified story
				$total_votes=$story->votes()->paginate(10);
				
				//get number of followers of a specified story
				$total_followers=$story->followers()->paginate(10);
				
				//get number of comments of a specified story
				$total_comments=$story->comments()->paginate(10);
				$type=$request['type'];
				return view('admin.story.story_detail',compact('story','data','type','total_likes','total_votes','total_followers','total_comments'));
			}
			return view('admin.story.show',compact('story','active','data'));
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
	}

    /**
	  * Render the view to edit a specified story. 
	  * @param int $id	  
      * @return Response
	  * Created on: 15/9/2015
	  * Updated on: 15/9/2015
	**/
    public function edit($id)
    {
		try
		{
			$active = 'story';
			$story = Story::findOrfail($id);
			// CommonRepository file is used here to fetch categories and subcategories.
			$categories=$this->common->getCategories();
			$subcategories=$this->common->getSubcategories();
			return view('admin.story.edit', compact('categories','subcategories','story','active'));
		}
        catch (\Exception $e) 
		{ 
            abort(404);
        }
    }

    /**
	  * Save the updated story in database.  
	  * @param int $id
      * @return Response
	  * Created on: 15/9/2015
	  * Updated on: 15/9/2015
	**/
    public function update(EditStory $request, $id)
    {
		try
		{
			$story = Story::findOrfail($id);
			if($story->update($request->all()))
			{
				flash()->success('Story has been updated!!');
			}
			else
			{
				flash()->error('Sorry! Something went wrong.');
			}
		}
        catch (\Exception $e) 
		{ 
            abort(404);
        }
        return redirect('admin/story');
    }

    /**
	  * Delete multiple records from database.            
      * @return Response
	  * Created on: 22/9/2015
	  * Updated on: 23/9/2015
	**/
    public function postDestroy(Request $request)
    {
		try
		{
			$ids = explode(',',$request->id);
			if(count($ids)>1)
				$story_delete=DB::table($request->table)->whereIn('_id', $ids)->delete();
			else
				$story_delete=DB::table($request->table)->whereIn('_id', $ids)->delete();
			
			if($story_delete)
			{
				flash()->success('Records deleted!!');
				return response()->json(['success'=>true,'redirect_url' => url('admin/story')]);
			}				
			else
			{
				flash()->error('Something went wrong!!');
				return response()->json(['success'=>false,'redirect_url' => url('admin/story')]);		
			}
		}
		catch(\Exception $e)
		{
			flash()->error('Something went wrong!!');
			return response()->json(['success'=>false,'redirect_url' => url('admin/story')]); 
		}
    }
	
	/**
	  * Delete the specified story from database. 
	  * @param int $id
      * @return Response
	  * Created on: 15/9/2015
	  * Updated on: 15/9/2015
	**/
	
	public function destroy($id)
	{
		try
		{
			$story = Story::findOrfail($id);
			if($story->update(['status' => '5']))
			{
				flash()->success('Story has been deleted!!');
			}
			else
			{
				flash()->error('Sorry! Something went wrong.');
			}
		}
        catch (\Exception $e) 
		{ 
            abort(404);
        }
        return redirect('admin/story');
	}
	
	/**
	  * Change the hide status of story.            
      * @return Response
	  * Created on: 1/10/2015
	  * Updated on: 1/10/2015
	**/
	
	public function postChangeStatus()
    { 
        try {
            $request = Input::all();
			$story_status=DB::table($request['table'])->where(array('_id'=>$request['id']));
			if($request['status'] == 0)
			{
				$story_status->update([
					'status' => '4'
				]);
			}
			else{
				$story_status->update([
					'status' => '2'
				]);
			}
			
            $result = [
                'success' => true
            ];
        } catch (\Exception $e) {
            $result = [
                'exception_message' => $e->getMessage()
            ];
        }
        return $result;
    }
	
	/**
	  * Fetch stories by status.            
      * @return Response
	  * Created on: 1/10/2015
	  * Updated on: 1/10/2015
	**/
	public function getFetch(Request $request)
    {
		try
		{
			if($request['type'] == 'All')
			{
				$stories = Story::whereNotIn('status',['5'])->paginate(10);
			}
			elseif($request['type'] == 'Draft')
			{
				$stories = Story::where('status','1')->paginate(10);
			}
			elseif($request['type'] == 'Review')
			{
				$stories = Story::where('status','2')->paginate(10);
			}
			elseif($request['type'] == 'Publish')
			{
				$stories = Story::where('status','3')->paginate(10);
			}
			elseif($request['type'] == 'Delete')
			{
				$stories = Story::where('status','5')->paginate(10);
			}
			else
			{
				$stories = Story::where('status','4')->paginate(10);
			}
			$category = '';
			$subcategory = '';
			return view('admin.story.ajax_stories',compact('stories','active','category','subcategory'));
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
	}
}

