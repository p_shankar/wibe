<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use repositories\CommonRepositoryInterface;
use App\Models\SubCategory;
use App\Http\Requests\SubCategoryRequest;
use Request as AjaxRequest;

class SubCategoryController extends Controller
{
	protected $common;
	
	public function __construct(CommonRepositoryInterface $common)
	{
		$this->common = $common;
	}

    /**
     * Display a listing of the Sub-categories.
     *
     * @return Response
	 * Created on: 15/9/2015
	 * Updated on: 15/9/2015
     */
    public function index(Request $request)
    {
		try
		{
			$active = 'sub-category';
			$subcategory = SubCategory::orderBy('id', 'DESC')->paginate(10);
			$subcategory->setPath('subcategory');
			// CommonRepository file is used here to fetch categories.
			$categories=$this->common->getCategories();
			if(AjaxRequest::ajax()) 
			{
				if(isset($request['category']))
				{
					$subcategory = SubCategory::where('category_id','=',$request['category'])->paginate(10)->setPath('subcategory');
					return view('admin.subcategory.ajax_subcategory', compact('subcategory'));
				}
			}
			return view('admin.subcategory.index', compact('subcategory','categories','active'));
		}
        catch (\Exception $e) 
		{ 
           abort(404);
        }
    }

    /**
     * Show the form for creating a new sub-category.
     *
     * @return Response
	 * Created on: 15/9/2015
	 * Updated on: 15/9/2015
     */
    public function create()
    {
		try
		{
			$active = 'sub-category';
			$categories=Category::lists('type_name','_id');
			return view('admin.subcategory.create',compact('active','categories'));
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
    }

    /**
     * Store a newly created sub-category in database.
     *
     * @return Response
	 * Created on: 15/9/2015
	 * Updated on: 15/9/2015
     */
    public function store(SubCategoryRequest $request)
    {
		try
		{
			if(SubCategory::create($request->all()))
			{
				flash()->success('Sub-Category has been created!!');
			}
			else
			{
				flash()->error('Sorry! Something went wrong.');
			}
		}
        catch (\Exception $e) 
		{ 
            abort(404);
        }
		return redirect('admin/subcategory');
    }

    /**
     * Show the form for editing the specified sub-category.
     *
     * @param int $id            
     * @return Response
	 * Created on: 15/9/2015
	 * Updated on: 15/9/2015
     */
    public function edit($id)
    {
		try
		{
			$active = 'sub-category';
			$categories=Category::lists('type_name','id');
			$subcategory = SubCategory::findOrfail($id);
			return view('admin.subcategory.edit', compact('subcategory','categories','active'));
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
    }

    /**
     * Update the specified sub-category in database.
     *
     * @param int $id            
     * @return Response
	 * Created on: 15/9/2015
	 * Updated on: 15/9/2015
     */
    public function update($id, SubCategoryRequest $request)
    {
		try
		{
			$subcategory = SubCategory::findOrfail($id);
			if($subcategory->update($request->all()))
			{
				flash()->success('Sub-Category has been updated!!');
			}
			else
			{
				flash()->error('Sorry! Something went wrong.');
			}
		}
        catch (\Exception $e) 
		{ 
            abort(404);
        }
		return redirect('admin/subcategory');
    }

    /**
     * Remove the specified sub-category from database.
     *
     * @param int $id            
     * @return Response
	 * Created on: 15/9/2015
	 * Updated on: 15/9/2015
     */
    public function destroy($id)
    {
		try
		{
			if(SubCategory::destroy($id))
			{
				flash()->success('Sub-Category has been deleted!!');
			}
			else
			{
				flash()->error('Sorry! Something went wrong.');
			}
		}
        catch (\Exception $e) 
		{ 
            abort(404);
        }
		return redirect('admin/subcategory');
    }
}
