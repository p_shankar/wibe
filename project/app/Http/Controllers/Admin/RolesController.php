<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Role;
use Illuminate\Http\Request;
use App\Models\Permission;
use App\Models\PermissionRole;

class RolesController extends Controller {
	
	/**
     * Display a listing of all roles.
     *
     * @return Response
	 * Created on: 30/9/2015
	 * Updated on: 30/9/2015
     */
	 
	public function getIndex()
	{
		try
		{
			$active = 'roles';
			$roles = Role::orderBy('name')->paginate(10);
			return view('admin.roles.index',compact('active','roles'));
		}
		catch (\Exception $e) 
		{ 
            echo $e->getMessage();//abort(404);
        }
	}
	
	/**
     * Show the form for creating a new role.
     *
     * @return Response
	 * Created on: 30/9/2015
	 * Updated on: 30/9/2015
     */
	
	public function getCreate()
	{
		try
		{
			$active = 'roles';
			$permissions = Permission::orderBy('id')->get();
			return view('admin.roles.create',compact('active','permissions'));
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
	}
	
	/**
     * Store a newly created role in database.
     *
     * @return Response
	 * Created on: 30/9/2015
	 * Updated on: 30/9/2015
     */
	
	public function postCreate(Requests\RolesRequest $request)
	{
		try
		{
			$allInputs = $request->all();
			$createId = Role::create($allInputs);
			if(!empty($createId))
			{
				//save Permissions
				if(!empty($allInputs['permission'])){
					foreach($allInputs['permission'] as $perm => $per_id){
						$permision['permission_id'] = $per_id;
						$permision['role_id'] = $createId['id'];
						PermissionRole::create($permision);
					}
				}
				flash()->success('New Role has been added!!');
			}
			else
			{
				flash()->error('Sorry! Something went wrong.');
			}
		}
        catch (\Exception $e) 
		{ 
            abort(404);
        }
        return redirect('admin/roles');
	}
	
	/**
     * Show the form for editing the specified role.
     *
     * @param int $id            
     * @return Response
	 * Created on: 30/9/2015
	 * Updated on: 30/9/2015
     */
    public function getEdit($id)
    {
		try
		{
			$active = 'roles';
			$role = Role::findOrfail($id);
			$permissions = Permission::orderBy('id')->get();
			$rolePermissions = $role->permission_role->toArray();
			//make a one-dimensional array of role permissions so that we can easily search for permission id's and mark its corresponding checkbox as checked.
			$rolePer = array();
			foreach($rolePermissions as $rol){
				$rolePer[] = $rol['permission_id'];
			}
			
			return view('admin.roles.edit', compact('role','active','permissions','rolePer'));
		}
        catch (\Exception $e) 
		{ 
            echo $e->getMessage();//abort(404);
        }
    }
	
	/**
     * Update the specified role in database.
     *
     * @param int $id            
     * @return Response
	 * Created on: 30/9/2015
	 * Updated on: 30/9/2015
     */
    public function postEdit($id, Requests\RolesRequest $request)
    {
		try
		{
			$role = Role::findOrfail($id);
			
			$allInputs = $request->all();
			$UpdateRole = $role->update($request->all());
			if(!empty($UpdateRole))
			{
				//delete previous permissions
				PermissionRole::where('role_id',$id)->delete();
				//save Permissions
				if(!empty($allInputs['permission'])){
					foreach($allInputs['permission'] as $perm => $per_id){
						$permision['permission_id'] = $per_id;
						$permision['role_id'] = $id;
						PermissionRole::create($permision);
					}
				}
				flash()->success('New Role has been added!!');
			}
			
			
			
			
			
			else
			{
				flash()->error('Sorry! Something went wrong.');
			}
		}
        catch (\Exception $e) 
		{ 
            abort(404);
        }
        return redirect('admin/roles');
    }
	
	/**
     * Remove the specified role from database.
     *
     * @param int $id            
     * @return Response
	 * Created on: 15/9/2015
	 * Updated on: 15/9/2015
     */
    public function postDelete($id)
    {
		try
		{
			if(Role::destroy($id))
			{
				flash()->success('Role has been deleted!!');
			}
			else
			{
				flash()->error('Sorry! Something went wrong.');
			}
		}
        catch (\Exception $e) 
		{ 
            abort(404);
        }
        return redirect('admin/roles');
    }

}
