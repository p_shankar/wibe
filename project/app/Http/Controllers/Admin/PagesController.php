<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Page;
use App\Http\Requests\PagesRequest;

class PagesController extends Controller
{

    /**
     * Display a listing of all pages.
     *
     * @return Response
	 * Created on: 15/9/2015
	 * Updated on: 15/9/2015
     */
    public function index()
    {
		try
		{
			$active = 'pages';
			$pages = Page::orderBy('id', 'DESC')->paginate(10);
			$pages->setPath('pages');
			return view('admin.pages.index', compact('pages','active'));
		}
        catch (\Exception $e) 
		{ 
            echo $e->getMessage();//abort(404);
        }
    }

    /**
     * Show the form for creating a new page.
     *
     * @return Response
	 * Created on: 15/9/2015
	 * Updated on: 15/9/2015
     */
    public function create()
    {
		try
		{
			$active = 'pages';
			return view('admin.pages.create',compact('active'));
		}
        catch (\Exception $e) 
		{ 
            echo $e->getMessage();//abort(404);
        }
    }

    /**
     * Store a newly created page in database.
     *
     * @return Response
	 * Created on: 15/9/2015
	 * Updated on: 15/9/2015
     */
    public function store(PagesRequest $request)
    {
		try
		{
			if(Page::create($request->all()))
			{
				flash()->success('Your page has been created!!');
			}
			else
			{
				flash()->error('Sorry! Something went wrong.');
			}
		}
        catch (\Exception $e) 
		{ 
           abort(404);
        }
        return redirect('admin/pages');
    }

    /**
     * Show the form for editing the specified page.
     *
     * @param int $id            
     * @return Response
	 * Created on: 15/9/2015
	 * Updated on: 15/9/2015
     */
    public function edit($id)
    {
		try
		{
			$active = 'pages';
			$page = Page::findOrfail($id);
			return view('admin.pages.edit', compact('page','active'));
		}
        catch (\Exception $e) 
		{ 
            abort(404);
        }
    }

    /**
     * Update the specified page in database.
     *
     * @param int $id            
     * @return Response
	 * Created on: 15/9/2015
	 * Updated on: 15/9/2015
     */
    public function update($id, PagesRequest $request)
    {
		try
		{
			$page = Page::findOrfail($id);
			if($page->update($request->all()))
			{
				flash()->success('Your page has been updated!!');
			}
			else
			{
				flash()->error('Sorry! Something went wrong.');
			}
		}
        catch (\Exception $e) 
		{ 
            abort(404);
        }
        return redirect('admin/pages');
    }

    /**
     * Remove the specified page from database.
     *
     * @param int $id            
     * @return Response
	 * Created on: 15/9/2015
	 * Updated on: 15/9/2015
     */
    public function destroy($id)
    {
		try
		{
			if(Page::destroy($id))
			{
				flash()->success('Page has been deleted!!');
			}
			else
			{
				flash()->error('Sorry! Something went wrong.');
			}
		}
        catch (\Exception $e) 
		{ 
            abort(404);
        }
        return redirect('admin/pages');
    }
}
