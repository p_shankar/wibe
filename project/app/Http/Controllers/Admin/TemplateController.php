<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\EmailTemplate;
use App\Models\EmailTemplateAttribute;
use App\Http\Requests\EmailTemplateRequest;
use Illuminate\Http\Request;

class TemplateController extends Controller {
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		try
		{
			$active = 'template';
			$templates = EmailTemplate::orderBy('id', 'DESC')->paginate(10);
			$templates->setPath('template');
			return view('admin.template.index', compact('templates','active'));
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		try
		{
			$active = 'template';
			return view('admin.template.create',compact('active'));
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(EmailTemplateRequest $request)
	{
		try
		{
			$template = EmailTemplate::create($request->all());
			$attributes=$request->variable;
			$count = 0;
			foreach($attributes as $attribute)
			{
				$object = new EmailTemplateAttribute(['variable' => $attribute]);
				$template->template_attributes()->save($object);
				$count = 1;
			}
			if($count==1)
			{
				flash()->success('Template has been created!!');
			}
			else
			{
				flash()->success('Template can not be created!!');
			}
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
		return redirect('admin/template');
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		try
		{
			$active = 'template';
			$template = EmailTemplate::findOrfail($id);
			$attributes=$template->template_attributes->lists('variable','variable');
			return view('admin.template.edit', compact('template','attributes','active'));
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, EmailTemplateRequest $request)
	{
		try
		{
			$template = EmailTemplate::findOrfail($id);
			if($template->update($request->all()))
			{
				flash()->success('Template has been updated!!');
			}
			else
			{
				flash()->error('Template can not be updated!!');
			}
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        } 
		return redirect('admin/template');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		try
		{
			if(EmailTemplate::destroy($id))
			{
				flash()->success('Template has been deleted!!');
			}
			else
			{
				flash()->error('Template can not be deleted!!');
			}
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
		return redirect('admin/template');
	}

}
