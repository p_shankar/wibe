<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Announcement;
use App\Models\User;
use App\Http\Requests\AnnouncementRequest;
use Mail;

class AnnouncementController extends Controller {

    /**
     * Display a listing of the announcement.
     *
     * @return Response
	 * Created on: 15/9/2015
	 * Updated on: 15/9/2015
     */
    public function index()
    {
		try
		{
			$active = 'announcement';
			$announcement = Announcement::orderBy('_id', 'DESC')->paginate(10);
			$announcement->setPath('announcement');
			return view('admin.announcement.index', compact('announcement','active'));
		}
        catch (\Exception $e) 
		{ 
            abort(404);
        }
    }

    /**
     * Show the form for creating a new announcement.
     *
     * @return Response
	 * Created on: 15/9/2015
	 * Updated on: 15/9/2015
     */
    public function create()
    {
		try
		{
			$active = 'announcement';
			return view('admin.announcement.create',compact('active'));
		}
        catch (\Exception $e) 
		{ 
            abort(404);
        }
    }

    /**
     * Store a newly created announcement in storage.
     *
     * @return Response
	 * Created on: 15/9/2015
	 * Updated on: 15/9/2015
     */
    public function store(AnnouncementRequest $request)
    {
		try
		{
			$announcements = Announcement::create($request->all());
			$users=User::where(array('role'=>'2'))->orWhere(array('role'=>'3'))->lists('email');
			//Send email to all users about new announcement
			Mail::send('emails.verify', array('content'=>$announcements->description), function($message) use($users,$announcements)
					{
						foreach($users as $user)
						{
							$message->to($user)
									->subject($announcements->title);
						} 					
					});
			flash()->success('Announcement has been created!!');
			return redirect('admin/announcement');
		}
        catch (\Exception $e) 
		{ 
            abort(404);
        }
    }

    /**
     * Show the form for editing the specified announcement.
     *
     * @param int $id            
     * @return Response
	 * Created on: 15/9/2015
	 * Updated on: 15/9/2015
     */
    public function edit($id)
    {
		try
		{
			$active = 'announcement';
			$announcement = Announcement::findOrfail($id);
			return view('admin.announcement.edit', compact('announcement','active'));
		}
        catch (\Exception $e) 
		{ 
            abort(404);
        }
    }

    /**
     * Update the specified announcement in storage.
     *
     * @param int $id            
     * @return Response
	 * Created on: 15/9/2015
	 * Updated on: 15/9/2015
     */
    public function update($id, AnnouncementRequest $request)
    {
		try
		{
			$announcement = Announcement::findOrfail($id);
			if($announcement->update($request->all()))
			{
				flash()->success('Announcement has been updated!!');
			}
			else
			{
				flash()->error('Sorry! Something went wrong.');
			}
		}
        catch (\Exception $e) 
		{ 
            abort(404);
        }
		return redirect('admin/announcement');
    }

    /**
     * Remove the specified announcement from storage.
     *
     * @param int $id            
     * @return Response
	 * Created on: 15/9/2015
	 * Updated on: 15/9/2015
     */
    public function destroy($id)
    {
		try
		{
			if(Announcement::destroy($id))
			{
				flash()->success('Announcement has been deleted!!');
			}
			else
			{
				flash()->error('Sorry! Something went wrong.');
			}
		}
        catch (\Exception $e) 
		{ 
            abort(404);
        }
		return redirect('admin/announcement');
    }

}
