<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use Illuminate\Http\Request;
use App\Models\Permission;
class AdminPermissionsController extends Controller {

	/*
     * Added on : 23 oct 2015
     * Added by : debut infotech
     * Desc : to manage permissions in admin section.
     * */
     public function getIndex(){
		 try {
			$active = 'abc';
           $permissions = Permission::OrderBy('id','DESC')->paginate(10);
		   return view('admin/permissions/index',compact('permissions','active'));
        } catch (\Exception $e) {
            abort(404);
        }
	 }
	 
 /*
  * Added on : 23 oct 2015
  * Added by : debut infotech
  * Desc : to display add permissions form.
  * */
  
  public function getAddPermissions(){
	  try {
		  $active = 'abc';
		   return view('admin/permissions/add-permission',compact('active'));
        } catch (\Exception $e) {
            echo $e->getMessage();//abort(404);
        }
  }
  
  /*
   * Added on : 23 oct 2015
   * Added by : debut infotech
   * Desc : to save permissions to database.
   * */
   public function postAddPermissions(Request $request){
	    try {
			$active = 'abc';
           
           if(Permission::create($request->all()))
			{
				flash()->success('Permission Added Successfully.!');
			}
			else
			{
				flash()->error('Sorry! Something went wrong.');
			}
			return redirect('admin/permissions');		   
        } catch (\Exception $e) {
          echo $e->getMessage(); // abort(404);
        }
     
   }
   
   /*
   * Added on : 23 oct 2015
   * Added by : debut infotech
   * Desc : to remove permissions from database.
   * */
   public function deleteDeletePermission($id){
	    try
		{
			if(Permission::where('id',$id)->delete())
			{
				flash()->success('Permission has been deleted!!');
			}
			else
			{
				flash()->error('Sorry! Something went wrong.');
			}
			return redirect('admin/permissions');	
		}
        catch (\Exception $e) 
		{ 
			abort(404);
        }
     
   }
   
    /*
   * Added on : 23 oct 2015
   * Added by : debut infotech
   * Desc : to update permissions in database.
   * */
   public function getEditPermission($id){
	   try {
			$active = 'abc';
           $permission = Permission::find($id);
		   return view('admin/permissions/edit',compact('permission','active'));
        } catch (\Exception $e) {
            abort(404);
        }
     
   }
   
    /*
   * Added on : 23 oct 2015
   * Added by : debut infotech
   * Desc : to update permissions in database.
   * */
   public function postEditPermission($id, Request $request){
	   try {
			$active = 'abc';
           $permission = Permission::find($id);
		  if($permission->update($request->all()))
			{
				flash()->success('Permission has been Updated!!');
			}
			else
			{
				flash()->error('Sorry! Something went wrong.');
			}
			return redirect('admin/permissions');
        } catch (\Exception $e) {
            abort(404);
        }
     
   }
}
