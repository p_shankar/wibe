<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;
use App\Http\Requests;
use Illuminate\Http\Request;
use Mail;
use Hash;
use Input;
use App\Models\User;
use App\Models\EmailTemplate;
use Illuminate\Auth\Passwords\TokenRepositoryInterface;

class AdminPasswordController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Password Reset Controller
	|--------------------------------------------------------------------------
	|
	| This controller is responsible for handling password reset requests
	| and uses a simple trait to include this behavior. You're free to
	| explore this trait and override any methods you wish to tweak.
	|
	*/
	
	use ResetsPasswords;
	
	/**
	 * Create a new password controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\PasswordBroker  $passwords
	 * @param  \Illuminate\Auth\Passwords\TokenRepositoryInterface  $tokens
	 * @return void
	 */

	public function __construct(TokenRepositoryInterface $tokens,Guard $auth, PasswordBroker $passwords)
	{
		$this->auth = $auth;
		$this->passwords = $passwords;
		$this->tokens = $tokens;
		$this->middleware('admin.auth');
	}
	
	/**
	  * Render the view to send email for reset password.
      * @param int $id            
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 21/8/2015
	**/
	
	public function getEmail()
	{
		try
		{
			return view('admin/reset_password');
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
	}
	
	/**
	  * Send reset link.
      * @param int $id            
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 21/8/2015
	**/
	
	public function postEmail(Request $request)
	{
		try
		{
			//call sendResetLink function
			switch ($response = $this->sendResetLink($request->only('email')))
			{
				case PasswordBroker::INVALID_USER:
					return redirect()->back()->withErrors(['email' =>trans($response)]);
		
				case PasswordBroker::RESET_LINK_SENT:
					flash()->success('Reset Link has been sent to your Email Address!!');
					return redirect()->back()->with('status', trans($response));
			}
			return Redirect('admin/auth/reset');
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
	}
	
	/**
	  * Send reset link to registered user after verification.
      * @param int $id            
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 21/8/2015
	**/
	
	public function sendResetLink(array $credentials)
	{
		try
		{
			$user = $this->passwords->getUser($credentials);

			if (is_null($user))
			{
				return PasswordBroker::INVALID_USER;
			}
			$token = $this->tokens->create($user);
			//call emailResetLink function
			//$this->emailResetLink($user, $token);
			return PasswordBroker::RESET_LINK_SENT;
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
	}
	
	/**
	  * Send dynamic email template to user for reset the password
      * @param int $id            
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 21/8/2015
	**/

	public function emailResetLink($user, $token)
	{
		try
		{
			return $template=EmailTemplate::find(2);
			$link="<a href='". url('admin/password/reset/'.$token)."'>Click here</a>";
			$find=array('@Click here@');
			$values=array($link);
			$body=str_replace($find,$values,$template->content);

			//Send Mail
			return Mail::send('emails.verify', array('content'=>$body), function($m) use($template)
			{
				$m->to(Input::get('email'))->subject($template->subject);
			});
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
	}
	
	/**
	  * Render the view to reset password.
      * @param int $id            
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 21/8/2015
	**/
	
	public function getReset($token = null)
	{
		try
		{
			if (is_null($token))
			{
				throw new NotFoundHttpException;
			}
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
		return view('admin.auth.reset')->with('token', $token);
	}
	
	/**
	  * Reset the password.
      * @param int $id            
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 21/8/2015
	**/
	
	public function postReset(Request $request)
	{
		try
		{
			$this->validate($request, [
				'token' => 'required',
				'email' => 'required|email',
				'password' => 'required|confirmed',
			]);

			$credentials = $request->only(
				'email', 'password', 'password_confirmation', 'token'
			);
			
			$response = $this->passwords->reset($credentials, function($user, $password)
			{
				//Reset password after credentials matched
				$user->password = Hash::make($password);
				$user->save();
				if($user->save())
				{
					flash()->success('Password Updated');
				}
				else
				{
					flash()->error('Sorry, password not updated. Please try again.');
				}
				return Redirect('admin/auth/login');
			});
			
			switch ($response)
			{
				case PasswordBroker::PASSWORD_RESET:
					return redirect('admin/auth/login');

				default:
					return redirect()->back()
								->withInput($request->only('email'))
								->withErrors(['email' => trans($response)]);
			}
	
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
	}	
}
