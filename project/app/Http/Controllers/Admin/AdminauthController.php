<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\User;
use Input;
use Cookie;

class AdminauthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/
	
	use AuthenticatesAndRegistersUsers;
	
	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @return void
	 */
	
	public function __construct(Guard $auth, Registrar $registrar)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;
		$this->middleware('admin.auth', ['except' => 'getLogout']);
	}

	/**
	  * Render view of login page.
      * @param int $id            
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 21/8/2015
	**/
	
	public function getLogin()
	{
		try
		{
			return view('admin/auth/login');
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
	}
	
	/**
	  * Verify that user is authenticated or not.
      * @param int $id            
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 21/8/2015
	**/
	
	public function postLogin(Requests\AuthenticateUser $request) 
	{	
		try
		{
			$data['email'] = $request->email;
			$data['password'] = $request->password;
			$data['status'] = 1;
			$remember = (Input::has('remember')) ? true : false;
			if($remember == "1")
			{
				
				if ($this->auth->attempt($data))
				{
					//Generate cookies.
					return redirect('admin')->withCookie(cookie('email', $request->email,60))->withCookie(cookie('password', $request->password,60));
				}
			}
			else
			{
				if ($this->auth->attempt($data))
				{
					//Forget cookies.
					$cookie_email = Cookie::forget('email');
					$cookie_password = Cookie::forget('password');
					return redirect('admin')->withCookie($cookie_email)->withCookie($cookie_password);
					
				}
			}
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
		//Redirect after successful login.
			return redirect('admin/auth/login')
						->withInput($request->only('email'))
						->withErrors([
							'email' => $this->getFailedLoginMessage(),
						]);
	}
	
	/**
	  * Log out the user from dashboard.
      * @param int $id            
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 21/8/2015
	**/
	
	public function getLogout()
	{
		try
		{
			$this->auth->logout();
			$val1 = Cookie::get('email');
			$val2= Cookie::get('password');
			//send cookies with response
			response()->view('admin/auth/login')->withCookie(cookie('email', $val1))->withCookie(cookie('password', $val2));
			return redirect('admin/auth/login');
		}
		catch (\Exception $e) 
		{ 
            abort(404);
        }
	}
	
	
}
