<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use DB;
use Illuminate\Http\Request;
use repositories\CommonRepositoryInterface;
use Input;
use Auth;
use Hash;
use Illuminate\Pagination\Paginator;
use App\Models\User;
use App\Models\Role;
use App\Models\EmailTemplate;
use App\Models\State;
use App\Models\Page;
use Image;
use LaravelCaptcha\Lib\Captcha;
use Mail;
use Request as AjaxRequest;
use App\Models\RoleUser;

class AdminController extends Controller {
	
	/*
	|--------------------------------------------------------------------------
	| Admin Controller
	|--------------------------------------------------------------------------
	|
	| This controller manages admin's profile.
	|
	*/
	
	protected $common;
	
	public function __construct(Guard $auth, Registrar $registrar,CommonRepositoryInterface $common)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;
		$this->middleware('admin');
		$this->common = $common;
	}
	
	/**
	  * Show the admin dashboard.            
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 21/8/2015
	**/

	public function Index()
	{	
		try
		{
			if(Auth::user()){
				$active = 'dashboard';
				$data=$this->common->getinfo();
				return view('admin/dashboard',compact('active','data'));
			}
			else
			{
				$data['logError'] = 'Login credentials are invalid.';
				return view('admin.auth.login',$data);	
			}
		}
		catch (\Exception $e) 
		{ 
			abort(404);
		}
	}
	
	/**
	  * Show the view to change the password.            
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 21/8/2015
	**/
	
	public function getChangePassword()
	{
		try
		{
			$active = 'change-password';
			return view('admin/change-password',compact('active'));
		}
		catch (\Exception $e) 
		{ 
			abort(404);
		}
	}
	
	/**
	  * Save changed password in storage.            
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 21/8/2015
	**/
	
	public function postChangePassword(Requests\ResetPassword $request)
	{	
		try
		{
			if(Hash::check($request->old_password, Auth::user()->password))
			  {
				   $id = Auth::user()->id;
				   $user = User::find($id);
				   $user->password=Hash::make($request->password);
				   if($user->save())
				   {
					flash()->success('Password Updated successfully');
				   }
				   else
				   {
					flash()->error('Sorry, Password can not be Changed');
				   }
			  }
			else
			  {
				   flash()->error('Sorry, your old password is not correct');
			  }
		}
		catch (\Exception $e) 
		{ 
			abort(404);
		}
		return redirect('admin/dashboard/change-password');
	}
	
	/**
	  * Listing of all users and moderators.           
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 21 oct 2015
	**/
	
	public function getUsers(Request $request)
	{
		try
		{
			$data['active'] = 'all-users';
			$data['users']=User::where('role','!=',1)->where('delete_status','!=',1)->orderBy('id','DESC')->paginate(10);
			$data['users']->setPath('users');
			
			if(AjaxRequest::ajax()) 
			{
				$user = User::where('role','!=',1);
				if(!empty($request['username'])){
					$user->orWhere('name','LIKE',$request['username'].'%');
				}
				if(!empty($request['email'])){
					$user->orWhere('email','LIKE',$request['email'].'%');
				}
				$data['users'] = $user->orderBy('id', 'DESC')->paginate(10)->setPath('users');
				
				return view('admin.ajax_users',$data);
			}
			return view('admin/users',$data);
		}
		catch (\Exception $e) 
		{ 
			abort(404);
		}
	}
	
	/**
	  * Fetches the states for a specific country.            
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 21/8/2015
	**/
	
	public function postState(Request $request)
	{ 
		try
		{
			$country=(int)$request->cid;
			$states = State::where('country_id',$country)->lists('name', 'id');
			return view('admin.getStates', compact('states'));
		}
		catch (\Exception $e) 
		{ 
			return response()->json(['success'=>false]);
		}
	}
	
	/**
	  * Show the view to upload profile picture.            
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 21/8/2015
	**/
	
	public function getChangePhoto(User $user)
	{
		try
		{
			$active = 'change-photo';
			return view('admin/change-profile',compact('active'));
		}
		catch (\Exception $e) 
		{ 
			abort(404);
		}
	}
	
	/**
	  * Save the uploaded profile picture in database.            
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 21/8/2015
	**/
	
	public function postChangePhoto(Requests\ImageFormat $request)
	{
		try
		{
			$id = Auth::user()->id;
			$img = Input::file('pic');
			$image = $img->getClientOriginalName();
			$path = 'uploads/' . $image;
			//Resizing image
			$editImage=Image::make($img->getRealPath())->resize(200, 200)->save($path);
			$user = User::find($id);
			$user->photo=$image;
			$user->save();
			if($user->save())
			{
				flash()->success('Profile Picture Uploaded successfully');
			}

			else
			{
				flash()->error('Profile Picture can not be uploaded');
			}
		}
		catch (\Exception $e) 
		{ 
			abort(404);
		}
		return redirect('admin/dashboard/change-photo');
	}
	
	/**
	  * Show the view to edit admin profile.            
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 21/8/2015
	**/
	
	public function getEditProfile()
	{
		try
		{
			$active = 'edit-profile';
			$states = $this->common->getStates();
			$countries = $this->common->getCountries();
			return view('admin/edit',compact('states','countries','active'));
		}
		catch (\Exception $e) 
		{ 
			abort(404);
		}
	}
	
	/**
	  * Save the updated admin profile in storage.            
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 21/8/2015
	**/
	
	public function postEditProfile(Requests\EditAdminProfile $request)
	{
		try
		{
			$id=Auth::user()->id;
			$user = User::find($id);
			$user->update($request->all());
			if($user->save())
			{
				flash()->success('Profile Updated successfully');
				return redirect()->action('Admin\AdminController@getView', $id);
			}

			else
			{
				flash()->error('Profile can not be updated');
				return redirect('admin/dashboard/update');
			}
		}
		catch (\Exception $e) 
		{ 
			echo $e->getMessage();//abort(404);
		} 
	}
	
	/**
	  * Show the view to add new user.            
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 21/8/2015
	**/
	
	public function getAddNewUser()
	{
		try
		{
			$active = 'add-new';
			$userRoles = Role::orderBy('id','DESC')->get();
			$states = $this->common->getStates();
			$countries = $this->common->getCountries();
			return view('admin.addNewUser', compact('active','userRoles','states','countries'));
		}
		catch (\Exception $e) 
		{ 
			abort(404);
		}
	}	
	
	/**
	  * Save the user's data in storage.
      * @param int $id            
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 23 oct 2015
	**/
	
	public function postAddNewUser(Requests\AdminNewUser $request)
	{
		try
		{
			//generate random code
			$confirmation_code= str_random(30);
			$newUser= new User($request->all());
			$actualPassword = $newUser->password;
			$newUser->password= Hash::make($newUser->password);
			$newUser->confirmation_code= $confirmation_code; //Save confirmation code
			$newUser->role= $request->type;
			$newUser->status= 1;
			//Dynamic Email Template
			$template=EmailTemplate::find('2');
			//$link="<a href='". url('register/verify/'.$newUser->confirmation_code)."'>Click here</a>";
			$find=array('@email@','@password@','@site_name@');
			$values=array($newUser->email,$actualPassword,config('app.site_name'));
			$body=str_replace($find,$values,$template->content);
			if($newUser->save()){	
				//after saving user info, assign role.
				$role=Role::find($request->type);
				$newUser->attachRole($role->id);
				//Send Mail
				Mail::send('emails.verify', array('content'=>$body), function($message) use($template)
					{
						$message->to(Input::get('email'))->subject($template->subject);
					});
				flash()->success('New User Added Successfully');
			}
			else{
				flash()->error('Sorry, something went wrong');
			}
		}
		catch (\Exception $e) 
		{ 
			abort(404);
		}
		return redirect('admin/dashboard/users');
	}
	
	/**
	  * Show user's details.
      * @param int $id            
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 20 oct 2015
	**/
	
	public function getView($id)
	{
		try
		{
			$active = 'all-users';
			$user=User::find($id); 
			return view('admin/user_detail',compact('active','user'));
		}
		catch (\Exception $e) 
		{ 
			abort(404);
		}
	}
	
	/**
	  * Show the view to update user profile.
      * @param int $id            
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 21/8/2015
	**/
	
	public function getEdit($id)
	{
		try
		{
			$active = 'all-users';
			//CommonRepository file is used here
			$states = $this->common->getStates();
			$countries = $this->common->getCountries();
			$users=User::findOrfail($id);
			$userRoles = Role::orderBy('id','DESC')->get();
			return view('admin/edit-user',compact('users','states','countries','active','userRoles'));
		}
		catch (\Exception $e) 
		{ 
			abort(404);
		}
	}
	
	/**
	  * Save the updated profile in storage.
      * @param int $id            
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 20 oct 2015
	  * desc : to update role and other related user info.
	**/
	
	public function postEdit(Requests\EditProfile $request,$id)
	{
		try
		{
			$user=User::find($id);
			$user->update($request->all());
			$user->save();
			if($user->save())
			{
				$role=Role::find($request->type);
				//detach previously assigned roles
				$previousRoles = $user->user_roles->toArray();
				foreach($previousRoles as $prevRole){
					$prev=Role::find($prevRole['role_id']);
					//detach the all the users associated with this role.
					$prev->users()->sync([]);
				}
				$user->attachRole($role->id);
				flash()->success('User Profile Updated successfully');
			}

			else
			{
				flash()->error('User Profile can not be updated');
			}
		}
		catch (\Exception $e) 
		{ 
			abort(404);
		}
		return redirect('admin/dashboard/users');
	}
	
	/**
	  * Delete the particular user.
      * @param int $id            
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 21/8/2015
	**/
	
	public function postDelete($id)
	{
		try
		{
			$user = User::find($id);
			$data['delete_status'] = 1;
			$user_deleted=$user->update($data); 
			if($user_deleted)
			{
				flash()->success('Record deleted successfully');
			}
			else
			{
				flash()->error('Record can not be deleted');
			}		
		}
		catch (\Exception $e) 
		{ 
			abort(404);
		}
		return redirect('admin/dashboard/users');  
	}
	
	/**
	  * Delete multiple records from database.            
      * @return Response
	  * Created on: 9/10/2015
	  * Updated on: 9/10/2015
	**/
    public function postDestroy(Request $request)
    {
		try
		{
			$ids = explode(',',$request->id);
			$data['delete_status'] = 1;
			if(count($ids)>1)
				$user_delete=DB::table($request->table)->whereIn('id', $ids)->update($data);
			else
				$user_delete=DB::table($request->table)->whereIn('id', $ids)->update($data);
			
			if($user_delete)
			{
				flash()->success('Records deleted!!');
				return response()->json(['success'=>true,'redirect_url' => url('admin/dashboard/users')]);
			}				
			else
			{
				flash()->error('Something went wrong!!');
				return response()->json(['success'=>false,'redirect_url' => url('admin/dashboard/users')]);		
			}
		}
		catch(\Exception $e)
		{
			flash()->error('Something went wrong!!');
			return response()->json(['success'=>false,'redirect_url' => url('admin/dashboard/users')]); 
		}
    }
	
	/**
	  * Change the status.            
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 21/8/2015
	**/
	
	public function postChangeStatus()
    {
        try {
            $request = Input::all();
			$page_status=DB::table($request['table'])->where(array('id'=>$request['id']));
			$page_status->update([
                'status' => $request['status']
            ]);
            
            $result = [
                'success' => true
            ];
        } catch (\Exception $e) {
            $result = [
                'exception_message' => $e->getMessage()
            ];
        }
        return $result;
    }
	
	/**
	  * Show the view to change the settings.            
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 21/8/2015
	**/
	
	public function getSettings()
	{	
		try
		{
			$active = 'settings';
			return view('admin/settings',compact('active'));
		}
		catch (\Exception $e) 
		{ 
			echo $e->getMessage();//abort(404);
		}
	}
	
	/**
	  * Save uploaded logo in storage.            
      * @return Response
	  * Created on: 21/8/2015
	  * Updated on: 21/8/2015
	**/
	
	public function postUploadLogo(Requests\ImageFormat $request)
	{
		try
		{
			$img = Input::file('pic');
			$image = $img->getClientOriginalName();
			$path = 'images/logo/' . $image;
			//Resizing Image
			Image::make($img->getRealPath())->resize(200, 200)->save($path);
			$upload=DB::table('settings')->where(array('_id'=>'55d5559eb0b95cc4790b7856'))->update(array('logo'=>$image));
			if($upload)
			{
				flash()->success('Logo Uploaded successfully');
			}
			else
			{
				flash()->error('Logo can not be uploaded');
			}
		}
		catch (\Exception $e) 
		{ 
			abort(404);
		}
		return redirect('admin/dashboard/settings');
	}
	
	/*
     * Added on : 23 oct 2015
     * Added by : debut infotech
     * Desc : to manage permissions in admin section.
     * */
     public function getPermissions(){
		 try {
           return  $permissions = Permission::all()->paginate(10);
			view('admin/permissions/index',compact('permissions'));
        } catch (\Exception $e) {
            echo $e->getMessage();//abort(404);
        }
	 }
	 
 /*
  * Added on : 23 oct 2015
  * Added by : debut infotech
  * Desc : to display add permissions form.
  * */
  
  public function getAddPermissions(){
	  
  }
  
  /*
   * Added on : 23 oct 2015
   * Added by : debut infotech
   * Desc : to save permissions to database.
   * */
   public function postAddPermissions(){
	   
   }
}
