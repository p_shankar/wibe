<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use repositories\CommonRepositoryInterface;
use Illuminate\Http\Request;
use App\Models\Article;
use App\Http\Requests\UpdateArticle;
use Input;
use Image;
use Auth;
use DB;

class AdminArticlesController extends Controller {
	
	/**
	 * Added On : 14 oct 2015
	 * Added by : debut infotech
	 * desc : to use common repository using $common variable.
	 */
	public function __construct(CommonRepositoryInterface $common)
	{
		$this->common = $common;
	}
	
	/**
	 * Added On : 14 oct 2015
	 * Added by : debut infotech
	 * desc : Display a listing of the Articles.
	 */
	public function getIndex()
	{
		try
		{ 
			$data['active'] = 'all-articles';
			$data['articles'] = Article::orderBy('id', 'DESC')->paginate(10);
			$data['articles']->setPath('articles');
			return view('admin/articles/index', $data);
		}
		catch (\Exception $e) 
		{ 
           abort(404);
        } 
	}

	/**
	 * Added On : 14 oct 2015
	 * Added by : debut infotech
	 * Desc : to render update article view
	 */
	public function getEditArticle($id)
	{
		$data['active'] = 'all-articles';
		//get article data
		$id = base64_decode($id);
		$data['article'] = Article::findOrfail($id);
		//fetch categories
		$data['categories'] = $this->common->getCategories();
		//fetch subcategories
		$data['sub_category'] = $this->common->getSubcategories();
		//render update article view.
		return view('admin/articles/edit', $data);
	}
	
	/**
	 * Added On : 14 oct 2015
	 * Added by : debut infotech
	 * Desc : to render add new article view
	 */
	public function getNewArticle()
	{
		$data['active'] = 'new-article';
		//get article data
		$data['article'] = '';
		//fetch categories
		$data['categories'] = $this->common->getCategories();
		//render update article view.
		return view('admin/articles/articleForm', $data);
	}
	
	/*
	 * Added on : 15 oct 2015
	 * Added by : debut infotech
	 * Desc : to return subcategories according to the category selected.
	 * */
	 public function postSubCategory($id){
		 if(!empty($id)){
			 //fetch subcategories
			$data['subcategory'] = $this->common->getSubcategories($id);
			return view('admin/articles/subcategory',$data);
		 }
	 }
	
	/**
	 * Added On : 14 oct 2015
	 * Added by : debut infotech
	 * Desc : to save article
	 */
	public function postNewArticle(UpdateArticle $request)
	{
		try
		{
			$allInputs = $request->all();
			$allInputs['image'] = '';
			
			$allInputs['created_by'] = Auth::user()->id;
			$allInputs['modified_by'] = Auth::user()->id;
			//check if image is uploaded.
			$img = Input::file('article-image');
			if ($img)
			{
				$image = $this->common->generate_random_string(8).".".$img->getClientOriginalExtension();
				'images/article-images/' . $image;
				//article image destination path
				$path = 'assets/article-images/' . $image;
				//Upload Image
				Image::make($img->getRealPath())->save($path);
				$allInputs['image'] = $image;
				unset($allInputs['article-image']);
			}
			unset($allInputs['htmlcontent']);
			
			//save article
			if(Article::create($allInputs))
			{
				flash()->success('Article Added Successfully.!');
			}
			else
			{
				flash()->error('Sorry! Something went wrong.');
			}
		}
        catch (\Exception $e) 
		{ 
            echo $e->getMessage();//abort(404);
        }
		return redirect('admin/articles');
	}
	
	/**
	 * Added On : 14 oct 2015
	 * Added by : debut infotech
	 * Desc : to render update article view
	 */
	public function postEditArticle($id, UpdateArticle $request)
	{
		try
		{
			//fetch article details
			$id = base64_decode($id);
			$article = Article::findOrfail($id);
			$allInputs = $request->all();
			$allInputs['image'] = $article->image;
			$allInputs['modified_by'] = Auth::user()->id;
			//check if image is uploaded.
			$img = Input::file('article-image');
			if ($img)
			{
				$image = $this->common->generate_random_string(8).".".$img->getClientOriginalExtension();
				//destination path for image
				$path = 'assets/article-images/' . $image;
				//Upload Image
				Image::make($img->getRealPath())->save($path);
				$allInputs['image'] = $image;
				//unset article-image element from $allInputs
				unset($allInputs['article-image']);
			}
			//update article
			if($article->update($allInputs))
			{
				flash()->success('Article has been updated!!');
			}
			else
			{
				flash()->error('Sorry! Something went wrong.');
			}
		}
        catch (\Exception $e) 
		{ 
            abort(404);
        }
		return redirect('admin/articles');
	}

	/**
	 * Added On : 14 oct 2015
	 * Added by : debut infotech
	 * Desc : to delete article
	 */
	public function deleteDeleteArticle($id)
	{
		try
		{
			if(Article::where('id',$id)->update(['delete_status' => 1]))
			{
				flash()->success('Article has been deleted!!');
			}
			else
			{
				flash()->error('Sorry! Something went wrong.');
			}
			
		}
        catch (\Exception $e) 
		{ 
           abort(404);
        }
      return redirect('admin/articles');
	}
	
	/**
	 * Added On : 15 oct 2015
	 * Added by : debut infotech
	 * Desc : to delete article
	 */
	public function getViewArticle($id)
	{
		try
		{
			$id = base64_decode($id);
			$data['active'] = 'all-articles';
			$data['article']= Article::findOrfail($id);
			$data['category'] = $data['article']->category;
			return view('admin/articles/view',$data);
		}
		catch (\Exception $e) 
		{ 
			abort(404);
		}
	}
	
	/*
	 * Added on : 15 oct 2015
	 * Added by : debut infotech
	 * Desc : to set articles as featured
	 * */
	public function postSetFeatured()
    {
        try {
            $request = Input::all();
			$page_status=DB::table($request['table'])->where(array('id'=>$request['id']));
			$page_status->update([
                'featured' => $request['status']
            ]);
            $result = [
                'success' => true
            ];
        } catch (\Exception $e) {
            $result = [
                'exception_message' => $e->getMessage()
            ];
        }
        return $result;
    }

}
