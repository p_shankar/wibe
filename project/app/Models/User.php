<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use DB;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword, EntrustUserTrait;

	//protected $connection = 'mongodb';
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $collection = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password','gender', 'dob', 'phone', 'profile_photo', 'status','country_id','state_id','zip','address','delete_status'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password'];
	
	/**  Has Many Relationship with Story Model  **/
	public function stories()
	{
		return $this->hasMany('App\Models\Story','author_id');
	}
	
	/**  One to One Relationship with Like Model  **/
	public function like()
	{
		return $this->hasOne('App\Models\Like','user_id');
	}
	
	/**  belongs to Relationship with State Model  **/
	public function state()
	{
		return $this->belongsTo('App\Models\State');
	}
	
	/**  belongs to Relationship with State Model  **/
	public function country()
	{
		return $this->belongsTo('App\Models\Country');
	}
	
	/**  Has Many Relationship with RoleUser Model  **/
	public function user_roles()
	{
		return $this->hasMany('App\Models\RoleUser','user_id');
	}
	
	public function setPhoto($path, $photo){
        $photo = ($photo ? $photo : 'null');
        if(file_exists($path.$photo)){
            return '<img src="/'.$path.$photo.'" />';
        }
        return '<img src="/uploads/userDefImage.png">';
    }
}
