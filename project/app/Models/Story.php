<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Story extends Model {

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['title','description','author_id','category_id','subcategory_id','story','vc_meta_title','vc_meta_description','vc_meta_keywords','alias','image','status'];
	
	/**  Belongs To Relationship with User Model  **/
	public function user()
	{
		return $this->belongsTo('App\Models\User');
	}
	
	/**  Belongs To Relationship with User Model  **/
	public function category()
	{
		return $this->belongsTo('App\Models\Category');
	}
	
	/**  Has Many Relationship with Like Model  **/
	public function likes(){
        return $this->hasMany('App\Models\Like','type_id');
    }
	
	/**  Has Many Relationship with Vote Model  **/
	public function votes(){
        return $this->hasMany('App\Models\Vote','type_id');
    }

	/**  Has Many Relationship with Follow Model  **/
	public function followers(){
        return $this->hasMany('App\Models\Follow','type_id');
    }
	
	/**  Has Many Relationship with Comment Model  **/
	public function comments(){
        return $this->hasMany('App\Models\Comment','type_id');
    }
	
	/** Function to set hide status of a story **/
	public function setStatus($id, $status)
    {
        return '<a href="javascript:void(0);" class="changeHideStatus" data-id="'.$id.'" data-table="stories" data-status="'.($status ? 0 : 1).'"><i class="fa fa-circle '.($status ? "active" : "inactive").'"></i></a>';
    }
}
