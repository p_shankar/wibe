<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
	/**  Belongs to Relationship with User Model  **/
	public function user()
	{
		return $this->belongsTo('App\Models\User','state');
	}
}
