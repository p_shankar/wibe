<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Like extends Model {
	
	/**  Belongs To Relationship with User Model  **/
	public function user()
	{
		return $this->belongsTo('App\Models\User');
	}
	
	/**  Belongs To Relationship with Story Model  **/
	public function story()
	{
		return $this->belongsTo('App\Models\Story');
	}

	/**  Function to set Like button **/
	public static function setLikesButton($id,$type,$is_like, $count)
    {
        return '<a href="javascript:void(0);" data-id="'.$id.'" data-type="like" data-category="'.$type.'" class="likeDislikeStory storyLike ' . ($is_like ? 'active disable' : '') . '"><i class="fa fa-thumbs-up"></i></a> <span class="storycountlist likeCount">(' . $count . ')</span>';
    }
	
	/**  Function to set Dislike button **/
	public static function setDislikesButton($id,$type,$is_dislike, $count)
    {
        return '<a href="javascript:void(0);" data-id="'.$id.'" data-type="dislike" data-category="'.$type.'" class="likeDislikeStory storyDislike ' . ($is_dislike ? 'active disable' : '') . '"><i class="fa fa-thumbs-down"></i></a> <span class="storycountlist dislikeCount">(' . $count . ')</span>';
    }
	
	/**  Function to get total likes of specified record  **/
	public static function getCountLikes($id,$type)
	{
		return Like::where(array('type'=>$type,'type_id'=>$id,'like_dislike'=>1))->get(array('like_dislike'))->count();
	}
	
	/**  Function to get total dislikes of specified record  **/
	public static function getCountDislikes($id,$type)
	{
		return Like::where(array('type'=>$type,'type_id'=>$id,'like_dislike'=>0))->get(array('like_dislike'))->count();
	}

}
