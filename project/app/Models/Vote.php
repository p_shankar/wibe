<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model {

	/**  Function to set Vote button **/
	public static function setVoteButton($id,$type,$is_vote, $count)
    {
        return '<a href="javascript:void(0);" data-id="'.$id.'" data-type="vote" data-category="'.$type.'" class="voteStory storyVote ' . ($is_vote ? 'active disable' : '') . '"><i class="fa fa-thumbs-o-up"></i></a> <span class="storycountlist voteCount">(' . $count . ')</span>';
    }

	/**  Belongs To Relationship with User Model  **/
	public function user()
	{
		return $this->belongsTo('App\Models\User');
	}
	
	/**  Function to get total votes of specified record  **/
	public static function getCountVotes($id,$type)
	{
		return Vote::where(array('type'=>$type,'type_id'=>$id))->get()->count();
	}
}
