<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole {

	protected $fillable = ['name','display_name','description'];
	
	public function permission_role(){
		return $this->hasMany('App\Models\PermissionRole','role_id')->select(array('permission_id'));
	}
	
	public function role_user(){
		return $this->hasMany('App\Models\RoleUser');
	}
}
