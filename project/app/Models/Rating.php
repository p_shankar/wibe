<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model {

	/**  Function to get average rating of specified record  **/
	public static function getAvgRating($id,$type)
	{
		$count = Rating::where(array('type'=>$type,'type_id'=>$id))->count();
		$rating = Rating::where(array('type'=>$type,'type_id'=>$id))->get(['score']);
		$sum = 0;
		foreach($rating as $rate)
		{
			 $sum = $sum + $rate['score'];
		}
		if($count > 0)
		{
			return $avg_rating = $sum/$count;
		}
		else
		{
			return $avg_rating = 0;
		}
	}

}
