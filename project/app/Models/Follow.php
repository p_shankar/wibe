<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class Follow extends Eloquent {

	/**  Belongs To Relationship with Story Model  **/
	public function story()
    {
        return $this->belongsTo('App\Models\Story');
    }

	/**  Belongs To Relationship with User Model  **/
	public function user()
	{
		return $this->belongsTo('App\Models\User');
	}
	
	/**  Function to get followers of specified record  **/
	public static function getFollowers($id,$type)
	{
		return Follow::where(array('type'=>$type,'type_id'=>$id))->get()->count();
	}
}
