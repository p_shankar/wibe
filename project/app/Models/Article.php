<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model {

	protected $fillable = ['title', 'description', 'category_id','status', 'delete_status','image','sub_categories_id','created_by','modified_by'];

	/** to fetch associated category details **/
	public function category()
	{
		return $this->belongsTo('App\Models\Category','category_id')->select(array('type_name'));
	}
	
	/** fetch associated user/author details according to created by field in articles table **/
	public function createdBy(){
		return $this->belongsTo('App\Models\User','created_by')->select(array('name'));
	}
	
	/** fetch associated user/author details according to modified by field in articles table **/
	public function modifiedBy(){
		return $this->belongsTo('App\Models\User','modified_by')->select(array('name'));
	}
	

	/** Function to set status of a article **/
	public function setStatus($id, $status)
    {
        return '<a href="javascript:void(0);" class="changeStatus" data-id="'.$id.'" data-table="articles" data-status="'.($status ? 0 : 1).'"><i class="fa fa-circle '.($status ? "active" : "inactive").'"></i></a>';
    }
    
    /** Function to set article as featured **/
    public function setfeatured($id,$featured){
		if($featured == 1){
			return '&nbsp;&nbsp;<a href="javascript:void(0);" data-id="'.$id.'" data-table="articles" data-status="0" class="featuredStatus" style="color:orange" title="featured"><span class="fa fa-star-o featured" aria-hidden="true"></span></a>';
		}else{
			return '&nbsp;&nbsp;<a href="javascript:void(0);" data-id="'.$id.'" data-table="articles" data-status="1" class="featuredStatus" title="set featured"><span class="fa fa-star-o featured" aria-hidden="true"></span></a>';
		}
	}
    
}
