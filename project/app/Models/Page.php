<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Page extends Model {

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
        'title',
		'name',
		'meta_title',
		'meta_description',
		'meta_tags',
        'content',
        'status'
    ];
	
	protected $dates = [
        'created_at',
        'updated_at'
    ];
    
    /**
     * Get the created_at attribute.
     *
     * @param unknown $date
     * @return \Carbon\Carbon
     */
    public function getCreatedAtAttribute($date)
    {
        return new Carbon($date);
    }
    
    /**
     * Get the updated_at attribute.
     *
     * @param unknown $date
     * @return \Carbon\Carbon
     */
    public function getUpdatedAtAttribute($date)
    {
        return new Carbon($date);
    }
	
	/** Function to set status of a page **/
	public function setStatus($id, $status)
    {
        return '<a href="javascript:void(0);" class="changeStatus" data-id="'.$id.'" data-table="pages" data-status="'.($status ? 0 : 1).'"><i class="fa fa-circle '.($status ? "active" : "inactive").'"></i></a>';
    }

}
