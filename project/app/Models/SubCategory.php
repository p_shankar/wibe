<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model {

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
        'name',
		'description',
        'category_id',
		'status'
    ];

	/**  Belongs To Relationship with Category Model  **/
	public function categories()
	{
		return $this->belongsTo('App\Models\Category','category_id');
	}
	
	/** Function to set status of a page **/
	public function setStatus($id, $status)
    {
        return '<a href="javascript:void(0);" class="changeStatus" data-id="'.$id.'" data-table="sub_categories" data-status="'.($status ? 0 : 1).'"><i class="fa fa-circle '.($status ? "active" : "inactive").'"></i></a>';
    }
}
