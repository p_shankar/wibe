<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
        'type_name',
		'type_description',
		'status',
		'image'
    ];

	/**  Has Many Relationship with SubCategory Model  **/
	public function subcategories()
	{
		return $this->hasMany('App\Models\SubCategory','category_id');
	}
	
	/**  Has Many Relationship with Story Model  **/
	public function stories()
	{
		return $this->hasMany('App\Models\Story','category_id');
	}
	
	/** Function to set status of a page **/
	public function setStatus($id, $status)
    {
        return '<a href="javascript:void(0);" class="changeStatus" data-id="'.$id.'" data-table="categories" data-status="'.($status ? 0 : 1).'"><i class="fa fa-circle '.($status ? "active" : "inactive").'"></i></a>';
    }
}
