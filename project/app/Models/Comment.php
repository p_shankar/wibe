<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Comment extends Model {

	protected $dates = [
        'created_at',
        'updated_at'
    ];
	
	/**  Belongs To Relationship with Story Model  **/
	public function story()
    {
        return $this->belongsTo('App\Models\Story');
    }
	
	/**
     * Set time attribute.
     *
     * @param unknown $date
     * @return \Carbon\Carbon
     */
	public function formattedCreatedDate() {
            return 'Created at ' . $this->created_at->diffForHumans();
    }

	/**  Function to set photo of user **/
    public static function setPhoto($path, $user_id)
    {
        $user = User::find($user_id);
        $user->photo = ($user->photo ? $user->photo : 'null');
        if (file_exists($path . $user->photo)) {
            return '<img src="/' . $path . $user->photo . '" />';
        }
        return '<img src="/uploads/userDefImage.png">';
    }
}
