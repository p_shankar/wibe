<?php namespace App;
// AuthenticateUser.php 
use Illuminate\Contracts\Auth\Guard; 
use Laravel\Socialite\Contracts\Factory as Socialite; 
use repositories\UserRepository; 
use Request; 
use OAuth;


class AuthenticateUser {     


 private $users;
  private $auth;

public function __construct(Guard $auth, UserRepository $users) {   
$this->users = $users;
$this->auth = $auth;

}



		public function execute($request, $listener, $provider) {
		$responce = $this->{$provider}($request);
		if($responce['status'] == "login") {
		$user = $this->users->findByUserNameOrCreate($responce['socialdata']);
		$this->auth->login($user, true);
		return $listener->userHasLoggedIn($responce['status']);
		} else if($responce['status'] == "redirect") {
		return redirect((string)$responce['url']);
		} else if($responce['status'] == "cancel") {
		return $listener->userHasLoggedIn($responce['status']);
		} 
		}

  

		/**
		* Get the failed login message.
		*
		* @return string
		*/


public function facebook($request)
{

	$respoonce = array();
    // get data from request
    $code = $request->get('code');
	$error = $request->get('error');
    // get fb service
    $fb = \OAuth::consumer('Facebook');
    // check if code is valid
    // if code is provided get user data and sign in
    if ( ! is_null($code))
    {
        // This was a callback request from facebook, get the token
        $token = $fb->requestAccessToken($code);

        // Send a request with it
        $result = json_decode($fb->request('/me?fields=email,name,id'), true);

       // $message = 'Your unique facebook user id is: ' . $result['id'] . ' and your name is //' . $result['name'];
      //  echo $message. "<br/>";

	    $data_pass['email'] = $result['email'];
		$data_pass['_id'] = $result['id'];
		$data_pass['first_name'] = $result['name'];
	
		$respoonce['socialdata'] = $data_pass;
		$respoonce['status'] =  "login";

    } else if(! is_null($error)) {
		   $respoonce['status'] =  "cancel";
     
	} else {
        // get fb authorization
        echo $url = $fb->getAuthorizationUri();

	        $respoonce['url'] = (string)$url;
			$respoonce['status'] =  "redirect";
    }

return  $respoonce;

}


public function google($request)
{

	$respoonce = array();
    // get data from request
    $code = $request->get('code');
	$error = $request->get('error');

    // get google service
    $googleService = \OAuth::consumer('Google');

    // check if code is valid
    // if code is provided get user data and sign in
    if ( ! is_null($code))
    {
        // This was a callback request from google, get the token
        $token = $googleService->requestAccessToken($code);

        // Send a request with it
        $result = json_decode($googleService->request('https://www.googleapis.com/oauth2/v1/userinfo'), true);

			$data_pass['email'] = $result['email'];
			$data_pass['_id'] = $result['id'];
			$data_pass['last_name'] = $result['family_name'];
			$data_pass['first_name'] = $result['given_name'];

			$respoonce['socialdata'] = $data_pass;
			$respoonce['status'] = "login";
    } else if(! is_null($error)) {

		   $respoonce['status'] =  "cancel";
     
	} 
    // if not ask for permission first
    else
    {
        // get googleService authorization
        $url = $googleService->getAuthorizationUri();

    
	        $respoonce['url'] = (string)$url;
			$respoonce['status'] = "redirect";
    }


	
return  $respoonce;
}




 public function linkedin($request)
 {

	 $respoonce = array();
    // get data from request
    $code = $request->get('code');
		$error = $request->get('error');

    $linkedinService = \OAuth::consumer('Linkedin');


    if ( ! is_null($code))
    {
        // This was a callback request from linkedin, get the token
        $token = $linkedinService->requestAccessToken($code);

        // Send a request with it. Please note that XML is the default format.
              $result = json_decode($linkedinService->request('/people/~:(id,email-address,first-name,last-name)?format=json'), true);


			$data_pass['email'] = $result['emailAddress'];
			$data_pass['id'] = $result['id'];
			$data_pass['last_name'] = $result['lastName'];
			$data_pass['first_name'] = $result['firstName'];

			$respoonce['socialdata'] = $data_pass;
			$respoonce['status'] = "login";

    } else if(! is_null($error)) {

		   $respoonce['status'] =  "cancel";
     
	} 
    // if not ask for permission first
    else
    {
        // get linkedinService authorization
        $url = $linkedinService->getAuthorizationUri(['state'=>'DCEEFWF45453sdffef424']);

	        $respoonce['url'] = (string)$url;
			$respoonce['status'] = "redirect";
    }

	
return  $respoonce;
}















}