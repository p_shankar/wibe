<?php
namespace app\Services;

use Config;

class Solar {  

protected $config;

protected $Client;

function __construct() {

$this->config =  Config::get('solr');
// create a client instance
$this->client = new \Solarium\Client($this->config);

}


	/**
	 * Function to check the connection with solar.
	 *
	 * @return Response
	 * created by :- harpartap singh
	 * Created on :- 26/10/2015
	 */
	public function check_connection()
	{
		//  declare an array for the responce
        $responce = array();
		try {
		// create a ping query
		$ping =$this->client->createPing();
		$result = $this->client->ping($ping);
		$responce = $result->getData();

		} catch (\Exception $e) {
		 $responce['status'] = 0; 
		 $responce['status'] = $e->getMessage(); 
		}
		 // return responce
		return $responce;

	}

	/**
	 * Function to add document to the Core.
	 * @data  : Array of fields those you want to enter in the document
	 * @return Response
	 * created by :- harpartap singh
	 * Created on :- 26/10/2015
	 */
	 	public function add_document($data)
	{
		//  declare an array for the responce
        $responce = array();
		try {
			if($data) {
					
						// get an update query instance
				$update = $this->client->createUpdate();

				// create a new document for the data
				// please note that any type of validation is missing in this example to keep it simple!
				$doc = $update->createDocument();
				$doc->id = $data['id'];
				$doc->title_s = $data['title_s'];
				$doc->desc_txt = $data['desc_txt'];

				// add the document and a commit command to the update query
				$update->addDocument($doc);
				$update->addCommit();

				// this executes the query and returns the result
				$result = $this->client->update($update);

				$responce = $result->getStatus();

		} else {

				$responce['status'] = 1; 
				$responce['message'] = "No data passed"; 
		}

		} catch (\Exception $e) {
				 $responce['status'] = 1; 
				 $responce['message'] = $e->getMessage(); 
		}
		return $responce;
	

	}


/**
	 * Function to delete document by id.
	 * @id  : document
	 * @return Response
	 * created by :- harpartap singh
	 * Created on :- 26/10/2015
	 */
	 	public function delete_document($id)
	{
		//  declare an array for the responce
        $responce = array();
		try {
			if($id) {
					
						// get an update query instance
				$update = $this->client->createUpdate();
				// get an update query instance
				$update = $this->client->createUpdate();
				// add the delete query and a commit command to the update query
				$update->addDeleteQuery('id:*'.$id);
				$update->addCommit();
				// this executes the query and returns the result
				$result = $this->client->update($update);

				$responce = $result->getStatus();

		} else {

				$responce['status'] = 1; 
				$responce['message'] = "No data passed"; 
		}

		} catch (\Exception $e) {
				 $responce['status'] = 1; 
				 $responce['message'] = $e->getMessage(); 
		}
		return $responce;
	

	}































}

?>