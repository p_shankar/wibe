<?php namespace app\Services;

use Illuminate\Validation\Validator;
use LaravelCaptcha\Lib\Captcha;

class Validation extends Validator {

    public function validateAlphaSpaces($attribute, $value)
    {
        return preg_match('/^[\pL\s]+$/u', $value);
    }
	
	public function validateCaptcha($attribute, $value)
	{
		return (new Captcha)->validate($value);
	}
	
}