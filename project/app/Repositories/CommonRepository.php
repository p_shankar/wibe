<?php namespace repositories;

use App\Models\State;
use App\Models\Country;
use App\Models\User;
use App\Models\Story;
use App\Models\Category;
use App\Models\SubCategory;
use App\Models\Vote;
use App\Models\Role;
use Illuminate\Support\Facades\DB;

class CommonRepository implements CommonRepositoryInterface
{
	/*
	 * desc : to return states
	 * */
    public function getStates()
    {
        return State::lists('name', 'id');
    }
	/*
	 * desc : to return countries
	 * */
	public function getCountries()
    {
        return Country::lists('name', 'id');
    }
	/*
	 * desc : to return categories
	 * */
	public function getCategories()
	{
		return Category::where('status','1')->lists('type_name','id');
	}
	
	/*
	 * desc : to return all sub categories.
	 * */
	public function getSubcategories()
	{
		return SubCategory::all()->lists('name','id');
	}
	
	/*
	 * desc : to return sub categories acc. to category id passed.
	 * */
	public function getSubcategory($cat_id)
	{
		return SubCategory::where('category_id',$cat_id)->lists('name','id');
	}
	
	/*
	 * desc : to display info on admin dashboard.
	 * */
	public function getinfo()
	{
		$data['moderators'] = User::where('role','2')->count();
		$data['users'] = User::where('role','3')->count();
		$data['categories'] = Category::all()->count();		
		return $data;
	}
	
	/*
	 * Added on : 21 oct 2015
	 * Added by : debut infotech
	 * Desc : to fetch user role name.
	 * */
	 public function get_user_role($userId){
		 //fetch user details and after that we will find out user role assigned,
		$users=User::findOrfail($userId);
		//check role assigned.
		$user_role = $users->user_roles->toArray();
		$userRole = '';
		foreach($user_role as $role){
			$checkRole = Role::findOrfail($role['role_id']);
			$userRole  = $checkRole->display_name;
		}
		// return user role name.
		return $userRole;
	 }
	 
	 /*
	 * Added on : 15 Oct 2015
	 * Added by : debut infotech
	 * Desc : to generate a random number based on 'length' parameter passed to this function
	 * @param : $length = interger (number of characters to generate randomly.)
	 * */
	 
	 public function generate_random_string($length) {
		$key = '';
		$keys = array_merge(range(0, 9), range('a', 'z'));

		for ($i = 0; $i < $length; $i++) {
			$key .= $keys[array_rand($keys)];
		}

		return $key;
	}
}

?>
