<?php namespace repositories;

use App\Models\User;

class UserRepository {
	
    public function findByUserNameOrCreate($userData) {
        $user = User::where('email', '=', $userData['email'])->first();
        if(!$user) {
            $user = User::create([
                'first_name' => $userData['first_name'],
                'email' => $userData['email'],
                'status' => 1,
            ]);
        }

        $this->checkIfUserNeedsUpdating($userData, $user);
        return $user;
    }

    public function checkIfUserNeedsUpdating($userData, $user) {

        $socialData = [
            'email' => $userData['email'],
            'username' => $userData['first_name'],
			'status' => 1
        ];
        $dbData = [
            'email' => $user->email,
            'first_name' => $user->first_name,
			'status' => $user->status,

        ];

        if (array_diff($socialData, $dbData)) {
		$user->email = $userData['email'];
		$user->first_name = $userData['first_name'];
		$user->status = 1;
		
		$user->save();
        }
    }
}
?>