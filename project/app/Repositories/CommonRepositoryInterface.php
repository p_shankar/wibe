<?php
namespace repositories;

interface CommonRepositoryInterface
{  
     public function getStates();
    public function getCountries();
    public function getCategories();
    public function getSubcategories();
    public function getSubcategory($cat_id);
	public function getinfo();
	public function get_user_role($userId);
	public function generate_random_string($length);
}

?>
