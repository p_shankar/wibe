<html>
	<head>
		<title>{{ config('app.site_name') }}</title>
		
		<link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>

		<style>
			body {
				margin: 0;
				padding: 0;
				width: 100%;
				height: 100%;
				color: #B0BEC5;
				display: table;
				font-weight: 100;
				font-family: 'Lato';
			}

			.container {
				text-align: center;
				display: table-cell;
				vertical-align: middle;
			}

			.content {
				text-align: center;
				display: inline-block;
			}

			.title {
				font-size: 96px;
				margin-bottom: 40px;
			}

			.quote {
				font-size: 24px;
			}
			.text {
				font-size:25px;
				color:blue; 
				font-weight:bold;
				margin:0 10px;
			}
		</style>
	</head>
	<body>
		<div class="container">
			<div class="content">
				<a href="{{ url('auth/login') }}" class="text"> LOGIN </a>
				<a href="{{ url('auth/register') }}" class="text"> REGISTER </a>
				<form method="post" action="{{ url('user/all') }}">
				<input type="hidden" name="_token" value="{{ csrf_token() }}"/>
				<button type="submit"> Submit </button>
				</form>
				<div class="title">Laravel 5</div>
				<div class="quote">{{ Inspiring::quote() }}</div>
			</div>
		</div>
	</body>
</html>
