{{-- */use App\Models\Like;/* --}}
{{-- */use App\Models\Vote;/* --}}
{{-- */use App\Models\Rating;/* --}}
@extends('app')
@section('css')
<link href="{{ asset('/css/jquery.raty.css') }}" rel="stylesheet">
@endsection
@section('content')
<?php $type='story'; ?>
				<div class="panel-heading">{{ $story->title }}'s Detail</div>
				<div class="panel-body">
					{!! $story->story !!}
				
					<li>
						<a href="javascript:void(0);" class="storyfollow followStory" data-type="story" data-id="{{$story->id}}" data-follower="{{$is_follower}}"><i class="fa fa-heart"></i>{{!$is_follower ? 'Follow' : 'Unfollow'}}</a> <span class="storycountlist followersCount">({{$story->followers->count()}})</span>
					</li>
					<li>
						{!! Like::setLikesButton($story->id, $type,$is_like,$count_likes) !!}
						{!! Like::setDislikesButton($story->id, $type, $is_dislike,$count_dislikes) !!}
						{!! Vote::setVoteButton($story->id, $type, $is_vote,$count_votes) !!}
					</li>
				
				<div id="star" data-score="{!! Rating::getAvgRating($story->id, $type) !!}"></div>
				<br><br>
				
				<div class="commentSectionMain">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						
						<div class="commentSectionListComment">
						<ul class="commentsContainer">
						
						</ul>
						</div>
						</div>
						     <div class="clearfix"></div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="commentSectionTitle">
							<b>Give a Comment</b>
							</div>
						
							{!! Form::open(['url' => '','id' => 'commentsForm']) !!}
							{!!Form::hidden('id',$story->id)!!}
							{!!Form::hidden('type',$type)!!}
							
							<div class="commentSectioninputM">
							
							<div class="commentSectionListCommentReg">
							<div class="form-group">
							
							
							<div class="commentSectionRegImg">
							<div class="col-md-12">
								<div class="element">
									{!! $captcha !!} 
									<span>Click on captcha to get new code</span>
									<input type="text" id="captcha" name="captcha" class="form-control" style="margin:5px 0; width:100px">
									<span class="star error"></span>
								</div>
							</div>
						</div>
							@if (Auth::user())
							{!!Auth::user()->setPhoto('uploads/',Auth::user()->photo)!!}
							@else
							<img src="{{url('uploads/userDefImage.png')}}">
							@endif
							</div>
							<div class="commentSectionRegcontent">
							<div class="commentSectionContent">
								<div class="element">
								<textarea name="comment" cols="" rows=""></textarea>
								<span class="star error"></span>
								</div>
							</div>
						
							<div class="commentSectionsendbtn">
							<input id="submitComment" name="submitComment" type="submit" value="Submit" data-loading-text="Please wait..." data-after-loading-text="Submit" />
							</div>
							<div class="clearfix"></div>
							</div>
							</div>
							</div>
							{!! Form::close() !!}
							</div>
							</div>
				
				</div>
@endsection
@section('js')
<script src="{{ asset('/js/story.view.js') }}"></script>
<script src="{{ asset('/js/loader.js') }}"></script>
<script src="{{ asset('/js/comments.js') }}"></script>
<script src="{{ asset('/js/jquery.raty.js') }}"></script>
<script type="text/javascript">
var path = "http://localhost:81/siteconcept/";
$.fn.raty.defaults.path = path+'images/';
$('#star').raty({
	score	   : function() {
		return $(this).attr('data-score');
	},
	click: function(score, evt) {
	  var formData = {
		  score : score,
		  type				 : $('input[name=type]').val(),
          type_id      		 : $('input[name=id]').val(),
	  }
	  formData._token = $('meta[name="csrf-token"]').attr('content');
			$.ajax({
                type     : 'POST',
                url      : path+'story/rating',
                data     : formData,
				datatype : 'json',
				beforeSend : function() {
					addLoader();
				},
				complete : function() {
					removeLoader();
				},
				success : function(json) {
					if(json.success == false)
					{
						new PNotify({
							type: 'error',
							title: 'Error',
							text: 'You have no right to give rating more than once.'
						});
						getRating(json.avg_rating);
					}
					if(json.success == true)
					{
						new PNotify({
							type: 'success',
							title: 'Success',
							text: 'You have given rating successfully.'
						});
						getRating(json.avg_rating);
					}
				},
				error : function(xhr, ajaxOptions, thrownError) {
					if(xhr.status == 401){
						new PNotify({
							type: 'warning',
							title: xhr.responseText,
							text: 'Please login.'
						});
					}
				}
			});
  }
});
function getRating(score)
{
	$('#star').raty({ readOnly: true, score: score });
}
</script>
@endsection