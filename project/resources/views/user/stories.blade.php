@extends('app')
@section('content')
				<div class="panel-heading">List of All Stories</div>
				<div class="panel-body">
				@include('flash::message')
				
				@if(count($stories) == 0) There are no stories!!!
				@else
				<table id="example2" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Status</th>
						<th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
					<?php
					
					foreach($stories as $story){
					?>
						<tr>
							<td>{{ $story->title }}</td>
							<td>{{ $story->vc_meta_description }}</td>
							<td>@if($story->status == 1) Pending @else Published @endif</td>
							<td> 
								{!! Form::open(['id' => 'deletePageForm','url' => 'story/delete/'.$story->_id,'method' => 'post' ]) !!}
								<a class="fa fa-edit" name="edit" id="edit" href="edit/<?php echo $story->id; ?>"></a> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
								
								<a href="javascript:void(0);" class="deleteRecord" data-confirm-message="Are you sure you want to delete this story?" style="color:red" title="Delete"><span class="fa fa-times" aria-hidden="true"></span></a> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
								
								<a class="fa fa-list" name="detail" id="detail" href="detail/<?php echo $story->alias; ?>"></a>
                        {!! Form::close() !!}
						
							</td>
						</tr>
					<?php	}	?>
					
					</tbody>
                  </table>
				  
				  
				  <div style="float:right">
					{!! $stories->render() !!}
				  </div>
				  @endif
					
				</div>
@endsection
