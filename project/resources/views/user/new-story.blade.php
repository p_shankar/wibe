@extends('app')
@section('css')
<link href="{{ asset('/css/editor.css') }}" type="text/css" rel="stylesheet"/>
@endsection
@section('content')
				<div class="panel-heading">Add Story</div>
				<div class="panel-body">
						@include('errors.user_error')
					{!! Form::open(['method' => 'POST','id' => 'add_template','url' => 'story/new-story','files' => true ]) !!}
    
						@include('user.story_form')
					
						<div class="form-group">
							<label class="col-md-4 control-label">Story Icon <span class="star">*</span></label>
							<div class="col-md-6">
							{!! Form::file('image',null,['class' => 'form-control']) !!}
							</div>
						</div>
					
						<div class="form-group">
							{!! Form::submit('Add Story', ['id' => 'submit','class' => 'btn btn-primary form-control']) !!}
						</div>
					
					{!! Form::close() !!}
				</div>
@endsection
@section('js')
<script src="{{ asset('/js/loader.js') }}"></script>
<!--- SubCategory JS --->
	<script src="{{ asset('js/subcategory.js') }}" type="text/javascript"></script>
<!---- Editor JS ---->
	<script src="{{ asset('/js/editor.js') }}"></script>
	<script src="{{ asset('assets/dist/js/editor.js') }}" type="text/javascript"></script>
<!---- Alias JS --->
	<script src="{{ asset('/js/alias.js') }}"></script>
@endsection