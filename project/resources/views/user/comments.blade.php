{{-- */use App\Models\Comment;/* --}}
<div class="commentSectionTitle loaderWillBeHere">
					Total Comments <span class="commentsCount">({{ $count }})</span>
						</div>
<div id="comment_section">
@if($comments->count())
    @foreach($comments as $comment)
        <li>
        	<div>{!!Comment::setPhoto('uploads/',$comment->user_id)!!}</div>
        	<div>
        		<div>{{$comment->name}}</div>
				<div>{{$comment->formattedCreatedDate()}}</div>
        		<div>{{$comment->comment}}</div>
        	</div>
        </li>
    @endforeach
    @else
        No Comments Found.
@endif
</div>
<div id="render">{!! $comments->render() !!}</div>