						<div class="form-group">
							<label class="col-md-4 control-label">E-Mail Address</label>
							<div class="col-md-6">
								<span> {{ Auth::user()->email }} </span>
							</div>
						</div>
						
						<div class="form-group">
							<div class="element">
								<label class="col-md-4 control-label">Username <span class="star">*</span></label>
								<div class="col-md-6">
									{!! Form::text('username',null,['class' => 'form-control']) !!}
									<span class="star error"></span>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="element">
								<label class="col-md-4 control-label">First Name <span class="star">*</span></label>
								<div class="col-md-6">
								{!! Form::text('first_name',null,['class' => 'form-control']) !!}
								<span class="star error"></span>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="element">
								<label class="col-md-4 control-label">Last Name <span class="star">*</span></label>
								<div class="col-md-6">
								{!! Form::text('last_name',null,['class' => 'form-control']) !!}
								<span class="star error"></span>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="element">
								<label class="col-md-4 control-label">Date of Birth <span class="star">*</span></label>
								<div class="col-md-6">
								{!! Form::text('dob',null,['id' => 'dob','class' => 'form-control']) !!}
								<span class="star error"></span>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="element">
								<label class="col-md-4 control-label">Country <span class="star">*</span></label>
								<div class="col-md-6">
								{!! Form::select('country_id',[''=>'---Select a Country---']+$countries,null,['id'=>'country_id','class' => 'form-control']) !!}
								<span class="star error"></span>
								</div>
							</div>
						</div>
  
					    <div class="form-group">
							<div class="element">
								<label class="col-md-4 control-label">State <span class="star">*</span></label>
								<div class="col-md-6">
								{!! Form::select('state',[''=>'---Select a State---']+$states,null,['id'=>'state','class' => 'form-control']) !!}
								<span class="star error"></span>
								</div>
								<div id="mystates"></div>
							</div>
					    </div>
  
					    <div class="form-group">
							<div class="element">
								<label class="col-md-4 control-label">Zip Code <span class="star">*</span></label>
								<div class="col-md-6">
								{!! Form::text('zip',null,['class' => 'form-control']) !!}
								<span class="star error"></span>
								</div>
							</div>
					    </div>
  
					    <div class="form-group">
							<div class="element">
								<label class="col-md-4 control-label">Address <span class="star">*</span></label>
								<div class="col-md-6">
								{!! Form::text('address',null,['class' => 'form-control']) !!}
								<span class="star error"></span>
								</div>
							</div>
					    </div>
						
						<div class="form-group">
							<div class="element">
								<label class="col-md-4 control-label">Phone Number <span class="star">*</span></label>
								<div class="col-md-6">
								{!! Form::text('phone',null,['class' => 'form-control']) !!}
								<span class="star error"></span>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="element">
								<label class="col-md-4 control-label">Profile Picture <span class="star">*</span></label>
								<div class="col-md-6">
								{!! Form::file('photo',['class' => 'form-control']) !!}
								<span class="star error"></span>
								</div>
							</div>
						</div>