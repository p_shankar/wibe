@extends('app')
@section('content')
				<div class="panel-heading">Home</div>
				<div class="panel-body">
					@include('flash::message')
					
					<ul style="padding-top:10px; font-size:18px">
						<li> <a href="{{ url('user/edit-profile') }}"> Edit Profile </a> </li>
						<li> <a href="{{ url('user/change-password') }}"> Change Password </a> </li>
						<li> <a href="{{ url('story/new-story') }}"> Add New Story </a> </li>
						<li> <a href="{{ url('story/stories') }}"> My Stories </a> </li>
					</ul>
				</div>
@endsection
