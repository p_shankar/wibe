						<div class="form-group">
							<label>Title <span class="star">*</span></label>
							{!! Form::text('title',null,['class' => 'form-control']) !!}
						</div>

						<div class="form-group">
							<label>Description <span class="star">*</span></label>
							{!! Form::text('description',null,['class' => 'form-control']) !!}
						</div>
						
							<div class="form-group">
							<label>Category <span class="star">*</span></label>
							{!! Form::select('category_id',[''=>'---Select a Category---']+$categories,null,['id'=>'category_id','class' => 'form-control']) !!}
						</div>
						
						<div class="form-group">
							<label>Sub-Category <span class="star">*</span></label>
							{!! Form::select('subcategory_id',[''=>'---Select a Sub-Category---']+$subcategories,null,['id'=>'subcategory_id','class' => 'form-control']) !!}
							<div id="subcategory"></div>
						</div>
						
						<div class="form-group">
							<label>Story <span class="star">*</span></label>
						{!! Form::textarea('story',null,['id' => 'txtEditor','class' => 'form-control']) !!}
						</div>

						<div class="form-group">
							{!! Form::hidden('hidden',null,['id' => 'hide_template','class' => 'form-control']) !!}
						</div>
						
						<div class="form-group">
							<label>Meta Title <span class="star">*</span></label>
							{!! Form::text('vc_meta_title',null,['class' => 'form-control']) !!}
						</div>
						
						<div class="form-group">
							<label>Meta Description <span class="star">*</span></label>
							{!! Form::textarea('vc_meta_description',null,['class' => 'form-control']) !!}
						</div>
						
						<div class="form-group">
							<label>Meta Keywords <span class="star">*</span></label>
							{!! Form::text('vc_meta_keywords',null,['class' => 'form-control']) !!}
						</div>
						
						<div class="form-group">
							<label>Alias <span class="star">*</span></label>
							<br>
							<span>{{ url('story') }}</span>
							{!! Form::text('alias',null,['id' => 'url','class' => 'form-control']) !!}
							<p>	<span id="link" ></span>
								<span id="url_message"></span> </p>
						</div>
						
						<div class="form-group">
							<label>Status <span class="star">*</span></label>
							{!! Form::radio('status',1) !!} Draft
							{!! Form::radio('status',3) !!} Publish
						</div>