@extends('app')
@section('css')
<link href="{{ asset('/css/editor.css') }}" type="text/css" rel="stylesheet"/>
@endsection
@section('content')
				<div class="panel-heading">Edit Story</div>
				<div class="panel-body">
					@include('errors.user_error')

					{!! Form::model($story,['method' => 'POST','id' => 'add_template','url' => 'story/edit/'.$story->_id,'files' => true ]) !!}
    
						@include('user.story_form')
						
						<div class="form-group">
							{!! Form::submit('Edit Story', ['id' => 'submit','class' => 'btn btn-primary form-control']) !!}
						</div>
					
					{!! Form::close() !!}
					
				</div>
@endsection
@section('js')
<script src="{{ asset('/js/loader.js') }}"></script>
<!--- SubCategory JS --->
	<script src="{{ asset('js/subcategory.js') }}" type="text/javascript"></script>
<!---- Editor JS ---->
	<script src="{{ asset('/js/editor.js') }}"></script>
	<script src="{{ asset('assets/dist/js/editor.js') }}" type="text/javascript"></script>
<!---- Alias JS --->
	<script src="{{ asset('/js/alias.js') }}"></script>
@endsection