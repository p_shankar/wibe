@extends('app')
@section('content')
				<div class="panel-heading">Change Password</div>
				<div class="panel-body">
					@include('errors.user_error')
					@include('flash::message')
					
					<form class="form-horizontal" role="form" method="POST" action="{{ url('user/change-password') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-4 control-label">Old Password <span class="star">*</span></label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="old_password">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label">New Password <span class="star">*</span></label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Confirm Password <span class="star">*</span></label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password_confirmation">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Change Password
								</button>
							</div>
						</div>
					</form>
				</div>
@endsection
