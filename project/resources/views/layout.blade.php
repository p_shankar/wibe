<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>{{ config('app.site_name') }}</title>
	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">	
	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
	<!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<!-- Wait Me Loader -->
	<link href="{{ asset('/css/waitMe.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/jquery.raty.css') }}" rel="stylesheet">
	
	<link href="{{ asset('/css/developer.css') }}" rel="stylesheet">
	<link href="//cdnjs.cloudflare.com/ajax/libs/pnotify/2.0.0/pnotify.all.min.css" rel="stylesheet">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<meta name="csrf-token" content="{!! csrf_token() !!}" />
	@yield('css')
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="{{ url('front') }}">{{ config('app.site_name') }}</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="{{ url('front/pages') }}">Pages</a></li>
				</ul>
			</div>
		</div>
	</nav>

	<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
	@yield('content')

	</div>
		</div>
	</div>
</div>

	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="http://code.jquery.com/jquery.js"></script>
	<!-- Bootstrap 3.3.2 JS -->
    <script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/js/path.js') }}"></script>  
	<script src="{{ asset('/js/jquery.raty.js') }}"></script>  
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<!-- Wait Me Loader JS -->
	<script src="{{ asset('/js/waitMe.js') }}"></script> 
	
	<script src="//cdnjs.cloudflare.com/ajax/libs/pnotify/2.0.0/pnotify.all.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js" type="text/javascript"></script>
	<script src="{{ asset('assets/dist/js/pages.js') }}" type="text/javascript"></script>
	<script type="text/javascript">
	$(function() {
		$( "#dob" ).datepicker({ maxDate:'0' ,dateFormat: "yy-mm-dd",yearRange: "-70:+0", changeYear: true, changeMonth: true}).attr('readonly','readonly'); ;
	});		
	</script>
	@yield('js')
</body>
</html>
