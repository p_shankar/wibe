<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Wibe social media application">
    <meta name="author" content="sanjeev debut Test Wibe app">
	<meta name="keywords" content="WIBE,Social,WibeApp,SanjeevDebutTestWibeAppTest">
    <title>..:: Dubai City Vibe ::..</title>
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('angular/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="{{ asset('angular/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('angular/css/angular_style.css') }}" rel="stylesheet">
    <script src="{{ asset('angular/js/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('angular/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('angular/js/jquery-migrate-1.2.1.min.js') }}" type="text/javascript"></script>
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

<!-- angular library files  -->
<script src="{{ asset('angular/libs/angular.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('angular/libs/angular-route.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('angular/libs/angular-animate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('angular/libs/ui-bootstrap-tpls-0.11.2.min.js') }}" type="text/javascript"></script>
<link href="{{ asset('angular/css/style_custom_animation.css') }}" rel="stylesheet" type="text/css"/>
  </head>

  <body data-ng-app="wibe_app">
  <div class="main">
  
  <div class="top_bar">
  <div class="container-fluid">
  <div class="member">
  <!--<a ng-click="open();">MEMBER</a>-->
  </div>


  
					<div ng-controller="modalForm">
                        <script type="text/ng-template" id="myModalContent.html">
                            <div class="log_in_pop_up_sub">
                            <div class="modal-header">
                            <button type="button" class="close" ng-click="cancel()" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Login</h4>
                            </div>
							<form  name="signin" novalidate  ng-submit="SignIn();" id="signin">
                            <div class="modal-body">
							
                            <div class="login_sec">
								<input type="text" ng-model="signin_username" name="signin_username" required
                                       placeholder="Enter Username"	ng-pattern="/^[a-z|A-Z|]+[a-z|A-Z|0-9|\s]*/" />
                                <div style="color:red" ng-show="signin.$submitted || signin.signin_username.$touched">
                                    <span ng-show="signin.signin_username.$error.required">Username is required.</span>
                                    <span ng-show="signin.signin_username.$error.pattern">must start from alphabet</span>
                                </div>    
								<input type="password" ng-model="signin_password" name="signin_password" id="signin_password" required 
                                       placeholder="Enter Password"	ng-pattern ="/^(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/" />
                                <div style="color:red" ng-show="signin.$submitted || signin.signin_password.$touched">
                                    <span ng-show="signin.signin_password.$error.required">password required.</span>
                                    <span ng-show="signin.signin_password.$error.pattern">
                                        password must contain 8 character including one special char ,one digit,
                                        one uppercase letter and one lowercase letter & length between (8-10)
                                    </span>
                                </div>								
                            </div>
                            </div>
                            <div class="modal-footer">
                            <input type="submit" class="btn btn-primary log_in_sen" ng-disabled="signin.$invalid" ng-model="submit"  name="Sign-IN" />
                            </div>
							</form>
                            </div>
                            <div class="log_in_pop_up_sub">
                            <div class="modal-header">

                            <h4 class="modal-title" id="myModalLabel">Not a member?
                            Sign up!</h4>
                            </div>
							<form  name="signup" novalidate  ng-submit="SignUp();" id="signup">
                            <div class="modal-body">
                            <div class="login_sec">
                            	<input type="text" ng-model="username" name="username" required
                                       placeholder="Enter Username"	ng-pattern="/^[a-z|A-Z|]+[a-z|A-Z|0-9|\s]*/" />
                                <div style="color:red" ng-show="signup.$submitted || signup.username.$touched">
                                    <span ng-show="signup.username.$error.required">Username is required.</span>
                                    <span ng-show="signup.username.$error.pattern">must start from alphabet</span>
                                </div>    
								<input type="password" ng-model="password" name="password" id="password" required 
                                       placeholder="Enter Password"	ng-pattern ="/^(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/" />
                                <div style="color:red" ng-show="signup.$submitted || signup.password.$touched">
                                    <span ng-show="signup.password.$error.required">password required.</span>
                                    <span ng-show="signup.password.$error.pattern">
                                        password must contain 8 character including one special char ,one digit,
                                        one uppercase letter and one lowercase letter & length between (8-10)
                                    </span>
                                </div>								
                            <input type="password" placeholder="password"/>
                            </div>
                            </div>
                            <div class="modal-footer">
                            <input type="submit" class="btn btn-primary log_in_sen" ng-disabled="signup.$invalid" ng-model="submit"  name="Sign-UP" />
                            <div class="signup_fb"> <a href="#!/">SING UP WITH FACEBOOK</a></div>
                            </div>
							</form>
                            </div>
                        </script>
                        <button class="btn btn-success" ng-click="open()" style="float:right;"><i class="glyphicon glyphicon glyphicon-plus-sign icon-plus-sign"></i> Member </button>
                    </div>
  </div>
  <div class="container top_position">
   <div class="arrow_top_bar">
    <img class="img-responsive" src="{{ asset('angular/images/topbar_image.png') }}" alt=""> </div>
  </div>
  </div>
  </div>
  
  <!--end of top bar-->
  <div class="top_nav">
  <div class="container">
  <div class="col-lg-3 col-md-3 col-xs-9">
  	<a href="#!/index">
	<div class="logo">
	    <img class="img-responsive" src="{{ asset('./angular/images/logo.png') }}" alt="">
	 </div>
	</a>
    </div>
    <div class="col-lg-9 col-md-9 col-xs-12 responsive_button">
    <div class="nav_bar_top">
     <div class="navbar-header">
          <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          </div>
		<div class="collapse navbar-collapse" id="navbar" ng-controller="navbarController">
          <ul class="nav navbar-nav">
		  <li ng-repeat="cat in categories"><a href="#!/articlecat/@{{cat.id}}">@{{cat.type_name|uppercase}}</a></li>
		  </ul>
		</div>
    </div>
    </div>
	<!--search box-->
 <div class="input_search_top"><input type="text" placeholder="search here.." ng-model="searchtext"></div>
 </div>
  </div>
  <!--end of nav-->
  
  @yield('content')
  
  <div class="page page-about" data-ng-view="">
     
  </div>

	 <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
  
   <script src="{{ asset('angular/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('angular/app/app.js') }}" type="text/javascript"></script>
    <script src="{{ asset('angular/app/factory/data.js') }}" type="text/javascript"></script>
    <script src="{{ asset('angular/app/filters/filter.js') }}" type="text/javascript"></script>
    <script src="{{ asset('angular/app/directives/directives.js') }}" type="text/javascript"></script>
    <!--Index page controller-->
    <script src="{{ asset('angular/app/controllers/indexpageController.js') }}" type="text/javascript"></script>
<script src="{{ asset('angular/app/controllers/articleController.js') }}" type="text/javascript"></script>
<script src="{{ asset('angular/app/controllers/article_category.js') }}" type="text/javascript"></script>
<script src="{{ asset('angular/app/controllers/navbarController.js') }}" type="text/javascript"></script>



  </body>
</html>
