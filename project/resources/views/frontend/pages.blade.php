@extends('layout')
@section('content')
				<div class="panel-heading">List of All Pages</div>
				<div class="panel-body">
				@include('flash::message')
				
				@if(count($pages) == 0) There are no pages!!!
				@else
				<table id="example2" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>Title</th>
                        <th>Name</th>
                        <th>Content</th>
                        <th>Status</th>
                      </tr>
                    </thead>
                    <tbody>
					<?php
					
					foreach($pages as $page){
					?>
						<tr>
							<td>{{ $page->title }}</td>
							<td>{{ $page->name }}</td>
							<td>{!! $page->content !!}</td>
							<td>@if($page->status == 1) Active @else Inactive @endif</td>
						</tr>
					<?php	}	?>
					
					</tbody>
                  </table>
				  
				  
				  <div style="float:right">
					{!! $pages->render() !!}
				  </div>
				  @endif
					
				</div>
@endsection
