@extends('layout')
@section('content')
				<div class="panel-heading">Home</div>
				<div class="panel-body">
					 <hr>
					@if(count($announcements) == 0) There are no announcements!!!
					@else
						<table>
							@foreach($announcements as $announcement)
								<tr><td>
									{{ $announcement->title }} <br>
									{!! $announcement->description !!}<br><hr>
								</td></tr>
							@endforeach
						</table>
					@endif
				</div>
@endsection