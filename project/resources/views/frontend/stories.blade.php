{{-- */use App\Models\Like;/* --}}
{{-- */use App\Models\Vote;/* --}}
{{-- */use App\Models\Follow;/* --}}
{{-- */use App\Models\Rating;/* --}}
@extends('layout')
@section('content')
<div id="fb-root"></div>
<script>
	window.fbAsyncInit = function() {
	FB.init({
		appId: '403165833211744', 
		status: true, 
		cookie: true,
		xfbml: true
		});
	};
	(function() {
	var e = document.createElement('script'); e.async = true;
	e.src = document.location.protocol +
	'//connect.facebook.net/en_US/all.js';
	document.getElementById('fb-root').appendChild(e);
	}());
</script>
				
				
				@if(count($stories) == 0)
				<div class="panel-heading">List of All Stories</div>
				<div class="panel-body">
					There are no stories!!!
				</div>
				@else
					
				@foreach($stories as $story)
				{!! Form::hidden('id',$story->id) !!}
				<?php $type='story'; ?>
				<div class="panel-heading">{{ $story->title }}</div>
			<div class="panel-body">
					{{ $story->description }}
				
					<li>
						<i class="fa fa-heart"></i>Followers({!! Follow::getFollowers($story->id, $type) !!})
					</li>
					<li>
					
						<i class="fa fa-thumbs-up"></i>Like({!! Like::getCountLikes($story->id, $type) !!})
						<i class="fa fa-thumbs-down"></i>Dislike({!! Like::getCountDislikes($story->id, $type) !!})
						<i class="fa fa-thumbs-o-up"></i>Vote({!! Vote::getCountVotes($story->id, $type) !!})
					</li>
					
					<div class="star" data-score="{!! Rating::getAvgRating($story->id, $type) !!}"></div>
					<?php  
					$link = url('/front/stories/'.$story->alias); 
					?>
					<a title="Share on Facebook" onclick="share_on_fb('{{ $story->title }}','{{ $story->image }}','{{ $story->description }}','{{ $link }}')" id="share_button" class="btn btn-facebook">Facebook</a>
					
					<a href="https://twitter.com/share" class="twitter-share-button" data-url="{{  $link }}" data-text="{{ $story->title }}" data-via="Siteconcept">Tweet</a>
					
					<script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: en_US</script>
					<script type="IN/Share" data-url="{{  $link }}" data-counter="right"></script>
					
				<div class="form-group">
					<button type="button" class="btn btn-primary"><a name="detail" id="detail" style="color:white" href="{{ url('story/detail/'.$story->alias)}}">View Detail</a></button>
				</div>
			</div>
				@endforeach
				  @endif
@endsection
@section('js')

<script>
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');
</script>
<script type="text/javascript">

		function share_on_fb(name,picture,description,link) {
			FB.ui(
			{
				method: 'feed',
				name: name,
				link: link,
				picture: picture,
				caption: 'Siteconcept.',
				description: description,
				message: ''
			});
	
		}
</script>
<script src="{{ asset('/js/story.view.js') }}"></script>
<script src="{{ asset('/js/loader.js') }}"></script>
<script src="{{ asset('/js/comments.js') }}"></script>
<script type="text/javascript">
var path = "http://localhost:81/siteconcept/";
$.fn.raty.defaults.path = path+'images/';
$('.star').raty({
	readOnly   : true,
	score	   : function() {
		return $(this).attr('data-score');
  }
});
</script>
@endsection