@extends('app')
@section('content')
				<div class="panel-heading">Home</div>
					@include('flash::message')
				<div class="panel-body">
					Welcome {{ Auth::user()->username }}!
					 <hr>
						<table>
							@foreach($announcements as $announcement)
								<tr><td>
									{{ $announcement->title }} <br>
									{!! $announcement->description !!}<br><hr>
								</td></tr>
							@endforeach
						</table>
				</div>
@endsection