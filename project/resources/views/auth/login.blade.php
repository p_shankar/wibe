@extends('app')

@section('content')
				<div class="panel-heading">Login</div>
				<div class="panel-body">
					@include('errors.user_error')
					@include('flash::message')
							
					<form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-4 control-label">E-Mail Address</label>
							<div class="col-md-6">
								<input type="email" class="form-control" name="email" value="{{ Cookie::has('email') ? Cookie::get('email') : old('email') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password" value="{{ Cookie::has('password') ? Cookie::get('password') :  '' }}">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<div class="checkbox">
									<label>
										<input <?php if(Cookie::has('email') && Cookie::has('password')) echo "checked"; ?> type="checkbox" name="remember"> Remember Me
									</label>
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">Login</button>
								<a class="btn btn-link" href="{{ url('/password/email') }}">Forgot Your Password?</a>
								<br>or<br>
								<a social_url="{{ url('/sociallogin/facebook') }}" class="social_login">Login with Facebook</a>
								<br>or<br>
								<a social_url="{{ url('/sociallogin/google') }}" class="social_login">Login with Google</a>
								<br>or <br>
								<a social_url="{{ url('/sociallogin/linkedin') }}" class="social_login">Login with Linkedin</a>
								
							</div>
						</div>
					</form>
				</div>
@endsection
@section('js')
<script src="{{ asset('js/oauth.js') }}"></script>
	 <script type="text/javascript">
	$(document).ready(function(){
	 $('.social_login').click(function(){
	  var url_link = $(this).attr('social_url');
	  $(this).oauthpopup({path:url_link,width:800,height:600});
	 });
	});
	</script>
@endsection