@extends('app')
@section('content')

				<div class="panel-heading">Register</div>
				<div class="panel-body">
					@include('errors.user_error')
					@include('flash::message')
					<form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/register') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
	
						<div class="form-group">
							<label class="col-md-4 control-label">E-Mail Address <span class="star">*</span></label>
							
							<div class="col-md-6">
								<input type="email" class="form-control" name="email" value="{{ old('email') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Password <span class="star">*</span></label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Confirm Password <span class="star">*</span></label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password_confirmation">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label">Username <span class="star">*</span></label>
							<div class="col-md-6">
								{!! Form::text('username',null,['class' => 'form-control']) !!}
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label">Date of Birth <span class="star">*</span></label>
							<div class="col-md-6">
							{!! Form::text('dob',null,['id' => 'dob','class' => 'form-control']) !!}
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label"></label>
							<div class="col-md-6">
								{!! $captcha !!} 
								<span>Click on captcha to get a new code</span>
								<input type="text" id="captcha" name="captcha" class="form-control" style="margin:5px 0">
							</div>
						</div>
		  
						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<input type="checkbox" name="agree" id="agree"> I agree to the <a href="#">terms and conditions</a>
							</div>
						</div>
           
						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Register
								</button>
							</div>
						</div>
					</form>
				</div>
			
@endsection