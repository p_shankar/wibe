@extends('admin.layout')
@section('title')
	Edit Announcement		
@endsection
@section('heading')
	Edit {{ $announcement->title }} Announcement
@endsection
@section('content')
<div class="row">
	<div class="col-md-8">
		
	@include('errors.user_error')
    
    {!! Form::model($announcement,['id' => 'add_template','method' => 'PATCH','url' => 'admin/announcement/'.$announcement->id ]) !!}
    
    @include('admin.announcement.form',['submitButtonText' => 'Update Announcement'])
    
    {!! Form::close() !!}
			
	</div>
</div>	
@endsection	