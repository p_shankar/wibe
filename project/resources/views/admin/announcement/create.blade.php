@extends('admin.layout')
@section('title')
	Create Announcement		
@endsection
@section('heading')
	Create Announcement
@endsection
@section('content')
<div class="row">
	<div class="col-md-8">
		
	@include('errors.user_error')
    
    {!! Form::model($announcement = new App\Models\Announcement,['id' => 'add_template','url' => 'admin/announcement','files' => true ]) !!}
    
    @include('admin.announcement.form',['submitButtonText' => 'Add Announcement'])
    
    {!! Form::close() !!}
			
	</div>
</div>	
@endsection	