<div class="box box-primary">
<div class="box-body">

<div class="form-group">
    {!! Form::label('title', 'Title: ') !!} <span class="star">*</span>
    {!! Form::text('title',null,['class' => 'form-control']) !!}
</div>

	{!! Form::label('description', 'Description: ') !!} <span class="star">*</span>
	{!! Form::textarea('description',null,['id' => 'txtEditor','class' => 'form-control']) !!}

<div class="form-group">
    {!! Form::hidden('hidden',null,['id' => 'hide_template','class' => 'form-control']) !!}
</div>
   
@include('partials.status', ['status' => $announcement->status])

<div class="form-group">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-primary']) !!}
</div>
</div>
</div>