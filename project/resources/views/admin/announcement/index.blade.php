@extends('admin.layout')
@section('title')
	Announcements		
@endsection
@section('heading')
				Announcements
				<a href="{{url('admin/announcement/create')}}" class="btn btn-primary btn-sm pull-right">Create Announcement</a>
@endsection
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-body">
			
				@if ( !$announcement->count() ) <div class="star">There is no announcement</div>@else
				<table id="example2" class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>S.no</th>
							<th>Title</th>
							<th>Description</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					
					<?php $index= ($announcement->currentPage() - 1) * $announcement->perPage() + 1; ?>
					@foreach( $announcement as $announcements )
					<tr>
						<td>{{ $index }}</td>
						<td>{{ $announcements->title }}</td>
						<td>{{ substr(strip_tags($announcements->description),0,100) }}</td>
						<td>
						  {!! Form::open(['method' => 'post' ]) !!}
						      {!! $announcements->setStatus($announcements->id, $announcements->status) !!}
						  {!! Form::close() !!}
						</td>
						<td>
						{!! Form::open(['id' => 'deletePageForm','url' => 'admin/announcement/'.$announcements->id,'method' => 'delete' ]) !!}
                            <a href="announcement/{{$announcements->id}}/edit" style="color:orange" title="Edit"><span class="fa fa-edit" aria-hidden="true"></span></a>
					
                            <a href="javascript:void(0);" class="deleteRecord" data-confirm-message="Are you sure you want to delete this announcement?" style="color:red" title="Delete"><span class="fa fa-times" aria-hidden="true"></span></button>
                        {!! Form::close() !!}
						</td>
					</tr>
					<?php  $index++;  ?>
					@endforeach
					</tbody>
				</table>
				{!! $announcement->render() !!} @endif
			</div>	
		</div>
	</div>
</div>
@endsection