@extends('admin.layout')
@section('title')
	Create User
@endsection
@section('heading')
	Add New User
@endsection
@section('content')
<div class="row">
	<div class="col-md-6">
		<div class="box box-primary">
			<div class="box-body">
				@include('errors.user_error')
				
	{!! Form::model(['method' => 'POST','url' => 'admin/dashboard/add-new-user']) !!}
		@include('admin.userform',['submitButtonText' => 'Add User'])
    {!! Form::close() !!}
			</div>	
		</div>
	</div>
</div>
@endsection	