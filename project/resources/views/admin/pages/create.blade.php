@extends('admin.layout')
@section('title')
	Create Page		
@endsection
@section('heading')
	Create Page
@endsection
@section('content')
<div class="row">
	<div class="col-md-12">
		
					@include('errors.user_error')
    
    {!! Form::model($page = new App\Models\Page,['id' => 'add_template','url' => 'admin/pages','files' => true ]) !!}
    
    @include('admin.pages.form',['submitButtonText' => 'Add Page'])
    
    {!! Form::close() !!}
	
	</div>
</div>
@endsection	
