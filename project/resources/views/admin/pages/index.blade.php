@extends('admin.layout')
@section('title')
	CMS Pages		
@endsection
@section('heading')
				CMS Pages
				<a href="{{url('admin/pages/create')}}" class="btn btn-primary btn-sm pull-right">Create Page</a>
@endsection
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-body">
			
				@if ( !$pages->count() ) <div class="star">You have no page</div> @else
				<table id="example2" class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>S.no</th>
							<th>Title</th>
							<th>Name</th>
							<th>Content</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					
					<?php $index= ($pages->currentPage() - 1) * $pages->perPage() + 1; ?>
					@foreach( $pages as $page )
					<tr>
						<td>{{ $index }}</td>
						<td>{{ $page->title }}</td>
						<td>{{ $page->name }}</td>
						<td>{{ substr(strip_tags($page->content),0,100) }}</td>
						<td>
						  {!! Form::open(['method' => 'post' ]) !!}
						      {!! $page->setStatus($page->id, $page->status) !!}
						  {!! Form::close() !!}
						</td>
						<td>
						{!! Form::open(['id' => 'deletePageForm','url' => 'admin/pages/'.$page->id,'method' => 'delete' ]) !!}
                            <a href="pages/{{$page->id}}/edit" style="color:orange" title="Edit"><span class="fa fa-edit" aria-hidden="true"></span></a>
					
                            <a href="javascript:void(0);" class="deleteRecord" data-confirm-message="Are you sure you want to delete this page?" style="color:red" title="Delete"><span class="fa fa-times" aria-hidden="true"></span></button>
                        {!! Form::close() !!}
						</td>
					</tr>
					<?php  $index++;  ?>
					@endforeach
					</tbody>
				</table>
				{!! $pages->render() !!} @endif
			</div>	
		</div>
	</div>
</div>
@endsection