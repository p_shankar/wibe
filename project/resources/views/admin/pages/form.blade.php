<div class="box box-primary">
<div class="box-body">

<div class="form-group">
    {!! Form::label('title', 'Title: ') !!} <span class="star">*</span>
    {!! Form::text('title',null,['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('name', 'Name: ') !!} <span class="star">*</span>
    {!! Form::text('name',null,['class' => 'form-control']) !!}
</div>
    
	<!-- ------------------------ ANGULAR JS TEXT EDITOR ---------------------- -->
<div ng-app="textAngularTest" ng-controller="wysiwygeditor" class="container app">
	{!! Form::label('content', 'Content: ') !!} <span class="star">*</span>
	<!-- Text angular editor -->
	<div text-angular="text-angular" name="htmlcontent" ng-model="htmlcontent" ta-disabled='enable' style="width:960px"></div>
	<!-- actual text box from where description will be passed to the controller -->
	 {!! Form::textarea('content',null,['id' => 'txtEditor','class' => 'form-control','ng-model'=>'htmlcontent','style'=>'display:none']) !!}
	
</div>
<!-- ------------------------------------------------- -->

<div class="form-group">
    {!! Form::hidden('hidden',null,['id' => 'hide_template','class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('meta_title', 'Meta Title: ') !!} <span class="star">*</span>
    {!! Form::text('meta_title',null,['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('meta_description', 'Meta Description: ') !!} <span class="star">*</span>
    {!! Form::textarea('meta_description',null,['class' => 'form-control','rows' => 2, 'cols' => 40]) !!}
</div>

<div class="form-group">
    {!! Form::label('meta_tags', 'Meta Tags: ') !!} <span class="star">*</span>
    {!! Form::text('meta_tags',null,['class' => 'form-control']) !!}
</div>

@include('partials.status', ['status' => $page->status])

<div class="form-group">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-primary']) !!}
</div>
</div>
</div>
