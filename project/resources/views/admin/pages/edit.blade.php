@extends('admin.layout')
@section('title')
	Edit Page		
@endsection
@section('heading')
	Edit {{ $page->name }} Page
@endsection
@section('content')
<div class="row">
	<div class="col-md-12">
		              
					@include('errors.user_error')
    
    {!! Form::model($page,['id' => 'add_template','method' => 'PATCH','url' => 'admin/pages/'.$page->id ]) !!}
    
    @include('admin.pages.form',['submitButtonText' => 'Update Page'])
    
    {!! Form::close() !!}
			
	</div>
</div>
@endsection	
