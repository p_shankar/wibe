@extends('admin.layout')
@section('title')
	Change Password
@endsection
@section('heading')
	Change Password
@endsection
@section('content')

<div class="row">
	<div class="col-md-6">
		<div class="box box-primary">

				@include('errors.user_error')
              <form method="post" action="<?php echo URL::to('admin/dashboard/change-password'); ?>">
			  <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                  <div class="box-body">
					<div class="form-group">
                      <label for="exampleInputEmail1">Old Password:</label> <span class="star">*</span>
                      <input type="hidden" name="_token" class="form-control" value="{{ csrf_token() }}">
                      <input type="password" name="old_password" class="form-control">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Password:</label> <span class="star">*</span>
                      <input type="hidden" name="_token" class="form-control" value="{{ csrf_token() }}">
                      <input type="password" name="password" class="form-control">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Confirm Password:</label> <span class="star">*</span>
                      <input type="password" name="password_confirmation" class="form-control">
                    </div>
                    
                    
                  </div>

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
		</div>
	</div>
</div>
@endsection	
