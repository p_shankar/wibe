  <div class="form-group has-feedback">
	  <label>Username <span class="star">*</span></label>
	<input type="text" name="name" id="username" class="form-control" placeholder="User name" value="{{ old('name') }}" maxlength="32"><span class="glyphicon glyphicon-user form-control-feedback"></span>
  </div>
		  
  <div class="form-group has-feedback">
	  <label>Email : <span class="star">*</span></label>
	<input type="email" name="email" id="email" class="form-control" value="{{ old('email') }}" placeholder="Email"><span class="glyphicon glyphicon-envelope form-control-feedback"></span>
  </div>
  
   <div class="form-group has-feedback">
	<label>Date of Birth: <span class="star">*</span></label> 
	<input type="text" name="dob" id="dob" class="form-control" placeholder="Date of Birth" value="{{ (Input::old('dob')) }}">
  </div>
  
  <div class="form-group has-feedback">
  <label>Phone No. <span class="star">*</span></label>
	<input type="text" name="phone" id="phone" class="form-control" placeholder="Mobile Number" value="{{ (Input::old('phone')) }}"/>
  </div>	  
  
  <div class="form-group has-feedback">
  <label> Country <span class="star">*</span></label>
	{!! Form::select('country_id',array_replace(['' => '---Please select---'],$countries),null,['id'=>'country_id','class' => 'form-control']) !!}
  </div>

  <div class="form-group has-feedback">
  <label> State <span class="star">*</span></label>
	{!! Form::select('state_id',array_replace(['' => '---Please select---'],$states),null,['id'=>'state','class' => 'form-control']) !!}
	<div id="mystates"></div>
  </div>
  
  <div class="form-group has-feedback">
  <label> Zip Code <span class="star">*</span></label>
	<input type="text" name="zip" id="zip" class="form-control" placeholder="Zip Code" value="{{ (Input::old('zip')) }}"/>
  </div>
  
  <div class="form-group has-feedback">
  <label> Address <span class="star">*</span></label>
	<input type="text" name="address" id="address" class="form-control" placeholder="Address" value="{{ (Input::old('address')) }}"/>
  </div>
		  
  <div class="form-group has-feedback">
	  <label> Password: <span class="star">*</span></label>
	<input type="password" name="password" id="password" class="form-control" placeholder="Password"/><span class="glyphicon glyphicon-lock form-control-feedback"></span>
  </div>
  
  <div class="form-group has-feedback">
	  <label> Confirm Password: <span class="star">*</span></label>
	<input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="Retype password"/><span class="glyphicon glyphicon-lock form-control-feedback"></span>
  </div>
		  
   <div class="form-group has-feedback">
	   <label> User Role: <span class="star">*</span></label>
	   <?php 
		foreach($userRoles as $roles){
			?>
		<input type="radio" name="type" id="type"  value="<?php echo $roles->id ?>"> <?php echo $roles->name ?>
			&nbsp; &nbsp;
			<?php
		}
	   ?>
  </div>
  <br> 
	<div class="box-footer"> 
	{!! Form::submit($submitButtonText, ['id'=>'submit','class' => 'btn btn-primary']) !!}   	
		<br> <br>
	</div><!-- /.col -->
</form>   
