@extends('admin.layout')
@section('title')
	Change Profile Picture
@endsection
@section('heading')
	Change Profile Picture
@endsection
@section('content')
<div class="row">
	<div class="col-md-6">
		<div class="box box-primary">
			<div class="box-body">
			
			<form method="post" action="{{ url('/admin/dashboard/change-photo') }}" role="form" enctype="multipart/form-data">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				@include('errors.user_error')	
			<div class="pull-left image col-xs-12">
             <?php
						$photo=Auth::user()->photo;
							if($photo==null)
							{
						?>
						<img src="{{ asset('assets/uploads/profile.png') }}" class="img-circle" alt="User Image" style="height: 200px;
  margin: 2% 25%;"/>
						<?php }
							else {
						?>
						
						<div style="height: 200px; margin: 2% 25%;">
							<img src="{{ asset('uploads/'.$photo) }}" class="img-circle" alt="User Image"/>
						</div>
						
							
					<?php	}	?>
			  <br  /><br />
            </div>
			
					<input class="form-group" type="file" name="pic" id="pic"/>
             
				<button type="submit" class="btn btn-primary " name="submit" id="submit">Update Profile Photo</button>
					
				</form>	
			</div>
		</div>
	</div>
</div>
@endsection
