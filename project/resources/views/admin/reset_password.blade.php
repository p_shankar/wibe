<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>{{ config('app.site_name') }} | Dashboard</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" /> 
      
    <!-- FontAwesome 4.3.0 -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.0 -->
    <link href="{{ asset('assets/dist/css/icons.css') }}" rel="stylesheet" type="text/css" />    
    <!-- Theme style -->
    <link href="{{ asset('assets/dist/css/AdminLTE.min.css') }}" rel="stylesheet" type="text/css" />
  </head>
  <body class="login-page">
    <div class="login-box">
      <div class="login-logo">
        <b>{{ config('app.site_name') }}</b>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
		  
        <p class="login-box-msg">Please Enter Your Email</p>
        
								@include('flash::message')
								@include('errors.user_error')
					
						<form method="post" action="{{ url('admin/password/email') }}" role="form" >
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							
				<input type="text" name="email" value="<?php echo Input::old('email'); ?>" class="form-control" placeholder="Email"/>
							<br><br>							
							 <div class="row">
								<div class="col-xs-8">    
								  <a class="btn btn-primary" href="{{ url('admin/auth/login') }}">Login</a>                       
								</div><!-- /.col -->
								<div class="col-xs-4">
								  <button type="submit" class="btn btn-primary btn-block btn-flat" id="admin-login">Submit</button>
								</div><!-- /.col -->
							  </div>
							</form>

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
  
  </body>
</html>
