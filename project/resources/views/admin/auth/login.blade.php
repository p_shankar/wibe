<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Wibe | Login</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" /> 
    <!-- FontAwesome 4.3.0 -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ asset('assets/dist/css/AdminLTE.min.css') }}" rel="stylesheet" type="text/css" />
	<meta name="csrf-token" content="{!! csrf_token() !!}" />
  </head>
  <body class="login-page">

    <div class="login-box">
      <div class="login-logo">
       <b>{{ config('app.site_name') }}</b>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
		             
		@include('flash::message')
        <p class="login-box-msg">Sign in here!!!</p>
        @include('errors.user_error')
		
        <form action="<?php echo URL::to('admin/auth/login'); ?>" method="post">
			<input type="hidden" name="_token" value="{{ csrf_token() }}"/>
			  
			<div class="form-group has-feedback"> 
            <input type="text" name="email" value="{{ Cookie::has('email') ? Cookie::get('email') : old('email') }}" class="form-control" placeholder="Email"/>
			<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
		  
          <div class="form-group has-feedback">
            <input type="password" name="password" class="form-control" placeholder="Password" value="{{ Cookie::has('password') ? Cookie::get('password') :  '' }}"/>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
		  
		  <div class="row">
            <div class="col-xs-8">    
              <div class="checkbox icheck" style="margin-left:21px">
                <label>
                  <input <?php if(Cookie::has('email') && Cookie::has('password')) echo "checked"; ?> type="checkbox" name="remember"> Remember Me
                </label>
              </div>                        
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
            </div><!-- /.col -->
          </div>
		  			
		  <div class="row">
            <div class="col-xs-8 text-center">    
               <a class="btn btn-link" href="{{ url('admin/password/email') }}" style="float:left; padding-left:0">Forgot Your Password?</a> 
			<br>
			
            </div><!-- /.col -->
            
          </div>
        </form>
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
  
  </body>
</html>
