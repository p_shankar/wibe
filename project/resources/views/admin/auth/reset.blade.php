<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Wibe | Dashboard</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" /> 
    <!-- FontAwesome 4.3.0 -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<!-- Theme style -->
    <link href="{{ asset('assets/dist/css/AdminLTE.min.css') }}" rel="stylesheet" type="text/css" />
  <!--  <link href="{{ asset('assets/dist/css/skins/_all-skins.min.css') }}" rel="stylesheet" type="text/css" /> -->
  </head>
  <body class="login-page">
 
    <div class="login-box">
	
      <div class="login-logo">
       <b>{{ config('app.site_name') }}</b>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
		  
        <p class="login-box-msg">Reset Password</p>
					@include('flash::message')
				<div class="panel-body">
					@include('errors.user_error')

					<form class="form-horizontal" role="form" method="POST" action="{{ url('admin/password/reset') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="token" value="{{ $token }}">

						<div class="form-group has-feedback">
							
							<div class="col-md-12">
								<input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email">
							</div>
						</div>

						<div class="form-group has-feedback">
							
							<div class="col-md-12">
								<input type="password" class="form-control" name="password" placeholder="Enter New Password">
							</div>
						</div>

						<div class="form-group has-feedback">
							
							<div class="col-md-12">
								<input type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password">
							</div>
						</div>

						<div class="form-group has-feedback">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Reset Password
								</button>
							</div>
						</div>
					</form>
				 </div><!-- /.login-box-body -->
		</div>
    </div><!-- /.login-box -->
</body>
</html>
