
{{-- */use repositories\CommonRepositoryInterface;/* --}}
{{-- */use repositories\CommonRepository;/* --}}


<?php $common = new CommonRepository(); ?>
@extends('admin.layout')
@section('title')
	Users
@endsection
@section('heading')
	Users
@endsection
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-body">
			
	<!-- Delete Record Modal-->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5>Confirm Delete</h5>
			</div>
			<div class="modal-body">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="record_id" id="record_id"/>
				<input type="hidden" name="table_name" id="table_name"/>
				<input type="hidden" name="action_name" id="action"/>
				<p class="modaltext">Are you sure you want to delete this record?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
				<button type="button" id="delete_multiple_record" class="btn btn-primary">Yes</button>
			</div>
		</div>
	</div>
</div>
<!--End Delete Record Modal-->			
			
							<div id="user_name" class="col-md-3 form-group">
							<label>Name</label>
							{!! Form::text('username',null,['id'=>'username','placeholder' => 'Search by Name','class' => 'form-control']) !!}
							</div>
							
							
							<div id="user_email" class="col-md-3 form-group">
							<label>Email</label>
							{!! Form::text('email',null,['id'=>'email','placeholder' => 'Search by Email','class' => 'form-control']) !!}
							</div><br>
							

                  <div class="col-md-1">
					<button type="submit" class="btn btn-primary btn-danger btn-sm" id="filter">Search</button>
                  </div>
                    <div class="col-md-1">
                  <a  href="{{url('admin/dashboard/users')}}" class="btn btn-primary btn-primary btn-sm">Reset</a>
                  </div>
              <br><br><br>
			
				
			
			@if(count($users))
					<div style="float:right">
						{!! Form::button('Delete Multiple Record',['class'=>"btn btn-primary",'onclick'=>"deleteMultipleRecords('users','dashboard')"]); !!}
					</div>
					<br>
					<br>
				<div id="ajax_data">
				<table id="example2" class="table table-bordered table-hover">
                    <thead>
					<span class="star multi_delete"></span>
                      <tr>
					    <th>
							{!! Form::checkbox('selectall',null,null,['class'=>'selectall']); !!}</th>
                        <th>S.no</th>
                        <th>User Name</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Status</th>
						<th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
					<?php $index= ($users->currentPage() - 1) * $users->perPage() + 1; ?>
					@foreach($users as $user)
						@if($user->delete_status != 1)
						<tr>
						    <td>{!! Form::checkbox('users[]', $user->id,null,['class'=>'records_id']) !!}</td>
							<td>{{ $index }}</td>
							<td>{{ $user->name }}</td>
							<td>{{ $user->email }}</td>
							{{-- */ $userRole = $common->get_user_role($user->id); /* --}}
							<td>
								@if($userRole != '1')
									{{ $userRole }}
								@endif
							</td>
							<td>@if($user->status == '1') Active @else Inactive @endif</td>
							<td> 
								{!! Form::open(['id' => 'deletePageForm','url' => 'admin/dashboard/delete/'.$user->id,'method' => 'post' ]) !!}
								<a class="fa fa-list-ul" style="color:orange" name="view" id="view" href="view/<?php echo $user->id; ?>"></a> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
								<a class="fa fa-edit" name="edit" id="edit" href="edit/<?php echo $user->id; ?>"></a> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
								<a href="javascript:void(0);" class="deleteRecord" data-confirm-message="Are you sure you want to delete this user?" style="color:red" title="Delete"><span class="fa fa-times" aria-hidden="true"></span></button>
                        {!! Form::close() !!}
						
							</td>
						</tr>
						@endif
						<?php  $index++;  ?>
					@endforeach
					</tbody>
                  </table>
                  <div id="render">
					{!! $users->render() !!}
					</div>
				</div>  
				  
				  
				  @else
					<div class="star">No user found.</div>
				  @endif				
				  
			</div>	
		</div>
	</div>
</div>
@endsection	
@section('js')
<script src="{{ asset('js/loader.js') }}" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function() {

		$("#filter").click(function()
		{
						var username = $('#username').val();
						var email = $('#email').val();
					if(username.trim() == "" && email.trim() == "")
					{
						$("#user_name").addClass(".form-group has-error");
						$("#user_email").addClass(".form-group has-error");
						return false;
					} 
					$.ajax({
						url : path+"admin/dashboard/users",
						type: 'GET',
						data : {
							'username'		 : username,
							'email'		 	 : email
						},
						dataType : 'html',
						beforeSend : function() {
							addLoader();
							$("#user_name").removeClass(".form-group has-error");
							$("#user_email").removeClass(".form-group has-error");
						},
						complete : function() {
							removeLoader();
						},
						success : function(html) {
							$('#ajax_data').html(html);
							//$('#render').hide();
							//$('#ajax_records').html(html);
						},
						error : function(xhr, ajaxOptions, thrownError) {
													console.log(xhr);
								
									new PNotify({
										type: 'error',
										title: xhr.statusText,
										text: 'Something went wrong!!!'
									});
							}
					});
							
				})	
	});
	

</script>
<script type="text/javascript">

$(document.body).on('change' , '.selectall', function(){
    $(this).closest('table').find('td input:checkbox').prop('checked', this.checked);
});

function deleteMultipleRecords(table,action)
{
	var searchIDs = $("input.records_id:checkbox:checked").map(function(){
        return this.value;
    }).toArray();
	if(searchIDs == "")
	{
		$('.multi_delete').text('Please check at least one record');
		return false;
	}
	else{
		$('#deleteModal').modal();
		$('#record_id').val(searchIDs);
		$('#table_name').val(table);
		$('#action').val(action);
	}
    
}
</script>
@endsection
