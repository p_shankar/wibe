</div><!-- /.content-wrapper -->
<footer class="main-footer">
        <strong>Copyright &copy; 2014-2015 <a href="{{ url('/') }}">{{ config('app.site_name') }}</a>.</strong> All rights reserved
      </footer>
    </div><!-- ./wrapper -->
	
    <!-- jQuery 2.1.3 -->
    <script src="{{ asset('assets/plugins/jQuery/jQuery-2.1.3.min.js') }}"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('assets/dist/js/app.min.js') }}" type="text/javascript"></script>
    <!-- page script -->

    <script src="{{ asset('assets/jquery.validate.min.js') }}"></script>
	<script src="{{ asset('/js/path.js') }}"></script> 
	<script src="{{ asset('/js/waitMe.js') }}"></script> 
	<script src="//cdnjs.cloudflare.com/ajax/libs/pnotify/2.0.0/pnotify.all.min.js"></script>
	 <!----  Custom JS  ---->
	<script src="//cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js" type="text/javascript"></script>
	<script src="{{ asset('/js/pages.js') }}" type="text/javascript"></script>
	
	<!----  Editor JS 
	<script src="{{ asset('/js/editor.js') }}"></script>
	<script src="{{ asset('assets/dist/js/editor.js') }}" type="text/javascript"></script> ---->
	
	<!-- ------------ Article JS -------------- -->
	<script src="{{ asset('/js/article.js') }}" type="text/javascript"></script>
	<!-- -------------- Text Angular Editor -- ------------- -->

	<script src='https://ajax.googleapis.com/ajax/libs/angularjs/1.3.11/angular.min.js'></script>
	<script src="{{ asset('/js/textAngular/dist/textAngular-rangy.min.js') }}"></script>
	<script src="{{ asset('/js/textAngular/dist/textAngular-sanitize.min.js') }}"></script>
	<script src="{{ asset('/js/textAngular/dist/textAngular.min.js') }}"></script>
	<script type="text/javascript">
		//check article description
		var articleDesc = $("#articleDesc, #txtEditor").val();
    angular.module("textAngularTest", ['textAngular'])
        .controller('wysiwygeditor', function wysiwygeditor($scope) {
            $scope.orightml = articleDesc;
            $scope.htmlcontent = $scope.orightml;
            $scope.disabled = false;
        });
    </script>
     <script src="{{ asset('/js/fetch.js') }}" type="text/javascript"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<!----  Datepicker JS  ---->
	<script type="text/javascript">
	$(function() {
		$( "#dob" ).datepicker({ maxDate:'0' ,yearRange: "-70:+0",dateFormat: "yy-mm-dd", changeYear: true, changeMonth: true});
	});		
	</script>
    
@yield('js')
  </body>
</html>
