@extends('admin.layout')
@section('title')
	Create Category		
@endsection
@section('heading')
	Create Category
@endsection
@section('content')
<div class="row">
    
{!! Form::model($category = new App\Models\Category,['id' => 'form','files'=>'true','url' => 'admin/category','files' => true ]) !!}
	<div class="col-md-6">
             
	<div class="box box-primary">
	  <div class="box-body">
	  @include('errors.user_error')
		@include('admin.category.form')
		<div class="form-group">
			{!! Form::submit('Add Category', ['id' => 'submit','class' => 'btn btn-primary']) !!}
		</div>
	
      </div>
    </div>
	</div>
	
	<div class="col-md-6">
             
	  <div class="box box-primary">
	  <div class="box-body">
	  <div class="box-header">
		  <h3 class="box-title">Add Sub-Category</h3>
		</div>
		<div class="input_fields_wrap">
			{!! Form::button('Add More', ['id' => 'add_field_button','class' => 'btn btn-primary form-control']) !!}
			<div><br>
			{!! Form::text('variable[0]',null,['id'=>'variable','class' => 'variable']) !!}
			</div>
					
		</div>
		
      </div>
    </div>		
    
	</div>
{!! Form::close() !!}	
</div>

@endsection	
@section('js')
<script type="text/javascript">
	$(document).ready(function() {
    
	$("#form").validate();
	
	var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $("#add_field_button"); //Add button ID
    
    var x = 1; //initial text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
			
            $(wrapper).append('<div><input type="text" class="variable" name="variable['+x+']"/><a href="#" class="remove_field" style="color:red"><span class="fa fa-times"></span></a></div>'); //add input box
			$('.variable').each(function () { 
				$(this).rules("add", {
					required: true
					});
				});
        }
		
	
    });
    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
	
	$("#submit").click(function(){
        $("#form").submit();
        //return false;
        });
});
	</script>
@endsection
