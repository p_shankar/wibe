@extends('admin.layout')
@section('title')
	Categories		
@endsection
@section('heading')
				Categories
				<a href="{{url('admin/category/create')}}" class="btn btn-primary btn-sm pull-right">Create Category</a>
@endsection
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-body">
			
				@if ( !$category->count() ) <div class="star">You have no category</div> @else
				<table id="example2" class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>S.no</th>
							<th>Name</th>
							<th>Description</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					
					<?php $index= ($category->currentPage() - 1) * $category->perPage() + 1; ?>
					@foreach( $category as $categories )
					<tr>
						<td>{{ $index }}</td>
						<td>{{ $categories->type_name }}</td>
						<td>{{ $categories->type_description }}</td>
						<td>
						  {!! Form::open(['method' => 'post' ]) !!}
						      {!! $categories->setStatus($categories->id, $categories->status) !!}
						  {!! Form::close() !!}
						</td>
						<td>
						{!! Form::open(['id' => 'deletePageForm','url' => 'admin/category/'.$categories->id,'method' => 'delete' ]) !!}
                            <a href="category/{{$categories->id}}/edit" style="color:orange" title="Edit"><span class="fa fa-edit" aria-hidden="true"></span></a>
					
                            <!-- <a href="javascript:void(0);" class="deleteRecord" data-confirm-message="Are you sure you want to delete this category?" style="color:red" title="Delete"><span class="fa fa-times" aria-hidden="true"></span></button> -->
                        {!! Form::close() !!}
						</td>
					</tr>
					<?php  $index++;  ?>
					@endforeach
					</tbody>
				</table>
				{!! $category->render() !!} @endif
			</div>	
		</div>
	</div>
</div>
@endsection
