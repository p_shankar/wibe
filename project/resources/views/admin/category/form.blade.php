
<div class="form-group">
    {!! Form::label('title', 'Name: ') !!} <span class="star">*</span>
    {!! Form::text('type_name',null,['class' => 'form-control']) !!}
</div>

<!-- category image -->

<div class="form-group">
    {!! Form::label('image', 'Image: ') !!} <span class="star">*</span>
   {!! Form::file('category-image') !!}
   @if(!empty($category->image))
   <img src="{{ asset('assets/category-images/'.$category->image) }}" height="200" width="200">
   @endif
</div>

<div class="form-group">
    {!! Form::label('name', 'Description: ') !!} <span class="star">*</span>
    {!! Form::textarea('type_description',null,['class' => 'form-control','rows' => 2, 'cols' => 40]) !!}
</div>

@include('partials.status', ['status' => $category->status])

