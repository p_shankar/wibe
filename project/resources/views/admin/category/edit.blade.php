@extends('admin.layout')
@section('title')
	Edit Category		
@endsection
@section('heading')
Edit {{ $category->type_name }} Category
@endsection
@section('content')
<div class="row">

    
    {!! Form::model($category,['method' => 'PATCH','files'=>'true','url' => 'admin/category/'.$category->id ]) !!}
	
	<div class="col-md-6">
             
	<div class="box box-primary">
	  <div class="box-body">
	  @include('errors.user_error')
		@include('admin.category.form')
		<div class="form-group">
				{!! Form::submit('Update Category', ['class' => 'btn btn-primary']) !!}
			</div>
	
      </div>
    </div>
	</div>
	
	<div class="col-md-6">
             
	  <div class="box box-primary">
	  <div class="box-body">
	  <div class="box-header">
		  <h3 class="box-title">List of Sub-Categories</h3>
		</div>
		
		<table class="table table-bordered">
                    <tbody><tr>
                      <th>Name</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
					@foreach($subcategory as $subcategories)
                    <tr>
                      <td>{{ $subcategories->name }}</td>
                      <td>@if($subcategories->status == 1) <span class="label label-success">Activated</span> @else <span class="label label-danger">Deactivated</span> @endif</td>
                      <td>
                        <a href="{{ url('admin/subcategory/'.$subcategories->id.'/edit') }}" style="color:orange" title="Edit">
					<span class="fa fa-edit" aria-hidden="true"></span>
					</a>
                      </td>
                    </tr>
                   @endforeach 
                  </tbody></table>
		
      </div>
    </div>		
    
	</div>
{!! Form::close() !!}	
</div>

@endsection	
