@if($type == 'Likes')
	@if(count($total_likes))
		<b>Total : {{ count($total_likes) }} likes</b>
			<table id="example2" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>Name</th>
									<th>Email</th>
								</tr>
							</thead>
							
							@foreach( $total_likes as $like )
							<tr>
								<td>{{ $like->user->username }}</td>
								<td>{{ $like->user->email }}</td>
							</tr>
							@endforeach
							</tbody>
						</table>
						<div id="render">
						{!! $total_likes->render() !!}
						</div>
	@else
		<div class="star">No Records Found!!!</div>
	@endif
	
@elseif($type == 'Votes')
@if(count($total_votes))
<b>Total : {{ count($total_votes) }} votes</b>
	<table id="example2" class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>Name</th>
							<th>Email</th>
						</tr>
					</thead>
					
					@foreach( $total_votes as $vote )
					<tr>
						<td>{{ $vote->user->username }}</td>
						<td>{{ $vote->user->email }}</td>
					</tr>
					@endforeach
					</tbody>
				</table>
				<div id="render">
						{!! $total_votes->render() !!}
						</div>
@else
		<div class="star">No Records Found!!!</div>
	@endif
	
@elseif($type == 'Followers')
@if(count($total_followers))
<b>Total : {{ count($total_followers) }} followers</b>
	<table id="example2" class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>Name</th>
							<th>Email</th>
						</tr>
					</thead>
					
					@foreach( $total_followers as $follow )
					<tr>
						<td>{{ $follow->user->username }}</td>
						<td>{{ $follow->user->email }}</td>
					</tr>
					@endforeach
					</tbody>
				</table>
				<div id="render">
						{!! $total_followers->render() !!}
						</div>
@else
		<div class="star">No Records Found!!!</div>
	@endif
	
@else
	@if(count($total_comments))
	<b>Total : {{ count($total_comments) }} comments</b>
	<table id="example2" class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>Name</th>
							<th>Email</th>
							<th>Comment</th>
						</tr>
					</thead>
					
					@foreach( $total_comments as $comments )
					<tr>
						<td>{{ $comments->name }}</td>
						<td>{{ $comments->email }}</td>
						<td>{{ $comments->comment }}</td>
					</tr>
					@endforeach
					</tbody>
				</table>
				<div id="render">
						{!! $total_comments->render() !!}
						</div>
	@else
		<div class="star">No Records Found!!!</div>
	@endif			
@endif