@extends('admin.layout')
@section('title')
	Create Story		
@endsection
@section('heading')
	Create Story
@endsection
@section('content')
<div class="row">
	<div class="col-md-8">
		<div class="box box-primary">
			<div class="box-body">

					@include('errors.user_error')
    
    {!! Form::model($story = new App\Models\Story,['id' => 'add_template','url' => 'admin/story','files' => true ]) !!}
    
    @include('user.story_form')
						<div class="form-group">
							<label>Story Icon <span class="star">*</span></label>
							{!! Form::file('image',null,['class' => 'form-control']) !!}
						</div>
					
						<div class="form-group">
							{!! Form::submit('Add Story', ['id' => 'submit','class' => 'btn btn-primary']) !!}
						</div>
    {!! Form::close() !!}
			</div>	
		</div>
	</div>
</div>
@endsection	
@section('js')
<script src="{{ asset('js/loader.js') }}" type="text/javascript"></script>
<!--- SubCategory JS --->
	<script src="{{ asset('js/subcategory.js') }}" type="text/javascript"></script>
<!---- Alias JS --->
	<script src="{{ asset('/js/alias.js') }}"></script>
@endsection