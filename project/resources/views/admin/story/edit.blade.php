@extends('admin.layout')
@section('title')
	Edit Story		
@endsection
@section('heading')
	Edit {{ $story->name }} Story
@endsection
@section('content')
<div class="row">
	<div class="col-md-8">
		<div class="box box-primary">
			<div class="box-body">
               
					@include('errors.user_error')
    
    {!! Form::model($story,['method' => 'PATCH','id' => 'add_template','url' => 'admin/story/'.$story->id ]) !!}
    
    @include('user.storyedit_form')
						<div class="form-group">
							{!! Form::submit('Update Story', ['id' => 'submit','class' => 'btn btn-primary']) !!}
						</div>
    {!! Form::close() !!}
			</div>	
		</div>
	</div>
</div>
@endsection	
@section('js')
<script src="{{ asset('js/loader.js') }}" type="text/javascript"></script>
<!--- SubCategory JS --->
	<script src="{{ asset('js/subcategory.js') }}" type="text/javascript"></script>
<!---- Alias JS --->
	<script src="{{ asset('/js/alias.js') }}"></script>
@endsection