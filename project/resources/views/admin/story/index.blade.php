@extends('admin.layout')
@section('title')
	Stories
@endsection
@section('heading')
				Stories
				<a href="{{url('admin/story/create')}}" class="btn btn-primary btn-sm pull-right">Create Story</a>
@endsection
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-body">

<!-- Delete Record Modal-->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5>Confirm Delete</h5>
			</div>
			<div class="modal-body">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="record_id" id="record_id"/>
				<input type="hidden" name="table_name" id="table_name"/>
				<input type="hidden" name="action_name" id="action"/>
				<p class="modaltext">Are you sure you want to delete this record?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
				<button type="button" id="delete_multiple_record" class="btn btn-primary">Yes</button>
			</div>
		</div>
	</div>
</div>
<!--End Delete Record Modal-->
			
				
							
							<div id="cat" class="col-md-6 form-group">
							<label>Category</label>
							{!! Form::select('category_id',[''=>'---Select a Category---']+$categories,null,['id'=>'category_id','class' => 'form-control']) !!}
							<!--<span class="star cat_error"></span>-->
							</div>
			
							
							<div id="subcat" class="col-md-6 form-group">
							<label>Sub-Category</label>
							{!! Form::select('subcategory_id',[''=>'---Select a Sub-Category---'],null,['id'=>'subcategory_id','class' => 'form-control']) !!}
							<!--<span class="star subcat_error"></span>-->
							<div id="subcategory"></div>
							</div>
							

                  <div class="col-md-1">
					<button type="submit" class="btn btn-primary btn-danger btn-sm" id="story_filter">Filter</button>
                  </div>
                  <div class="col-md-1">
					<a href="{{url('admin/story')}}" class="btn btn-primary btn-primary btn-sm">Reset</a>
                  </div>
              <br>
				
				<div id="ajax_stories"></div><br><br><br><br><br><br>
			<div id="story_table">
				@if ( !$stories->count() ) 
				<div class="star">
				No data Available 
				</div>
				@else
				
					<div>
						{!! Form::button('Delete Multiple Record',['class'=>"btn btn-primary",'onclick'=>"deleteMultipleRecords('stories','story')"]); !!}
					</div>
					<br>
		<div class="row">
					<div class="col-md-12">
              <!-- Custom Tabs (Pulled to the right) -->
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs pull-right">
                  <li class="getStoryDetail"><a href="#tab_1-1" data-toggle="tab">Hide</a></li>
                  <li class="getStoryDetail"><a href="#tab_1-1" data-toggle="tab">Delete</a></li>
                  <li class="getStoryDetail"><a href="#tab_1-1" data-toggle="tab">Publish</a></li>
                  <li class="getStoryDetail"><a href="#tab_1-1" data-toggle="tab">Review</a></li>
                  <li class="getStoryDetail"><a href="#tab_1-1" data-toggle="tab">Draft</a></li>
                  <li class="getStoryDetail"><a href="#tab_1-1" data-toggle="tab">All</a></li>
                  <li class="pull-left header"><i class="fa fa-book"></i>Stories</li>
                </ul>
                <div class="tab-content">
				<table id="example2" class="table table-bordered table-hover">
				<thead>
					<span class="star multi_delete"></span>
						<tr>
							<th>
							{!! Form::checkbox('selectall',null,null,['class'=>'selectall']); !!}</th>
							<th>S.no</th>
							<th>Title</th>
							<th>Description</th>
							<th>Category</th>
							<th>Hide Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<?php $index= ($stories->currentPage() - 1) * $stories->perPage() + 1; ?>
					@foreach( $stories as $story )
					<tr>
						<td>{!! Form::checkbox('stories[]', $story->id,null,['class'=>'records_id']) !!}</td>
						<td>{{ $index }}</td>
						<td>{{ $story->title }}</td>
						<td>{!! $story->description !!}</td>
						<td>{{ $story->category->type_name }}</td>
						<td><?php $status = '';
						if($story->status == 4){ $status = 0; } else{ $status = 1; }
							?>
						  {!! Form::open(['method' => 'post' ]) !!}
						      {!! $story->setStatus($story->id, $status) !!}
						  {!! Form::close() !!}
						</td>
						<td>
						{!! Form::open(['id' => 'deletePageForm','url' => 'admin/story/'.$story->id,'method' => 'delete' ]) !!}
							<a class="fa fa-list" name="detail" id="detail" href="story/{{$story->alias}}"></a>
							
                            <a href="story/{{$story->id}}/edit" style="color:orange" title="Edit"><span class="fa fa-edit" aria-hidden="true"></span></a>
					
                            <a href="javascript:void(0);" class="deleteRecord" data-confirm-message="Are you sure you want to delete this story?" style="color:red" title="Delete"><span class="fa fa-times" aria-hidden="true"></span></a>
                        {!! Form::close() !!}
						</td>
					</tr>
					<?php  $index++;  ?>
					@endforeach
					</tbody>
				</table>
				
				
				
				<div id="render">
				{!! $stories->render() !!}
				</div>
                  <div class="tab-pane active storydetail" id="tab_1-1">
                    
                  </div><!-- /.tab-pane -->
                  <div id="results"></div>
                </div><!-- /.tab-content -->
              </div><!-- nav-tabs-custom -->
            </div><!-- /.col -->
		</div>		
				@endif
			</div>
			
			</div>	
		</div>
	</div>
</div>				
@endsection
@section('js')
<script src="{{ asset('js/loader.js') }}" type="text/javascript"></script>
<!--- SubCategory JS --->
<script src="{{ asset('js/subcategory.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/hidestory.js') }}" type="text/javascript"></script>
	
<script type="text/javascript">
	$(document).ready(function() {

		$("#story_filter").click(function()
		{
						var category = $('#category_id').val();
						var subcategory = $('#subcategoryid').val();

					if(category==""||category==null)
					{
						$("#cat").addClass(".form-group has-error");
						$("#subcat").addClass(".form-group has-error");
						//$('.cat_error').text('Select a Category');
						//$('.subcat_error').text('Select a Sub-Category');
						return false;
					} 
					$.ajax({
						url : path+"admin/story",
						type: 'get',
						data : {
							'category'		 : category,
							'subcategory'	 : subcategory
						},
						dataType : 'html',
						beforeSend : function() {
							addLoader();
							$("#cat").removeClass(".form-group has-error");
							$("#subcat").removeClass(".form-group has-error");
							//$('.cat_error').text('');
							//$('.subcat_error').text('');
						},
						complete : function() {
							removeLoader();
						},
						success : function(html) {
							$('#story_table').hide();
							$('#render').hide();
							$('#ajax_stories').html(html);
						},
						error : function(xhr, ajaxOptions, thrownError) {
													console.log(xhr);
								
									new PNotify({
										type: 'error',
										title: xhr.responseText,
										text: 'Something went wrong!!!'
									});
							}
					});
							
				})	
	});
</script>
<script type="text/javascript">
$(document.body).on('change' , '.selectall', function(){
    $(this).closest('table').find('td input:checkbox').prop('checked', this.checked);
});

function deleteMultipleRecords(table,action)
{
	var searchIDs = $("input.records_id:checkbox:checked").map(function(){
        return this.value;
    }).toArray();
	if(searchIDs == "")
	{
		$('.multi_delete').text('Please check at least one record');
		return false;
	}
	else{
		$('#deleteModal').modal();
		$('#record_id').val(searchIDs);
		$('#table_name').val(table);
		$('#action').val(action);
	}
    
}
</script>
<script type="text/javascript">
$(document).ready(function(){
	
	
	$('.getStoryDetail').click(function(){
		$this = $(this);
		var formData = {
			type             : $this.text()
		}
		formData._token = $('meta[name="csrf-token"]').attr('content');
		$.ajax({
			type 	 : 'get',
			url      : 'story/fetch',
			data     : formData,
			datatype : 'html',
			beforeSend : function() {
				addLoader();
			},
			complete : function() {
				removeLoader();
			},
			success  : function(data) {
				$('#example2').hide();
				$('#render').hide();
				$(".storydetail").html(data);
			}
		})
	});
$(document).on('click', '.pagination a', function (e) 
 {
			$('.storydetail').hide();
			addLoader();
			
            loadResults($(this).attr('href').split('page=')[1]);
            e.preventDefault();
 });	

});
function loadResults(page)
  {
	   var type = false;
	  $('.getStoryDetail a').each(function(){
		  var check = $(this).attr('aria-expanded');
		  if(check == 'true'){
			  type = $(this).text();
		  }  
	  });
	  var url      = window.location.href;
	  
		$.ajax({
			type     : 'GET',
			url      : url+'?page='+page+'&type='+type,
			datatype : 'html',
			success  : function(data) 
				{
				  removeLoader();
				  $('.storydetail').hide();
				  $('#example2').hide();
				  $('#render').hide();
				  $('#results').html(data);   
                },
    error: function(data) {
     // Error...
      var errors = data.responseJSON;
      console.log(errors);
    }
    
   });
  }
</script>
@endsection