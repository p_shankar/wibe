
@if ( !$stories->count() ) 
				<div class="star">
				No data Available 
				</div>
				@else
					
				<table id="example" class="table table-bordered table-hover">
					<thead>
						<tr>
							<th role="columnheader" style="font-weight:bold">
							{!! Form::checkbox('selectall',null,null,['class'=>'selectall']); !!}</th>
							<th>Title</th>
							<th>Description</th>
							<th>Category</th>
							<th>Hide Status</th>
							<th>Action</th>
						</tr>
					</thead>
					
					@foreach( $stories as $story )
					<tr>
						<td>{!! Form::checkbox('stories[]', $story->id,null,['class'=>'records_id']); !!}</td>
						<td>{{ $story->title }}</td>
						<td>{!! $story->description !!}</td>
						<td>{{ $story->category->type_name }}</td>
						<td><?php $status = '';
						if($story->status == 4){ $status = 0; } else{ $status = 1; }
							?>
						  {!! Form::open(['method' => 'post' ]) !!}
						      {!! $story->setStatus($story->id, $status) !!}
						  {!! Form::close() !!}
						</td>
						<td>
						{!! Form::open(['id' => 'deletePageForm','url' => 'admin/story/'.$story->id,'method' => 'delete' ]) !!}
							<a class="fa fa-list" name="detail" id="detail" href="story/{{$story->alias}}"></a>
							
                            <a href="story/{{$story->id}}/edit" style="color:orange" title="Edit"><span class="fa fa-edit" aria-hidden="true"></span></a>
					
                            <a href="javascript:void(0);" class="deleteRecord" data-confirm-message="Are you sure you want to delete this story?" style="color:red" title="Delete"><span class="fa fa-times" aria-hidden="true"></span></button>
                        {!! Form::close() !!}
						</td>
					</tr>
					@endforeach
					</tbody>
					
				</table>
				
				<div id="filter_render">
				@if($category || $subcategory)
				{!! $stories->appends(['category' => $category,'subcategory' => $subcategory])->render() !!}
			@else
				{!! $stories->render() !!}
			@endif
				 </div>
				 @endif