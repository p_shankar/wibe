@extends('admin.layout')
@section('title')
{{ $story[0]->title }}'s Detail
@endsection

@section('content')

<div class="row">

 <div class="col-md-6">
              <!-- Custom Tabs (Pulled to the right) -->
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs pull-right">
                  <li class="getDetail"><a href="#tab_1-1" data-toggle="tab">Comments</a></li>
                  <li class="getDetail"><a href="#tab_1-1" data-toggle="tab">Followers</a></li>
                  <li class="getDetail"><a href="#tab_1-1" data-toggle="tab">Votes</a></li>
                  <li class="getDetail"><a href="#tab_1-1" data-toggle="tab">Likes</a></li>
                  <li class="pull-left header"><i class="fa fa-th"></i> {{ $story[0]->title }}</li>
                </ul>
                <div class="tab-content">
                  <div class="tab-pane active storydetail" id="tab_1-1">
                    
                  </div><!-- /.tab-pane -->
                  <div id="results"></div>
                </div><!-- /.tab-content -->
              </div><!-- nav-tabs-custom -->
            </div><!-- /.col -->
			<div class="box box-primary">
			
					<input name="story_id" type="hidden" value="{{ $story[0]->id }}"/>
				 <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
				{!! $story[0]->story !!}
				
				</div>
				
				</div>
	</div>	
	
</div>		
@endsection
@section('js')
<script src="{{ asset('js/loader.js') }}" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	
	
	$('.getDetail').click(function(){
		$this = $(this);
		var formData = {
			id				 : $('input[name=story_id]').val(),
			type             : $this.text()
		}
		formData._token = $('meta[name="csrf-token"]').attr('content');
		$.ajax({
			url      : 'show',
			data     : formData,
			datatype : 'html',
			beforeSend : function() {
				addLoader();
			},
			complete : function() {
				removeLoader();
			},
			success  : function(data) {
				$(".storydetail").html(data);
			}
		})
	});
$(document).on('click', '.pagination a', function (e) 
 {
			$('.storydetail').hide();
			addLoader();
			
            loadResults($(this).attr('href').split('page=')[1]);
            e.preventDefault();
 });	

});
function loadResults(page)
  {
	   var type = false;
	  $('.getDetail a').each(function(){
		  var check = $(this).attr('aria-expanded');
		  if(check == 'true'){
			  type = $(this).text();
		  }  
	  });
	  var url      = window.location.href;
	  
    $.ajax({
			type     : 'GET',
			url      : url+'?page='+page+'&type='+type,
			datatype : 'html',
			success  : function(data) 
				{
				  removeLoader();
				  $('.storydetail').hide();
				  $('#results').html(data);   
                },
    error: function(data) {
     // Error...
      var errors = data.responseJSON;
      console.log(errors);
    }
    
   });
  }
</script>
@endsection