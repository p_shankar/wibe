@extends('admin.layout')
@section('title')
Admin|Dashboard
@endsection
@section('content')
<div class="row">
	<div class="col-md-12">
		
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3>{{ $data['users'] }}</h3>
                  <p>Users</p>
                </div>
                <div class="icon">
                  <i class="fa fa-users"></i>
                </div>
                <a href="{{ url('admin/dashboard/users') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <h3>{{ $data['moderators'] }}</h3>
                  <p>Moderators</p>
                </div>
                <div class="icon">
                  <i class="fa fa-users"></i>
                </div>
                <a href="{{ url('admin/dashboard/users') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <h3>{{ $data['categories'] }}</h3>
                  <p>Categories</p>
                </div>
                <div class="icon">
                  <i class="fa fa-tags"></i>
                </div>
                <a href="{{ url('admin/category') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
			
	</div>
</div>
  
@endsection	
