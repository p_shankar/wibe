@extends('admin.layout')
@section('title')
	Change Password
@endsection
@section('heading')
	Change Password
@endsection

@section('content')
					@include('errors.user_error')
				
              <form method="post" action="<?php echo URL::to('admin/dashboard/reset-admin-password'); ?>">
                  <div class="box-body">
				  <div class="form-group">
					   <label for="exampleInputEmail1">Old Password</label>
					   
						<input type="password" class="form-control" name="old_password">
					   
					  </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">New Password</label>
                      <input type="hidden" name="_token" class="form-control" value="{{ csrf_token() }}">
                      <input type="password" name="password" class="form-control">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Confirm Password</label>
                      <input type="password" name="password_confirmation" class="form-control">
                    </div>
                    
                    
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
@endsection
