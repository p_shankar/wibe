@include('admin.header')
@include('admin.aside')
<section class="content-header">
			  <h1>
				@yield('heading')
			  </h1>
			</section>
        <!-- Main content -->
        <section class="content">          
			@include('flash::message')
			<div id="waitMeLoader">
			@yield('content')
			</div>
            
          <!-- /.box -->
		
</section><!-- /.content -->
    
      
@include('admin.footer')
