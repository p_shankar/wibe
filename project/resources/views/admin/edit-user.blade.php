@extends('admin.layout')

@section('title')
	Edit Profile
@endsection
@section('heading')
	Edit Profile
@endsection
@section('content')
<div class="row">
	<div class="col-md-6">
		<div class="box box-primary">
			<div class="box-body">
			
			<form method="post" action="{{ url('/admin/dashboard/edit',$users->id) }}" role="form" >
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				@include('errors.user_error')
				
				<div class="form-group has-feedback">
				<label>Username <span class="star">*</span></label>
            <input type="text" name="name" id="username" class="form-control" placeholder="User Name" value="{{ (Input::old('username'))? Input::old('username') : $users->name }}" maxlength="32"/>
          </div>
		 
          <div class="form-group has-feedback">
		  <label>Email : </label>
            <span style="padding-left:16px"> {{ $users->email }} </span>
          </div>
		 
		  <div class="form-group has-feedback">
            <label>Date of Birth: <span class="star">*</span></label> 
            <input type="text" name="dob" id="dob" class="form-control" placeholder="Date of Birth" value="{{ (Input::old('dob'))? Input::old('dob') : $users->dob }}">
          </div>
		  
		  <div class="form-group has-feedback">
		  <label>Phone No. <span class="star">*</span></label>
            <input type="text" name="phone" id="phone" class="form-control" placeholder="Mobile Number" value="{{ (Input::old('phone'))? Input::old('phone') : $users->phone }}"/>
          </div>	  
		  
		  <div class="form-group has-feedback">
		  <label> Country <span class="star">*</span></label>
			@if($users->country_id)
			{!! Form::select('country_id',array_replace(['' => '---Please select---'],$countries),$users->country_id,['id'=>'country_id','class' => 'form-control']) !!}
			@else
			{!! Form::select('country_id',array_replace(['' => '---Please select---'],$countries),null,['id'=>'country_id','class' => 'form-control']) !!}
			@endif
		  </div>
  
		  <div class="form-group has-feedback">
		  <label> State <span class="star">*</span></label>
		  @if($users->state)
			{!! Form::select('state_id',array_replace(['' => '---Please select---'],$states),$users->state_id,['id'=>'state','class' => 'form-control']) !!}
			@else
			{!! Form::select('state_id',array_replace(['' => '---Please select---'],$states),null,['id'=>'state','class' => 'form-control']) !!}
			@endif
			<div id="mystates"></div>
		  </div>
		  
		  
		  <div class="form-group has-feedback">
		  <label> Zip Code <span class="star">*</span></label>
            <input type="text" name="zip" id="zip" class="form-control" placeholder="Zip Code" value="{{ (Input::old('zip'))? Input::old('zip') : $users->zip }}"/>
          </div>
		  
		  <div class="form-group has-feedback">
		  <label> Address <span class="star">*</span></label>
            <input type="text" name="address" id="address" class="form-control" placeholder="Address" value="{{ (Input::old('address'))? Input::old('address') : $users->address }}"/>
          </div>

		 <div class="form-group has-feedback">
	   <label> User Role: <span class="star">*</span></label>
	   <?php 
		foreach($userRoles as $roles){
			?>
		<input type="radio" name="type" id="type" <?php if($users->hasRole($roles->name)){ echo "checked"; } ?> value="<?php echo $roles->id ?>"> <?php echo $roles->name ?>
			&nbsp; &nbsp;
			<?php
		}
	   ?>
  </div>
            <div class="box-footer">    
                        
					<button type="submit" class="btn btn-primary" name="submit" id="submit">Update</button>
					<br> <br>
            </div><!-- /.col -->
        </form> 
			</div>	
		</div>
	</div>
</div>		
@endsection

