
{{-- */use repositories\CommonRepositoryInterface;/* --}}
{{-- */use repositories\CommonRepository;/* --}}


<?php $common = new CommonRepository(); ?>

@if(count($users))
	<br>
					<div style="float:right">
						<!-- {!! Form::button('Delete Multiple Record',['class'=>"btn btn-primary",'onclick'=>"deleteMultipleRecords('users','dashboard')"]); !!} -->
					</div>
					<br>
				<table id="example" class="table table-bordered table-hover">
                    <thead>
					<span class="star multi_delete"></span>
                      <tr>
					    <th role="columnheader" style="font-weight:bold">
							{!! Form::checkbox('selectall',null,null,['class'=>'selectall']); !!}</th>
                        <th style="font-weight:bold">User Name</th>
                        <th style="font-weight:bold">Email</th>
                        <th style="font-weight:bold">Role</th>
                        <th style="font-weight:bold">Status</th>
						<th style="font-weight:bold">Action</th>
                      </tr>
                    </thead>
                    <tbody>
					
					@foreach($users as $user)
						@if($user->delete_status != 1)
						<tr>
						    <td>{!! Form::checkbox('users[]', $user->id,null,['class'=>'records_id']) !!}</td>
							<td>{{ $user->name }}</td>
							<td>{{ $user->email }}</td>
							{{-- */ $userRole = $common->get_user_role($user->id); /* --}}
							@if($userRole != '1')
							<td>{{ $userRole }}</td>
							@else
							<td>No role assigned</td>
							@endif
							<td>@if($user->status == '1') Active @else Inactive @endif</td>
							<td> 
								{!! Form::open(['id' => 'deletePageForm','url' => 'admin/dashboard/delete/'.$user->id,'method' => 'post' ]) !!}
								<a class="fa fa-list-ul" style="color:orange" name="view" id="view" href="view/<?php echo $user->id; ?>"></a> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
								<a class="fa fa-edit" name="edit" id="edit" href="edit/<?php echo $user->id; ?>"></a> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
								<a href="javascript:void(0);" class="deleteRecord" data-confirm-message="Are you sure you want to delete this user?" style="color:red" title="Delete"><span class="fa fa-times" aria-hidden="true"></span></button>
                        {!! Form::close() !!}
						
							</td>
						</tr>
						@endif
					@endforeach
					</tbody>
                  </table>
				  
				  
				  <div>
					{!! $users->render() !!}
				  </div>
				  @else
					<div class="star">No user found.</div>
				  @endif
