@extends('admin.layout')
@section('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
@endsection
@section('title')
	Edit Profile
@endsection
@section('heading')
	Edit Profile
@endsection
@section('content')
<div class="row">
	<div class="col-md-6">
		<div class="box box-primary">

			<form method="post" action="{{ url('/admin/dashboard/edit-profile') }}" role="form">
			<div class="box-body">
				<input type="hidden" name="_token" value="{{ csrf_token() }}"/>
				
				@include('errors.user_error')
			
			
			<div class="form-group has-feedback">
			<label>Name <span class="star">*</span></label>	
            <input type="text" name="name" id="name" class="form-control" value="{{ (Input::old('name'))? Input::old('name') : Auth::user()->name }}" maxlength="32"/>
          </div>
		  
          <div class="form-group has-feedback">
		   <label>Email : </label>
            <span style="padding-left:16px"> {{ Auth::user()->email }} </span>
          </div>
		  
		  <div class="form-group">
				{!! Form::label('gender', 'Gender: ') !!} <span class="star">*</span>
				{!! Form::radio('gender',0) !!}Male
				{!! Form::radio('gender',1) !!}Female
		  </div>
		  
		  <div class="form-group">
			<label>Date of Birth</label>
			<div class="form-group has-feedback">
				<input type="text" class="form-control" name="dob" id="dob" value="{{ (Input::old('dob'))? Input::old('dob') : Auth::user()->dob }}">
			</div>
		  </div>
		  
		  <div class="form-group has-feedback">
		  <label>Phone <span class="star">*</span></label>
            <input type="text" name="phone" id="phone" class="form-control" value="{{ (Input::old('phone'))? Input::old('phone') : Auth::user()->phone }}"/>
          </div>
		  
		  <br>
	
          <div class="row">
            <div class="col-xs-4">    
                        
					<button type="submit" class="btn btn-primary btn-block btn-flat" name="submit" id="submit">Update Profile</button>
					<br> <br>
            </div><!-- /.col -->
            
          </div>
		  </div>
        </form>    
			</div>
	</div>
</div>
@endsection
@section('js')
<!----  Fetch JS  ---->
	<script src="{{ asset('/js/loader.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/js/fetch.js') }}" type="text/javascript"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<!----  Datepicker JS  ---->
	<script type="text/javascript">
	$(function() {
		$( "#dob" ).datepicker({ maxDate:'0' ,yearRange: "-70:+0",dateFormat: "yy-mm-dd", changeYear: true, changeMonth: true}).attr('readonly','readonly');
	});		
	</script>
@endsection
