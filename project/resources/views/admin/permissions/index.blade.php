@extends('admin.layout')
@section('title')
	Permissions
@endsection
@section('heading')
	Permissions
	
	<a class="btn btn-primary btn-sm pull-right" href="{{ url('/admin/permissions/add-permissions') }}">Add Permissions</a>
@endsection
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-body">
			
	<!-- Delete Permission Modal-->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5>Confirm Delete</h5>
			</div>
			<div class="modal-body">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="record_id" id="record_id"/>
				<input type="hidden" name="table_name" id="table_name"/>
				<input type="hidden" name="action_name" id="action"/>
				<p class="modaltext">Are you sure you want to delete this permission?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
				<button type="button" id="delete_multiple_record" class="btn btn-primary">Yes</button>
			</div>
		</div>
	</div>
</div>
<!--End Delete Permission Modal-->			
			
			@if(count($permissions))
					
				
				<table id="example2" class="table table-bordered table-hover">
                    <thead>
					<span class="star multi_delete"></span>
                      <tr>
                        <th>S.no</th>
                        <th>Permission Name</th>
                        <th>Display Name</th>
                        <th>Description</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
					<?php $index= ($permissions->currentPage() - 1) * $permissions->perPage() + 1; ?>
					@foreach($permissions as $per)
							<td>{{ $index }}</td>
							<td>{{ $per->name }}</td>
							<td>{{ $per->display_name }}</td>
							<td>{{ $per->description }}</td>
							<td> 
								{!! Form::open(['id' => 'deletePageForm','url' => 'admin/permissions/delete-permission/'.$per->id,'method' => 'delete' ]) !!}
								<a class="fa fa-edit" name="edit" id="edit" href="permissions/edit-permission/<?php echo $per->id; ?>"></a> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
								<a href="javascript:void(0);" class="deleteRecord" data-confirm-message="Are you sure you want to delete this permission?" style="color:red" title="Delete"><span class="fa fa-times" aria-hidden="true"></span></button>
                        {!! Form::close() !!}
						
							</td>
						</tr>
						<?php  $index++;  ?>
					@endforeach
					</tbody>
                  </table>
                  <div id="render">
					{!! $permissions->render() !!}
					</div>
				  @else
					<div class="star">No user found.</div>
				  @endif				
				  
			</div>	
		</div>
	</div>
</div>
@endsection	
