<div class="box box-primary">
<div class="box-body">
<!-- Role name -->
<div class="form-group">
    {!! Form::label('name', 'Permission Name: ') !!} <span class="star">*</span>
    {!! Form::text('name',null,['class' => 'form-control']) !!}
</div>
<!-- Display Name -->
<div class="form-group">
    {!! Form::label('display_name', 'Display Name: ') !!} <span class="star">*</span>
    {!! Form::text('display_name',null,['class' => 'form-control']) !!}
</div>
    <!-- Description -->	
<div class="form-group">
    {!! Form::label('description', 'Permission Description: ') !!} <span class="star">*</span>
    {!! Form::textarea('description',null,['class' => 'form-control','rows' => 2, 'cols' => 40]) !!}
</div>

<div class="form-group">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-primary']) !!}
</div>
</div>
</div>
