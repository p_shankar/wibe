@extends('admin.layout')
@section('title')
	Add Permission
@endsection
@section('heading')
	Add Permission
	<a class="btn btn-primary btn-sm pull-right" href="{{ url('/admin/permissions') }}">Back</a>
@endsection
@section('content')
<div class="row">
	<div class="col-md-8">
		
					@include('errors.user_error')
    
    {!! Form::model($page = new App\Models\Permission,['id' => 'add_template','method' => 'POST','url' => 'admin/permissions/add-permissions']) !!}
    
    @include('admin.permissions.form',['submitButtonText' => 'Add Permission'])
    
    {!! Form::close() !!}
	
	</div>
</div>
@endsection	
