@extends('admin.layout')
@section('title')
	Edit Permission		
@endsection
@section('heading')
	Edit Permission
	<a class="btn btn-primary btn-sm pull-right" href="{{ url('/admin/permissions') }}">Back</a>
@endsection
@section('content')
<div class="row">
	<div class="col-md-8">
		              
					@include('errors.user_error')
    
    {!! Form::model($permission,['id' => 'add_template','method' => 'post','url' => 'admin/permissions/edit-permission/'.$permission->id ]) !!}
    
    @include('admin.permissions.form',['submitButtonText' => 'Update Permission'])
    
    {!! Form::close() !!}
			
	</div>
</div>
@endsection	
