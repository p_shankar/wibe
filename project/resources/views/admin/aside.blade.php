<aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
			<?php
				$photo=Auth::user()->photo;
							if($photo==null)
							{
						?>
						<img src="{{ asset('assets/uploads/profile.png') }}" class="img-circle" alt="User Image" style="margin:4% 34%"/>
					<?php
							}
							else
							{
						?>
						<img src="{{ asset('uploads/'.$photo) }}" class="img-circle" alt="User Image" style="margin:4% 34%"/> <br> <br> <br>
					<?php	}	?>
            </div>
            <div class="pull-left info">
              <p>{{ Auth::user()->name }}</p>           
            </div>
          </div>
          
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
           
            <li class="@if($active == 'dashboard') active @endif">
              <a href="<?php echo URL::to('admin') ?>">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
              </a>
            </li>
			
			<!-- Manage Users -->
			<li class="@if($active == 'add-new' || $active == 'all-users') active @endif">
                <a href="">
                <i class="fa fa-users"></i> <span>Users</span><i class="fa fa-angle-left pull-right"></i>
              </a>
              
              <ul class="treeview-menu">
                <li class="@if($active == 'add-new') active @endif">
					<a href="{{ url('/admin/dashboard/add-new-user') }}"><i class="fa fa-user"></i> Add New</a>
				</li>
				<li class="@if($active == 'all-users') active @endif">
				<a href="<?php echo URL::to('admin/dashboard/users'); ?>">
                <i class="fa fa-users"></i> <span>View All</span>
              </a></li>
                </ul>
            </li>
			
			<!-- Manage Categories -->
			<li class="@if($active == 'category' || $active == 'sub-category') active @endif">
                <a href="">
                <i class="fa fa-tags"></i> <span>Manage Categories</span><i class="fa fa-angle-left pull-right"></i>
              </a>
              
              <ul class="treeview-menu">
                <li class="@if($active == 'category') active @endif">
					<a href="{{ url('/admin/category') }}"><i class="fa fa-tag"></i>Categories</a>
				</li>
				<li class="@if($active == 'sub-category') active @endif">
				<a href="<?php echo URL::to('admin/subcategory'); ?>">
                <i class="fa fa-tags"></i> <span>Sub-Categories</span>
              </a></li>
                </ul>
            </li>
            
            <!-- Manage Articles -->
            <li class="@if($active == 'all-articles' || $active == 'new-article') active @endif">
                <a href="">
                <i class="fa fa-tags"></i> <span>Manage Articles</span><i class="fa fa-angle-left pull-right"></i>
              </a>
              
              <ul class="treeview-menu">
                <li class="@if($active == 'new-article') active @endif">
					<a href="{{ url('/admin/articles/new-article') }}"><i class="fa fa-tag"></i>Add New</a>
				</li>
				<li class="@if($active == 'all-articles') active @endif">
				<a href="<?php echo URL::to('admin/articles'); ?>">
                <i class="fa fa-tags"></i> <span>View All</span>
              </a></li>
                </ul>
            </li>
            
            <!-- Manage Pages -->
		 
			<li class="@if($active == 'pages') active @endif">
              <a href="<?php echo URL::to('admin/pages'); ?>">
                <i class="fa fa-file-text-o"></i> <span>Manage CMS pages</span>
              </a>
            </li>
			
			<!-- Email Templates -->
			<li class="@if($active == 'template') active @endif">
              <a href="{{ url('admin/template') }}">
                <i class="fa fa-envelope"></i> <span>Email Templates</span>
              </a>
            </li>
			
			<!-- Manage Roles -->
			<li class="@if($active == 'roles') active @endif">
              <a href="<?php echo URL::to('admin/roles'); ?>">
                <i class="fa fa-plus-circle"></i> <span>Manage Roles</span>
              </a>
            </li>
            
            <!-- Manage Permissions -->
			<li class="@if($active == 'roles') active @endif">
              <a href="<?php echo URL::to('admin/permissions'); ?>">
                <i class="fa fa-plus-circle"></i> <span>Manage Permissions</span>
              </a>
            </li>
			
			<!-- Settings -->
			<li class="@if($active == 'settings') active @endif">
              <a href="<?php echo URL::to('admin/dashboard/settings'); ?>">
                <i class="fa fa-cogs"></i> <span>Settings</span>
              </a>
            </li>
			
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
