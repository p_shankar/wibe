<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
	<link rel="shortcut icon" type="image/png" href="{{ url('favicon.ico') }}"/>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ asset('assets/dist/css/AdminLTE.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
    <link href="{{ asset('assets/dist/css/skins/_all-skins.min.css') }}" rel="stylesheet" type="text/css" />
	<!-- Wait Me Loader -->
	<link href="{{ asset('/css/waitMe.css') }}" rel="stylesheet">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<link href="{{ asset('/css/developer.css') }}" rel="stylesheet">
	<link href="//cdnjs.cloudflare.com/ajax/libs/pnotify/2.0.0/pnotify.all.min.css" rel="stylesheet">
	<link href="{{ asset('/css/editor.css') }}" type="text/css" rel="stylesheet"/>
	
    <!-- ------------------------------------------------------- -->
	<meta name="csrf-token" content="{!! csrf_token() !!}" />
	@yield('css')
  </head>
   <style>
    .ta-editor {
        min-height: 300px;
        height: auto;
        overflow: auto;
        font-family: inherit;
        font-size: 100%;
    }
    </style>
  <body class="skin-blue">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo, set site_name variable in config/app.php -->
       <a href="{{ url('/admin') }}" class="logo"><img src="#" class="img-circle" style="height:40px; width:50px; margin-right:10px"/><b>{{ config('app.site_name') }}</b></a> 
       <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="{{ url('/admin') }}"  data-toggle="offcanvas" role="button">
            <span class="sr-only">
				
					  <a class="navbar-brand topnav" href="{{ url('/admin') }}"> {{ $test }} {{ Auth::user()->username }}!</a>
						 
					
			</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
					
					<?php
						$photo=Auth::user()->photo;
							if($photo==null)
							{
						?>
						<img src="{{ asset('assets/uploads/profile.png') }}" class="user-image" alt="User Image"/>
					<?php
							}
							else
							{
						?>
						<img src="{{ asset('uploads/'.$photo) }}" class="user-image" alt="User Image"/>
					<?php	}	?>
					
					
                  <span class="hidden-xs">{{ Auth::user()->username }}</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
				  
					
                    <?php
						$photo=Auth::user()->photo;
				
							if($photo==null)
							{
						?>
						<img src="{{ asset('assets/uploads/profile.png') }}" class="img-circle" alt="Admin Image"/>
					<?php
							}
							else
							{
						?>
						<img src="{{ asset('uploads/'.$photo) }}" class="img-circle" alt="Admin Image"/><br>
					<?php	}	?>
					
                    <p>
                      <a href="{{ url('admin/dashboard/change-photo') }}" style="color:white">Change Profile Picture</a> 
                    </p>
					<p>
                      <a href="{{ url('admin/dashboard/change-password') }}" style="color:white">Change Password</a> 
                    </p>
                  </li>
                  <!-- Menu Body -->
                  
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="{{ url('admin/dashboard/edit-profile') }}" class="btn btn-default btn-flat">Edit Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="{{ url('admin/auth/logout') }}" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      
