@extends('admin.layout')
@section('title')
	Settings
@endsection
@section('heading')
	Settings
@endsection
@section('content')
<div class="row">
	<div class="col-md-6">
		<div class="box box-primary">
			<div class="box-body">
			
			<form method="post" action="{{ url('/admin/dashboard/upload-logo') }}" role="form" enctype="multipart/form-data">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				@include('errors.user_error')	
			<div class="pull-left image col-xs-12">
             <?php
							/*if($logo[0]==null)
							{
						?>
						<img src="{{ asset('assets/uploads/profile.png') }}" class="img-circle" alt="User Image" style="height: 200px;
  margin: 2% 25%;"/>
					<?php
							}
							else
							{
						?>
						
						<div style="height: 200px; margin: 2% 5%;">
							<img src="{{ asset('images/logo/'.$logo[0]) }}" class="img-circle" alt="User Image" style="height: 200px"/>
						</div>
						
					<?php	}*/	?>
					<img src="{{ asset('assets/uploads/profile.png') }}" class="img-circle" alt="User Image" style="height: 200px;
  margin: 2% 25%;"/>
			  <br  /><br />
            </div>
			
			
					<input class="form-group" type="file" name="pic" id="pic"/>
			                      
				<button type="submit" class="btn btn-primary" name="submit" id="submit">Change Logo</button>
					
           

			</form>	
			</div>	
		</div>
	</div>
</div>
                @endsection	
