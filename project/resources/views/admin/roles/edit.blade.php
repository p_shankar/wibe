@extends('admin.layout')
@section('title')
	Roles		
@endsection
@section('heading')
	Edit {{ $role->name }} Role
@endsection
@section('content')
<div class="row">
	<div class="col-md-6">
		              
					@include('errors.user_error')
    
    {!! Form::model($role,['method' => 'POST','url' => 'admin/roles/edit/'.$role->id ]) !!}
    
    @include('admin.roles.form',['submitButtonText' => 'Update Role'])
    
    {!! Form::close() !!}
			
	</div>
</div>
@endsection	