@extends('admin.layout')
@section('title')
	Roles		
@endsection
@section('heading')
				Roles
				<a href="{{url('admin/roles/create')}}" class="btn btn-primary btn-sm pull-right">Add Role</a>
@endsection
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-body">
			
				@if ( !$roles->count() ) <div class="star">No roles found</div> @else
				<table id="example2" class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>S.no</th>
							<th>Name</th>
							<th>Display Name</th>
							<th>Description</th>
							<th>Action</th>
						</tr>
					</thead>
					
					<?php $index= ($roles->currentPage() - 1) * $roles->perPage() + 1; ?>
					@foreach( $roles as $role )
					<tr>
						<td>{{ $index }}</td>
						<td>{{ $role->name }}</td>
						<td>{{ $role->display_name }}</td>
						<td>{{ $role->description }}</td>
						<td>
						{!! Form::open(['id' => 'deletePageForm','url' => 'admin/roles/delete/'.$role->id,'method' => 'POST' ]) !!}
                            <a href="{{ url('admin/roles/edit/'.$role->id) }}" style="color:orange" title="Edit"><span class="fa fa-edit" aria-hidden="true"></span></a>
					
                            <a href="javascript:void(0);" class="deleteRecord" data-confirm-message="Are you sure you want to delete this role?" style="color:red" title="Delete"><span class="fa fa-times" aria-hidden="true"></span></button>
                        {!! Form::close() !!}
						</td>
					</tr>
					<?php  $index++;  ?>
					@endforeach
					</tbody>
				</table>
				{!! $roles->render() !!} @endif
			</div>	
		</div>
	</div>
</div>
@endsection