@extends('admin.layout')
@section('title')
	Roles		
@endsection
@section('heading')
	Add Role
@endsection
@section('content')
<div class="row">
	<div class="col-md-6">
		
					@include('errors.user_error')
    
    {!! Form::model($role = new App\Models\Role,['method' => 'POST','url' => 'admin/roles/create']) !!}
    
    @include('admin.roles.form',['submitButtonText' => 'Add Role'])
    
    {!! Form::close() !!}
	
	</div>
</div>
@endsection	