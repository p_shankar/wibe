<div class="box box-primary">
<div class="box-body">
<!-- Role name -->
<div class="form-group">
    {!! Form::label('name', 'Role Name: ') !!} <span class="star">*</span>
    {!! Form::text('name',null,['class' => 'form-control']) !!}
</div>
<!-- Display Name -->
<div class="form-group">
    {!! Form::label('display_name', 'Display Name: ') !!} <span class="star">*</span>
    {!! Form::text('display_name',null,['class' => 'form-control']) !!}
</div>
    <!-- Description -->	
<div class="form-group">
    {!! Form::label('description', 'Role Description: ') !!} <span class="star">*</span>
    {!! Form::textarea('description',null,['class' => 'form-control','rows' => 2, 'cols' => 40]) !!}
</div>
<!-- Permissions -->
<div class="form-group">
	{!! Form::label('permissions', 'Permissions: ') !!} <span class="star">*</span>
	
	@foreach($permissions as $per)
		{!! Form::label($per->name, $per->name .': ') !!}
		@if(!empty($rolePer) && in_array($per->id,$rolePer))
			{!! Form::checkbox('permission[]',$per->id,true) !!}
		@else
			{!! Form::checkbox('permission[]',$per->id,false) !!}
		@endif
		&nbsp;&nbsp;&nbsp;
	@endforeach

</div>

<div class="form-group">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-primary']) !!}
</div>
</div>
</div>
