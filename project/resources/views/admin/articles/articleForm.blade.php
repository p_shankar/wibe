@extends('admin.layout')
@section('title')
	New Article		
@endsection
@section('heading')
	New Article
	<a href="{{url('admin/articles')}}" class="btn btn-primary btn-sm pull-right">Back</a>
@endsection
@section('content')

<div class="row">
	
	{!! Form::model($article = new App\Models\Article,['id' => 'Articleform','files'=>'true','url' => 'admin/articles/new-article','files' => true ]) !!}
	<div class="col-md-12">
             
	<div class="box box-primary">
	  <div class="box-body">
	  @include('errors.user_error')
		@include('admin.articles.form')
		<div class="form-group">
			{!! Form::submit('Submit', ['id' => 'submit','class' => 'btn btn-primary']) !!}
		</div>
	
      </div>
    </div>
	</div>    
    {!! Form::close() !!}
			
	</div>

@endsection	
