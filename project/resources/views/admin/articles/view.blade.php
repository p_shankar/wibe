@extends('admin.layout')
@section('title')
	Article Details
@endsection
@section('heading')
	Article Details
	<a href="{{url('admin/articles')}}" class="btn btn-primary btn-sm pull-right">Back</a>
@endsection
@section('content')
<div class="row">
	<div class="col-md-6">
		<div class="box box-primary">
			
			<div class="box-body">
					@include('errors.user_error')
					
					<table class="table table-bordered">
						<tr>
							<th>Title : </th>
							<td>{{$article->title}}</td>
						</tr>
						<tr>
							<th>Category : </th>
							<td>{{$category->type_name}}</td>
						</tr>
						<tr>
							<th>Description : </th>
							<td>{{$article->description}}</td>
						</tr>
						<tr>
							<th>Image : </th>
							<td><img src="{{ asset('assets/article-images/'.$article->image) }}" height="200" width="200"></td>
						</tr>
						
						<tr>
							<th>Status : </th>
							<td>@if($article->status == 0) Unpublished @else Published @endif</td>
						</tr>				
						
					</table>
			</div>	
		</div>
	</div>
</div>
@endsection	
