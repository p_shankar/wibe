@if(!empty($subcategory))
	<div class="form-group">
		{!! Form::label('sub category', 'Sub Category: ') !!}
		{!! Form::select('sub_categories_id',[''=>'---Select a Category---']+$subcategory,null,['id'=>'article_sub_category_id','class' => 'form-control']) !!}
	</div>
@endif
