@extends('admin.layout')
@section('title')
	Articles		
@endsection
@section('heading')
				Articles
				<a href="{{url('admin/articles/new-article')}}" class="btn btn-primary btn-sm pull-right">Create Article</a>
@endsection
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-body">
			
				@if ( !$articles->count() ) <div class="star">You have no articles</div> @else
				<table id="example2" class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>S.no</th>
							<th>Title</th>
							<th>Description</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					
					<?php $index= ($articles->currentPage() - 1) * $articles->perPage() + 1; ?>
					@foreach( $articles as $article )
					
					@if($article->delete_status != 1 )
					<tr>
						<td>{{ $index }}</td>
						<td>{{ substr($article->title,'0','10') }}...</td>
						<td>{{ substr($article->description,'0','25') }}...</td>
						<td>
							 {!! Form::open(['method' => 'post' ]) !!}
								 <!-- set status -->
								  {!! $article->setStatus($article->id, $article->status) !!}
								 <!-- set featured -->
								  {!! $article->setFeatured($article->id, $article->featured) !!}
							 {!! Form::close() !!}
						</td>
						<td>
                            {!! Form::open(['id' => 'deletePageForm','url' => 'admin/articles/delete-article/'.$article->id,'method' => 'delete' ]) !!}
								<!-- view article -->
								<a href="articles/view-article/{{ base64_encode($article->id)}}" class="viewRecord" title="View"><span class="fa fa-eye" aria-hidden="true"></span></a>
								<!-- edit article -->
								<a href="articles/edit-article/{{base64_encode($article->id)}}" style="color:orange" title="Edit"><span class="fa fa-edit" aria-hidden="true"></span></a>
								<!-- Delete article -->
								<a href="javascript:void(0);" class="deleteRecord" data-confirm-message="Are you sure you want to delete this article?" style="color:red" title="Delete"><span class="fa fa-times" aria-hidden="true"></span></button>
							{!! Form::close() !!}
                        </td>
					</tr>
					@endif
					<?php  $index++;  ?>
					@endforeach
					</tbody>
				</table>
				{!! $articles->render() !!} @endif
			</div>	
		</div>
	</div>
</div>
@endsection
