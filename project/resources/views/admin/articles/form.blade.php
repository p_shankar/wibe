		<!-- Title -->
		<div class="form-group">
			{!! Form::label('title', 'Title: ') !!} <span class="star">*</span>
			{!! Form::text('title',null,['class' => 'form-control']) !!}
		</div>
		<!-- Categories -->
		<div class="form-group">
			{!! Form::label('category', 'Category: ') !!} <span class="star">*</span>
			{!! Form::select('category_id',[''=>'---Select a Category---']+$categories,null,['id'=>'article_category_id','class' => 'form-control']) !!}
		</div>
		<!-- sub categories -->
		<div id="load_subCategories"style="display:none">
			<!-- load sub categories here -->
		</div>
		<!-- description -->
		<div ng-app="textAngularTest" ng-controller="wysiwygeditor" class="container app">
			{!! Form::label('Description', 'Description: ') !!} <span class="star">*</span>
			<!-- Text angular editor -->
			<div text-angular="text-angular" name="htmlcontent" ng-model="htmlcontent" ta-disabled='enable' style="width:960px"></div>
			<!-- actual text box from where description will be passed to the controller -->
			{!! Form::textarea('description',null,['id' => 'articleDesc','class' => 'form-control','ng-model'=>'htmlcontent','style'=>'display:none']) !!}
		</div>
		<!-- Article Image -->
		<br>
		<div class="form-group">
			<label for="image">Image: </label>
			{!! Form::file('article-image') !!}
			@if(!empty($article->image))
			   <img src="{{ asset('assets/article-images/'.$article->image) }}" height="200" width="200">
			@endif
		</div>
		<!-- status -->
		@include('partials.status', ['status' => $article->status])
