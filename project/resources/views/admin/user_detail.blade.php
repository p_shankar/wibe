
{{-- */use repositories\CommonRepositoryInterface;/* --}}
{{-- */use repositories\CommonRepository;/* --}}


<?php $common = new CommonRepository(); ?>
@extends('admin.layout')
@section('title')
	User Details
@endsection
@section('heading')
	User Details
@endsection
@section('content')
<div class="row">
	<div class="col-md-6">
		<div class="box box-primary">
			
			<div class="box-body">
					@include('errors.user_error')
					
					<table class="table table-bordered">
						<tr>
							<th>Email Address : </th>
							<td>{{$user->email}}</td>
						</tr>
						<tr>
							<th>Name : </th>
							<td>{{$user->name}}</td>
						</tr>
						<tr>
							<th>Gender : </th>
							<td>@if($user->gender == 0) Male @else Female @endif</td>
						</tr>
						
						<tr>
							<th>Date of Birth : </th>
							<td>{{$user->dob}}</td>
						</tr>	
						
						<tr>
							<th>Contact Number : </th>
							<td>{{$user->phone}}</td>
						</tr>
						
						<tr>
							<th>Country : </th>
							<td>{{$user->country->name}}</td>
						</tr>
						
						<tr>
							<th>State : </th>
							<td>{{$user->state->name}}</td>
						</tr>
						
						<tr>
							<th>Zip Code : </th>
							<td>{{$user->zip}}</td>
						</tr>
						
						<tr>
							<th>Address : </th>
							<td>{{$user->address}}</td>
						</tr>
						
						<tr>
							<th>Role : </th>
							{{-- */ $userRole = $common->get_user_role($user->id); /* --}}
							<td>
								@if($userRole != '1')
									{{ $userRole }}
								@endif
							</td>
						</tr>					
						
					</table>
			</div>	
		</div>
	</div>
</div>
@endsection	
