@if ( !$subcategory->count() ) <div class="star">There is no sub-category</div> @else
				<table id="example2" class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>S.no</th>
							<th>Name</th>
							<th>Category</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<?php $index= ($subcategory->currentPage() - 1) * $subcategory->perPage() + 1; ?>
					@foreach( $subcategory as $subcategories )
					<tr>
						<td>{{ $index }}</td>
						<td>{{ $subcategories->name }}</td>
						<td>{{ $subcategories->categories->type_name }}</td>
						<td>
						  {!! Form::open(['method' => 'post' ]) !!}
						      {!! $subcategories->setStatus($subcategories->id, $subcategories->status) !!}
						  {!! Form::close() !!}
						</td>
						<td>
						{!! Form::open(['id' => 'deletePageForm','url' => 'admin/subcategory/'.$subcategories->id,'method' => 'delete' ]) !!}
                            <a href="subcategory/{{$subcategories->id}}/edit" style="color:orange" title="Edit"><span class="fa fa-edit" aria-hidden="true"></span></a>
					
                            <!-- <a href="javascript:void(0);" class="deleteRecord" data-confirm-message="Are you sure you want to delete this sub-category?" style="color:red" title="Delete"><span class="fa fa-times" aria-hidden="true"></span></a> -->
                        {!! Form::close() !!}
						</td>
					</tr>
					<?php  $index++;  ?>
					@endforeach
					</tbody>
				</table>
				{!! $subcategory->render() !!}
				@endif