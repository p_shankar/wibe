<div class="box box-primary">
<div class="box-body">

<div class="form-group">
    {!! Form::label('title', 'Name: ') !!} <span class="star">*</span>
    {!! Form::text('name',null,['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('description', 'Description: ') !!} <span class="star">*</span>
    {!! Form::textarea('description',null,['class' => 'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::label('category','Category:') !!} <span class="star">*</span>
	{!! Form::select('category_id',$categories,null,['id'=>'category_id','class' => 'form-control']) !!}
</div>

@include('partials.status', ['status' => $subcategory->status])

<div class="form-group">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-primary']) !!}
</div>
</div>
</div>