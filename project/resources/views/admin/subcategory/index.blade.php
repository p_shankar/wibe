@extends('admin.layout')
@section('title')
	Sub-Categories		
@endsection
@section('heading')
				Sub-Categories
@endsection
@section('content')	
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-body">
			
					
							<div id="cat" class="col-md-3 form-group">
							<label>Category</label>
							{!! Form::select('category_id',[''=>'---Select a Category---']+$categories,null,['id'=>'category_id','class' => 'form-control']) !!}
							</div><br>

                  <div class="col-md-1">
					<button type="submit" class="btn btn-primary btn-danger btn-sm" id="filter">Filter</button>
                  </div>
                    <div class="col-md-1">
                  <a  href="{{url('admin/subcategory')}}" class="btn btn-primary btn-primary btn-sm">Reset</a>
                  </div>
              <br><br><br>
			
				<div id="ajax_records"></div>
				@if ( !$subcategory->count() ) <div class="star">There is no sub-category</div> @else
				<table id="example2" class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>S.no</th>
							<th>Name</th>
							<th>Category</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<?php $index= ($subcategory->currentPage() - 1) * $subcategory->perPage() + 1; ?>
					@foreach( $subcategory as $subcategories )
					<tr>
						<td>{{ $index }}</td>
						<td>{{ $subcategories->name }}</td>
						<td>{{ $subcategories->categories->type_name }}</td>
						<td>
						  {!! Form::open(['method' => 'post' ]) !!}
						      {!! $subcategories->setStatus($subcategories->id, $subcategories->status) !!}
						  {!! Form::close() !!}
						</td>
						<td>
						{!! Form::open(['id' => 'deletePageForm','url' => 'admin/subcategory/'.$subcategories->id,'method' => 'delete' ]) !!}
                            <a href="subcategory/{{$subcategories->id}}/edit" style="color:orange" title="Edit"><span class="fa fa-edit" aria-hidden="true"></span></a>
					
                            <!-- <a href="javascript:void(0);" class="deleteRecord" data-confirm-message="Are you sure you want to delete this sub-category?" style="color:red" title="Delete"><span class="fa fa-times" aria-hidden="true"></span></a> -->
                        {!! Form::close() !!}
						</td>
					</tr>
					<?php  $index++;  ?>
					@endforeach
					</tbody>
				</table>
				<div id="render">
				{!! $subcategory->render() !!}
				</div>	@endif
			</div>	
		</div>
	</div>
</div>
@endsection
@section('js')
<script src="{{ asset('js/loader.js') }}" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function() {

		$("#filter").click(function()
		{
						var category = $('#category_id').val();

					if(category==""||category==null)
					{
						$("#cat").addClass(".form-group has-error");
						return false;
					} 
					$.ajax({
						url : path+"admin/subcategory",
						type: 'get',
						data : {
							'category'		 : category
						},
						dataType : 'html',
						beforeSend : function() {
							addLoader();
							$("#cat").removeClass(".form-group has-error");
						},
						complete : function() {
							removeLoader();
						},
						success : function(html) {
							$('#example2').html(html);
							//$('#render').hide();
							//$('#ajax_records').html(html);
						},
						error : function(xhr, ajaxOptions, thrownError) {
													console.log(xhr);
								
									new PNotify({
										type: 'error',
										title: xhr.responseText,
										text: 'Something went wrong!!!'
									});
							}
					});
							
				})	
	});
</script>
@endsection
