@extends('admin.layout')
@section('title')
	Create Sub-Category
@endsection
@section('heading')
	Create Sub-Category		
@endsection
@section('content')
<div class="row">
	<div class="col-md-6">
	@include('errors.user_error')
    
    {!! Form::model($subcategory = new App\Models\SubCategory,['url' => 'admin/subcategory','files' => true ]) !!}
    
    @include('admin.subcategory.form',['submitButtonText' => 'Add Sub-Category'])
    
    {!! Form::close() !!}
			</div>	
		</div>
	</div>
</div>
@endsection