@extends('admin.layout')
@section('title')
	Edit Sub-Category		
@endsection
@section('heading')
	Edit {{ $subcategory->name }} Sub-Category
@endsection
@section('content')
<div class="row">
	<div class="col-md-6">               
					@include('errors.user_error')
    
    {!! Form::model($subcategory,['method' => 'PATCH','url' => 'admin/subcategory/'.$subcategory->id ]) !!}
    
    @include('admin.subcategory.form',['submitButtonText' => 'Update Sub-Category'])
    
    {!! Form::close() !!}
			</div>	
		</div>
	</div>
</div>
@endsection