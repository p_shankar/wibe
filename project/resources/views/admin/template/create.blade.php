@extends('admin.layout')
@section('title')
	Create Template		
@endsection
@section('heading')
	Create Template	
@endsection
@section('content')
<div class="row">		
					@include('errors.user_error')
    
    {!! Form::model($template = new App\Models\EmailTemplate,['id' => 'add_template','url' => 'admin/template']) !!}
    
    @include('admin.template.form')
</div>
</div>
	<div class="col-md-3">
				<div class="input_fields_wrap">
					{!! Form::button('Add More Attributes', ['id' => 'add_field_button','class' => 'btn btn-primary form-control']) !!}
					<div><br>
					{!! Form::text('variable[]',null,['class' => 'field']) !!}
					</div>
				</div>
			</div>
			<div class="form-group">
				{!! Form::submit('Add Template', ['class' => 'btn btn-primary form-control']) !!}
			</div>
	{!! Form::close() !!}

</div>			
</div>
@endsection
@section('js')
<script type="text/javascript">
	$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $("#add_field_button"); //Add button ID
    
    var x = 1; //initial text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div><input type="text" class="field" name="variable[]"/><a href="#" class="remove_field">Remove</a></div>'); //add input box
        }
    });
    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});
	</script>
@endsection