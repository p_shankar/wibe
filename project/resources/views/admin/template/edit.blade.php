@extends('admin.layout')
@section('title')
	Edit Template		
@endsection
@section('heading')
				Edit {{ $template->name }} Template
@endsection
@section('content')
<div class="row">
             
					@include('errors.user_error')
    
    {!! Form::model($template,['method' => 'PATCH','id' => 'add_template','url' => 'admin/template/'.$template->id ]) !!}
    
    @include('admin.template.form')
    
    {!! Form::close() !!}
</div>
</div>
  
			<div class="col-md-3">
			<div class="form-group">
				{!! Form::label('attribute', 'Select Attributes: ') !!}
				{!! Form::select('attribute',$attributes,null,['id'=>'attribute','class' => 'form-control','multiple']) !!}
			</div>
			</div>
			<div class="form-group">
				{!! Form::submit('Update Template', ['class' => 'btn btn-primary form-control']) !!}
			</div>
	{!! Form::close() !!}

</div>			
</div>
@endsection
@section('js')
<script type="text/javascript">
		$(document).ready(function() {
			$('select').change(function() {
			var currentVal = $('#textarea').val();
			$('#textarea').append(currentVal + $(this).val()); 
		});
});
</script>
@endsection