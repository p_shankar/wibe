@extends('admin.layout')
@section('title')
	Templates		
@endsection
@section('heading')
	Email Templates
	<a href="{{url('admin/template/create')}}" disabled class="btn btn-primary btn-sm pull-right">Create Template</a>
@endsection
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-body">
			
				@if ( !$templates->count() ) <div class="star">You have no email template</div> @else
				<table id="example2" class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>S.no</th>
							<th>Name</th>
							<th>Subject</th>
							<th>Content</th>
							<th>Action</th>
						</tr>
					</thead>
					
					<?php $index= ($templates->currentPage() - 1) * $templates->perPage() + 1; ?>
					@foreach( $templates as $template )
					<tr>
						<td>{{ $index }}</td>
						<td>{{ $template->name }}</td>
						<td>{{ $template->subject }}</td>
						<td>{{ substr(strip_tags($template->content),0,100) }}</td>
						<td>
						{!! Form::open(['url' => 'admin/template/'.$template->id,'method' => 'delete' ]) !!}
						 <a href="template/{{$template->id}}/edit" style="color:orange" title="Edit"><span class="fa fa-edit" aria-hidden="true"></span></a>
						 
						<!-- <a href="javascript:void(0);" class="deleteRecord" data-confirm-message="Are you sure you want to delete this template?" style="color:red" title="Delete"><span class="fa fa-times" aria-hidden="true"></span></a> -->
						{!! Form::close() !!}
						</td>
					</tr>
					<?php  $index++;  ?>
					@endforeach
					</tbody>
				</table>
				{!! $templates->render() !!} @endif
			</div>	
		</div>
	</div>
</div>
@endsection