<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff">
  <tr>
    <td><table width="600" border="0" cellspacing="0" cellpadding="0"  align="center"  style="" >
      
        <tr>
          <td>

		  @include('emails.header')
		  </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>
		  
	 @yield('content')  
		
			
			</td>
        </tr>
        
        <tr>
          <td>&nbsp;</td>
        </tr>
       
        <tr>
          <td>
		  
	  @include('emails.footer')
</td>
</tr>
<tr>
<td align="center" bgcolor="#0E4EC4">
<font style="color:#fff; font-size:10px"></font></td>
</tr>
<tr>
<td bgcolor="#eee">&nbsp;</td>
</tr>
</table>
</td>
</tr>
</table>

