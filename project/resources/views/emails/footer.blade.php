<table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: 'Open Sans', Arial, sans-serif"  bgcolor="#0E4EC4">
<tr>
<td colspan="3">&nbsp;
</td>
</tr>
<tr>
<td width="60%" align="center"><font style=" color:#000; font-size:9px; text-transform:uppercase; font-family: 'Open Sans', sans-serif;">
<a href= "{{ url('/') }}" style="color:#fff; text-decoration:none; padding-right:10px">Home </a>
<a href= "#" style="color:#fff; text-decoration:none ; padding-right:10px">Terms and Conditions </a>
<a href= "#" style="color:#fff; text-decoration:none; padding-right:10px">Privacy Policy </a>
</font></td>
<td width="38%" align="right" valign="middle">
<a href="{{ url('/') }}" style=" width:30px; height:30px; display:inline-block;" target="_blank"><img src="{{ asset('/images/email_template/emailsoc1.png') }}" alt="facebook" border="0" /></a>
<a href="{{ url('/') }}" style=" width:30px; height:30px; display:inline-block;" target="_blank"><img src="{{ asset('/images/email_template/emailsoc2.png') }}" alt="Twitter"  border="0" /></a>
<a href="{{ url('/') }}" style=" width:30px; height:30px; display:inline-block;" target="_blank"><img src="{{ asset('/images/email_template/emailsoc3.png') }}" alt="Linkedin" border="0" /></a>
<a href="{{ url('/') }}" style=" width:30px; height:30px; display:inline-block;" target="_blank"><img src="{{ asset('/images/email_template/emailsoc4.png') }}" alt="Youtube" border="0" /></a>
</td>
<td width="2%" align="right" valign="middle">&nbsp;</td>
</tr>
</table>