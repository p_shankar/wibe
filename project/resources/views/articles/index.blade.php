@extends('admin.layout')
@section('title')
	Categories		
@endsection
@section('heading')
				Articles
				<a href="{{url('admin/articles/new-article')}}" class="btn btn-primary btn-sm pull-right">Create Article</a>
@endsection
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-body">
			
				@if ( !$articles->count() ) <div class="star">You have no articles</div> @else
				<table id="example2" class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>S.no</th>
							<th>Title</th>
							<th>Description</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					
					<?php $index= ($articles->currentPage() - 1) * $articles->perPage() + 1; ?>
					@foreach( $articles as $article )
					<tr>
						<td>{{ $index }}</td>
						<td>{{ $article->title }}</td>
						<td>{{ $article->description }}</td>
						<td>
						 
						</td>
						<td>
						
                            <a href="articles/{{$article->id}}/edit" style="color:orange" title="Edit"><span class="fa fa-edit" aria-hidden="true"></span></a>
					
                           
						</td>
					</tr>
					<?php  $index++;  ?>
					@endforeach
					</tbody>
				</table>
				{!! $article->render() !!} @endif
			</div>	
		</div>
	</div>
</div>
@endsection
