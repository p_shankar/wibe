<?php

return [

	/*
	|--------------------------------------------------------------------------
	| oAuth Config
	|--------------------------------------------------------------------------
	*/

	/**
	 * Storage
	 */
	'storage' => 'Session',

	/**
	 * Consumers
	 */
	'consumers' => [

		'Facebook' => [
			'client_id'     => '403165833211744',
			'client_secret' => '642c521e6a437ddff4e4af3183591e01',
			'scope'         => ['email'],
		],
		
		'Google' => [
			'client_id'     => '94830592805-crt0jjd2q13mlmenn5o48op2v75a6kud.apps.googleusercontent.com',
			'client_secret' => '85vS6AZX5b-GFlRv5VlhdVDp',
			'scope'         => ['userinfo_email', 'userinfo_profile'],
		], 
	
		'Linkedin' => [
			'client_id'     => '75fga7c6183pfo',
			'client_secret' => '4tQxlV32HlyTqDNU',
			'scope' 		=> ['r_basicprofile','r_emailaddress'],
		]

	]
];