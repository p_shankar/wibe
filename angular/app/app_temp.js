var app = angular.module('wibe_app', ['ngRoute', 'ui.bootstrap', 'ngAnimate']);

app.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider
    .when('/index', {
      title: 'indexPage',
      templateUrl: 'app/partials/index/indexpage.html',
      controller: 'indexpageController',
      controllerAs: 'i_ctrl'
    })
.when('/article/:id', {
      title: 'Article Detail',
url:'article',
      templateUrl: 'angular/app/partials/article/detailPage.html',
      controller: 'articleController',
      controllerAs: 'art_ctrl'
    })
 .when('/articlecat/:id', {
                    title: 'ArticleCat',
                    templateUrl: 'angular/app/partials/article_cat/article_cat.html',
                    controller: 'articleCatController',
                    controllerAs: 'art_cat_ctrl'
                })
	.when('/vehicles', {
      title: 'vehiclePage',
      templateUrl: 'angular/app/partials/vehicles/vehicles.html',
      controller: 'vehiclesController',
      controllerAs: 'veh_ctrl'
    })
   .otherwise({
      redirectTo: '/index'
    });;

}]);
    
