app.controller('navbarController', function($scope, $modal, $filter, Data) {
    $scope.categories = {};
    
	Data.get('categories').then(function(data) {
        $scope.categories = data.categories;
    });
});