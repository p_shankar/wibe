app.controller('articleController', function($scope, $modal, $filter, Data,$routeParams) {
$scope.article={};
    Data.get('article/'+$routeParams.id).then(function(data) {
        $scope.articles = data;
    });
});

app.filter('highlight', function($sce) {
    return function(text, phrase) {
      if (phrase) text = text.replace(new RegExp('('+phrase+')', 'gi'),
        '<span class="highlighted">$1</span>')

      return $sce.trustAsHtml(text)
    }
  });
