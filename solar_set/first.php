<?php
  
 
require_once( 'SolrPHPClient/Apache/Solr/Service.php' );
require_once( 'SolrPHPClient/Apache/Solr/Response.php' );
require_once( 'SolrPHPClient/Apache/Solr/Document.php' );
  
  // http://112.196.27.243:8983/solr
  // 
  // Try to connect to the named server, port, and url
  // 
  $solr = new Apache_Solr_Service('112.196.27.243', '8983', '/solr' );

var_dump($solr);
  
  if ($solr->ping() ) {
    echo 'Solr service not responding.';
    exit;
  }
  die;
  //
  //
  // Create two documents
  //
  $docs = array(
    'doc_no1' => array(
      'id' => 1,
      'title_s' => 'Alphabet',
      'desc_txt' => 'Franz jagt im komplett verwahrlosten Taxi quer durch Bayern'
    ),
    'doc_no2' => array(
      'id' => 2,
      'title_s' => 'Buchstaben',
      'desc_txt' => 'Polyfon zwitschernd assen Mäxchens Vögel Rüben, Joghurt und Quark.'
    ),
  );
    
  $documents = array();
  
  foreach ( $docs as $item => $fields ) {
    
    $part = new Apache_Solr_Document();
    
    foreach ( $fields as $key => $value ) {
      if ( is_array( $value ) ) {
        foreach ( $value as $data ) {
          $part->setMultiValue( $key, $data );
        }
      }
      else {
        $part->$key = $value;
      }
    }
    
    $documents[] = $part;
  }
    
  //
  //
  // Load the documents into the index
  // 
  try {
    $solr->addDocuments( $documents );
    $solr->commit();
    $solr->optimize();
  }
  catch ( Exception $e ) {
    echo $e->getMessage();
  }
  
  //
  // 
  // Run some queries. 
  //
  $offset = 0;
  $limit = 10;
  
  $queries = array(
    'id: 1 OR id: 2',
    'category: Birne',
    'title: Buchstaben'
  );

  foreach ( $queries as $query ) {
    $response = $solr->search( $query, $offset, $limit );
    
    if ( $response->getHttpStatus() == 200 ) { 
      // print_r( $response->getRawResponse() );
      
      if ( $response->response->numFound > 0 ) {
        echo "$query <br />";

        foreach ( $response->response->docs as $doc ) { 
          echo "$doc->id $doc->title <br />";
        }
        
        echo '<br />';
      }
    }
    else {
      echo $response->getHttpStatusMessage();
    }
  }


?>