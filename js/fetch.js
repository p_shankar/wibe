$("#country_id").change(function()
 { 
   addLoader();
  var country = $('#country_id').find(":selected").val();
  var formData = {
   cid  : country,
  }
   
  formData._token = $('meta[name="csrf-token"]').attr('content');
  $.ajax({
   type     : 'POST',
   url      : path+'admin/dashboard/state',
   data     : formData,
   datatype : 'html',
   success  : function(data) {
    console.log(data); 
    removeLoader();
	$('#state').hide();
    $('#mystates').html(data);
    
   },
     
   error: function(data) {
   // Error...
    var errors = data.responseJSON;
      console.log(errors);
   }
   
  })
 });
 
 function addLoader(){
	$('#waitMeLoader').waitMe({
	effect : 'win8', 
	text : '', 
	bg : 'rgba(255,255,255,0.7)', 
	color : '#000', 
	sizeW : '20px', 
	sizeH : '20px', 
	source : ''
	});
}

function removeLoader(){
	$('#waitMeLoader').waitMe("hide");
}