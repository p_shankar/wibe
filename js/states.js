$("#country_id").change(function()
 { 
 addLoader();
  var country = $('#country_id').find(":selected").val();
  var formData = {
   cid  : country,
  }
   
  formData._token = $('meta[name="csrf-token"]').attr('content');
  $.ajax({
   type     : 'POST',
   url      : path+'user/state',
   data     : formData,
   datatype : 'html',
   success  : function(data) {
    console.log(data); 
   removeLoader();
		if(data.success == false)
		{
			$('#state').show();
			$('#state').html('').append('<option> -- Select State -- </option>');
			$('#mystates').hide();
			new PNotify({
					   type: 'error',
					   title: 'Error',
					   text: 'Sorry, something went wrong. Please try again.'
					   });
		}
		else
		{
		$('#state').hide();
		$('#mystates').html(data);
		}
    
   },
     
   error: function(data) {
   // Error...
    var errors = data.responseJSON;
      console.log(errors);
	  new PNotify({
					   type: 'error',
					   title: 'Error',
					   text: 'Sorry, something went wrong. Please try again.'
					   });
   }
   
  })
 });