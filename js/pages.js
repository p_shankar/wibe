$(function() {
	
	$(document).on('click','.changeStatus', function(){
		var $this = $(this);
		var table = $this.data('table');
		var id = $this.data('id');
		var status = $this.data('status');
		bootbox.confirm('Are you sure you want to '+(status ? "activate" : "deactivate")+' this?', function(result) {
			if(result){
				$.ajax({
					url: path+'admin/dashboard/change-status',
					data : $this.closest('form').serialize()+'&id='+id+'&table='+table+'&status='+status,
					dataType: 'json',
					type: 'post',
					beforeSend: function(){
						$this.html('<i class="fa fa-spin fa-spinner"></i>');
					},
					complete: function(){
						
					},
					success: function(json){
						console.log(json);
						if (json.success) {
							if(status){
								$this.data('status',0);
								$this.html('<i class="fa fa-circle active"></i>');
								new PNotify({
								   type: 'success',
								   title: 'Success',
								   text: 'Status changed'
								   });
							}
							else{
								$this.data('status',1);
								$this.html('<i class="fa fa-circle inactive"></i>');
								new PNotify({
								   type: 'success',
								   title: 'Success',
								   text: 'Status changed'
								   });
							}
						} else if (json.exception_message) {
							if(status){
								$this.html('<i class="fa fa-circle inactive"></i>');
							}
							else{
								$this.html('<i class="fa fa-circle active"></i>');
							}
							new PNotify({
								   type: 'error',
								   title: 'Error',
								   text: 'Sorry, Something went wrong!!'
								   });
						}
					},
					error : function(xhr, ajaxOptions, thrownError) {
						
					}
				});
			}
		});
	});
	
	$('.deleteRecord').on('click', function(){
		$this = $(this);
		bootbox.confirm($this.data('confirm-message'), function(result) {
			if(result){
				$($this).closest('form').submit();
			}
		});
	});
	
	$("#delete_multiple_record").on('click',function(){
		addLoader();
    var action = $('input[name=action_name]').val();
	var formData = {
        table : $('input[name=table_name]').val(),
        id : $('input[name=record_id]').val()
	}
	formData._token = $('meta[name="csrf-token"]').attr('content');
	$.ajax({
		type     : 'POST',
		url      : path+'admin/'+action+'/destroy',
		data     : formData,
		datatype : 'html',
		success  : function(data) {
			removeLoader();
			if(data.success == true)
			{
				window.location = data.redirect_url;
			}
			if(data.success == false) 
			{
				window.location = data.redirect_url;
			}
		},
		error: function(data) {
			 var errors = data.responseJSON;
		}
	});
});

});
