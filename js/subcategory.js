$(function(){
$("#category_id").change(function()
 { 
  var category = $('#category_id').find(":selected").val();
  var formData = {
   cid  : category,
  }
   
  formData._token = $('meta[name="csrf-token"]').attr('content');
  $.ajax({
    type     : 'POST',
    url      : path+'story/subcategory',
    data     : formData,
    datatype : 'html',
    beforeSend : function() {
		addLoader();
	},
	complete : function() {
		removeLoader();
	},
   success  : function(data) {
    console.log(data); 
		if(data.success == false)
		{
			$('#subcategory_id').show();
			$('#subcategory_id').html('').append('<option> -- Select a Sub-Category -- </option>');
			$('#subcategory').hide();
			new PNotify({
					   type: 'error',
					   title: 'Error',
					   text: 'Sorry, something went wrong. Please try again.'
					   });
		}
		else
		{
			$('#subcategory_id').hide();
			$('#subcategory').html(data);
		}
	
    
   },
     
   error: function(data) {
   // Error...
    var errors = data.responseJSON;
      console.log(errors);
	  new PNotify({
					   type: 'error',
					   title: 'Error',
					   text: 'Sorry, something went wrong. Please try again.'
					   });
   }
   
  })
 });
 
$(document).on('click', '.pagination a', function (e) 
 {
	addLoader();
	var url=$(this).attr('href');
	if(url.indexOf('category') > 0)
	{
		var filter_category = getParameterByName('category',url);
		var filter_subcategory = getParameterByName('subcategory',url);
		loadFilteredStories(url.split('page=')[1],filter_category,filter_subcategory);
		e.preventDefault();
	}
	else{
		loadStories(url.split('page=')[1]);
		e.preventDefault();
	}
	
 });	

});

function getParameterByName( name,href )
{
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( href );
  if( results == null )
    return "";
  else
    return decodeURIComponent(results[1].replace(/\+/g, " "));
}

function loadStories(page)
{
   $.ajax({
			type     : 'GET',
			url      : path+'admin/story?page='+page,
			datatype : 'html',
			success  : function(data) 
				{
				  removeLoader();
				  $('#story_table').hide();
				  $('#render').hide();
				  $('#ajax_stories').html(data);   
                },
    error: function(data) {
     // Error...
      var errors = data.responseJSON;
      console.log(errors);
    }
    
   });
}

function loadFilteredStories(page,category,subcategory)
{
   $.ajax({
			type     : 'GET',
			data : {
				'category'		 : category,
				'subcategory'	 : subcategory
			},
			url      : path+'admin/story?page='+page,
			datatype : 'html',
			success  : function(data) 
				{
				  removeLoader();
				  $('#story_table').hide();
				  $('#render').hide();
				  $('#ajax_stories').html(data);   
                },
    error: function(data) {
     // Error...
      var errors = data.responseJSON;
      console.log(errors);
    }
    
   });
}