$(function() {
	$(document).on('click','.changeHideStatus', function(){
		var $this = $(this);
		var table = $this.data('table');
		var id = $this.data('id');
		var status = $this.data('status');
		bootbox.confirm('Are you sure you want to '+(status ? "unhide" : "hide")+' this story?', function(result) {
			if(result){
				$.ajax({
					url: path+'admin/story/change-status',
					data : $this.closest('form').serialize()+'&id='+id+'&table='+table+'&status='+status,
					dataType: 'json',
					type: 'post',
					beforeSend: function(){
						$this.html('<i class="fa fa-spin fa-spinner"></i>');
					},
					complete: function(){
						
					},
					success: function(json){
						console.log(json);
						if (json.success) {
							if(status){
								$this.data('status',0);
								$this.html('<i class="fa fa-circle active"></i>');
								new PNotify({
								   type: 'success',
								   title: 'Success',
								   text: 'Story unhide'
								   });
							}
							else{
								$this.data('status',1);
								$this.html('<i class="fa fa-circle inactive"></i>');
								new PNotify({
								   type: 'success',
								   title: 'Success',
								   text: 'Story hide'
								   });
							}
						} else if (json.exception_message) {
							if(status){
								$this.html('<i class="fa fa-circle inactive"></i>');
							}
							else{
								$this.html('<i class="fa fa-circle active"></i>');
							}
							new PNotify({
								   type: 'error',
								   title: 'Error',
								   text: 'Sorry, Something went wrong!!'
								   });
						}
					},
					error : function(xhr, ajaxOptions, thrownError) {
						
					}
				});
			}
		});
	});
});