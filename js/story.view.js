$(function(){
	$('.followStory').on('click', function(){
		var $this = $(this);
		$.ajax({
			url : path+"story/follow",
			data : {
				id : $this.data('id'),
				type: $this.data('type'),
				follower: $this.data('follower')
			},
			type : 'get',
			dataType : 'json',
			beforeSend : function() {
				addLoader();
			},
			complete : function() {
				removeLoader();
			},
			success : function(json) {
				if (json.success) {
					if(json.followed){
						$this.html('<i class="fa fa-heart"></i>Unfollow');
						$this.data('follower',1);
					}
					else{
						$this.html('<i class="fa fa-heart"></i>Follow');
						$this.data('follower',0);
					}
					$('.followersCount').text('('+json.count.followers+')');
				} else if (json.exception_message) {
					new PNotify({
			        	type: 'error',
			            title: 'Error',
			            text: json.exception_message
			        });
				}
			},
			error : function(xhr, ajaxOptions, thrownError) {
				if(xhr.status == 401){
					new PNotify({
			        	type: 'warning',
			            title: xhr.responseText,
			            text: 'Please login.'
			        });
				}
			}
		});
	});
	
	$(document).on('click','.likeDislikeStory:not(".disable")', function(){
		var $this = $(this);
		$.ajax({
			url : path+"story/likedislike",
			data : {
				id : $this.data('id'),
				type: $this.data('type'),
				category:$this.data('category')
			},
			type : 'get',
			dataType : 'json',
			beforeSend : function() {
				addLoader();
			},
			complete : function() {
				removeLoader();
				if($this.data('type') == 'like'){
					$this.html('<i class="fa fa-thumbs-up"></i>');
				}else{
					$this.html('<i class="fa fa-thumbs-down"></i>');
				}
			},
			success : function(json) {
				if (json.success) {
					if(json.type == 'like'){
						$this.closest('li').find('.storyLike').addClass('active').addClass('disable');
						$this.closest('li').find('.storyDislike').removeClass('active').removeClass('disable');
						$this.html('<i class="fa fa-thumbs-up"></i>');
					}
					else{
						$this.closest('li').find('.storyDislike').addClass('active').addClass('disable');
						$this.closest('li').find('.storyLike').removeClass('active').removeClass('disable');
						$this.html('<i class="fa fa-thumbs-down"></i>');
					}
					$('.likeCount').text('('+json.count.likes+')');
					$('.dislikeCount').text('('+json.count.dislikes+')');
				} else if (json.exception_message) {
					new PNotify({
			        	type: 'error',
			            title: 'Error',
			            text: json.exception_message
			        });
				}
			},
			error : function(xhr, ajaxOptions, thrownError) {
				if(xhr.status == 401){
					new PNotify({
			        	type: 'warning',
			            title: xhr.responseText,
			            text: 'Please login.'
			        });
				}
			}
		});
	});
	
$(document).on('click','.voteStory:not(".disable")', function(){
		var $this = $(this);
		$.ajax({
			url : path+"story/vote",
			data : {
				id : $this.data('id'),
				type: $this.data('type'),
				category:$this.data('category')
			},
			type : 'get',
			dataType : 'json',
			beforeSend : function() {
				addLoader();
			},
			complete : function() {
				removeLoader();
				if($this.data('type') == 'vote'){
					$this.html('<i class="fa fa-thumbs-o-up"></i>');
				}else{
					$this.html('<i class="fa fa-thumbs-o-down"></i>');
				}
			},
			success : function(json) {
				if (json.success) {
					if(json.type == 'vote'){
						$this.closest('li').find('.storyVote').addClass('active').addClass('disable');
						$this.html('<i class="fa fa-thumbs-o-up"></i>');
					}
					else{
						$this.closest('li').find('.storyVote').removeClass('active').removeClass('disable');
						$this.html('<i class="fa fa-thumbs-o-down"></i>');
					}
					$('.voteCount').text('('+json.count.votes+')');
				} else if (json.exception_message) {
					new PNotify({
			        	type: 'error',
			            title: 'Error',
			            text: json.exception_message
			        });
				}
			},
			error : function(xhr, ajaxOptions, thrownError) {
				if(xhr.status == 401){
					new PNotify({
			        	type: 'warning',
			            title: xhr.responseText,
			            text: 'Please login.'
			        });
				}
			}
		});
	});
});
