$(function(){
	getCommentsOnStory($('input[name="id"]').val(),$('input[name="type"]').val());
	$('#submitComment').on('click', function(event) {

		event.preventDefault()
		var e = $(this);
		$.ajax({
			url : path+"story/save-comment",
			data : $('#commentsForm').serialize(),
			type : 'post',
			dataType : 'json',
			beforeSend : function() {
				addLoader();
			},
			complete : function() {
				removeLoader();
			},
			success : function(json) {
				$('.error').text('');
				if (json.success) {
					new PNotify({
			        	type: 'success',
			            title: 'Success',
			            text: 'Comment has been submitted.'
			        });
					getCommentsOnStory($('input[name="id"]').val(),$('input[name="type"]').val());
					$('html, body').animate({scrollTop : 0},800);
					$('#commentsForm')[0].reset();
				} else if (json.exception_message) {
					new PNotify({
			        	type: 'error',
			            title: 'Error',
			            text: json.exception_message
			        });
				}
			},
			error : function(xhr, ajaxOptions, thrownError) {
				if(xhr.status == 422){
					var errors = $.parseJSON(xhr.responseText);
					$.each(errors, function(i, obj)
					   {
							$('input[name='+i+']').closest('.element').find('.error').slideDown(400).html(obj);
							$('textarea[name='+i+']').closest('.element').find('.error').slideDown(400).html(obj);
					   }); 
				}
				
				if(xhr.status == 401){
					new PNotify({
			        	type: 'warning',
			            title: xhr.responseText,
			            text: 'Guest user isn\'t authorized to comment on any stories, please login.'
			        });
				}
			}
		});

	});
	
$(document).on('click', '.pagination a', function (e) 
 {
			$('#comment_section').hide();
		    $('#render').hide();
			addLoader();
			
            loadComments($(this).attr('href').split('page=')[1]);
            e.preventDefault();
 });	

});

function getCommentsOnStory($type_id,$type){
	$.ajax({
		type: 'get', 
		url : path+"story/comments",
		data : {
			'type_id': $type_id,
			'type'	 : $type
		},
		dataType : 'html',
		beforeSend : function() {
			addLoader();
		},
		complete : function() {
			removeLoader();
		},
		success : function(html) {
			$('.commentsContainer').html(html);
		},
		error : function(xhr, ajaxOptions, thrownError) {
									console.log(xhr);
				
				if(xhr.status == 403){
					new PNotify({
			        	type: 'warning',
			            title: xhr.responseText,
			            text: 'Guest user isn\'t authorized to comment on any stories, please login.'
			        });
				}
			}
	});
}

function loadComments(page)
  {
   $.ajax({
			type     : 'GET',
			url      : path+'/story/comments?page='+page,
			data : {
				'type_id': $('input[name="id"]').val(),
				'type'	 : $('input[name="type"]').val()
			},
			datatype : 'html',
			success  : function(data) 
				{
				  console.log(data);
				  removeLoader();
				  $('.commentsContainer').html(data);   
                },
    error: function(data) {
     // Error...
      var errors = data.responseJSON;
      console.log(errors);
    }
    
   });
  }