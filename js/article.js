/*
 * Added on : 15 oct 2015
 * Added by : debut infotech
 * Desc : To perform js operations on articles module.
 * */
$(document).ready(function(){
	
	//when category is selected, load sub categories.
	$("#article_category_id").change(function(){
		var category_id = $("#article_category_id :selected").val();
		//csrf token required for ajax requests
		var formData = $('meta[name="csrf-token"]').attr('content');
		if(category_id != ''){
			$.ajax({
					type : 'POST',
					data : '_token='+formData,
					url : path+'admin/articles/sub-category/'+category_id,
					success : function(data){
						$("#load_subCategories").show();
						$("#load_subCategories").html(data);
					},
					error : function(data){
						console.log(data);
					}
			});	
		}
	});
	
	//set articles as featured or un featured.
	$(".featuredStatus").click(function(){
		var $this = $(this);
		var table = $this.data('table');
		var id = $this.data('id');
		var status = $this.data('status');
		bootbox.confirm('Are you sure you want to make this article '+(status ? "featured" : "unfeatured")+' ?', function(result) {
			if(result){
				//csrf token required for ajax requests
				var formData = $('meta[name="csrf-token"]').attr('content');
				var icon = $this.html();
				$.ajax({
					url: path+'admin/articles/set-featured',
					data : '_token='+formData+'&id='+id+'&table='+table+'&status='+status,
					dataType: 'json',
					type: 'post',
					beforeSend: function(){
						$this.html('<i class="fa fa-spin fa-spinner"></i>');
						
					},
					complete: function(){
						
					},
					success: function(json){
						console.log(json);
						if (json.success) {
							if(status){
								$this.data('status',0);
								$this.css('color','orange');
								$this.html(icon);
								new PNotify({
								   type: 'success',
								   title: 'Success',
								   text: 'Status changed'
								   });
							}
							else{
								$this.data('status',1);
								$this.removeAttr('style');
								$this.html(icon);
								new PNotify({
								   type: 'success',
								   title: 'Success',
								   text: 'Status changed'
								   });
							}
						} else if (json.exception_message) {
							new PNotify({
								   type: 'error',
								   title: 'Error',
								   text: 'Sorry, Something went wrong!!'
								   });
						}
					},
					error : function(xhr, ajaxOptions, thrownError) {
						
					}
				});
			}
	});
});
});
