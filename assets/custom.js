$(document).ready(function(){
	
	/*login form validation */
		$("#adminLoginForm").validate({
			rules: {
					"email":{
						required: true,
						email:true
					},
					"password":{
					required: true
					}
				},
			success: function(label) {
				$(label).closest('.form-group').removeClass('has-error').addClass('has-success has-feedback');
				$(label).closest('.form-group').find('span.glyphicon').removeClass('glyphicon-remove').addClass('glyphicon-ok').show();
				},
			errorClass: "error",
			errorElement: 'span',
			errorClass: 'help-block',
			highlight: function(label){
			$(label).closest('.form-group').removeClass('has-success').addClass('has-error has-feedback');
			$(label).closest('.form-group').find('span.glyphicon').addClass('glyphicon-remove').removeClass('glyphicon-ok').show();
			}
			});
			
			/* new user registration */
			
			$("#new_user").validate({
			rules: {
					"username":{
						required: true,
					},
					"email":{
						required: true,
						email : true
					},
					"role":{
						required: true,
					},
					"status":{
						required: true,
					},
					"password":{
					required: true
					}
				},
			success: function(label) {
				$(label).closest('.form-group').removeClass('has-error').addClass('has-success has-feedback');
				$(label).closest('.form-group').find('span.glyphicon').removeClass('glyphicon-remove').addClass('glyphicon-ok').show();
				},
			errorClass: "error",
			errorElement: 'span',
			errorClass: 'help-block',
			highlight: function(label){
			$(label).closest('.form-group').removeClass('has-success').addClass('has-error has-feedback');
			$(label).closest('.form-group').find('span.glyphicon').addClass('glyphicon-remove').removeClass('glyphicon-ok').show();
			}
			});
			 
			});
